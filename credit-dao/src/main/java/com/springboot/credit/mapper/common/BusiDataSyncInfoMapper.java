package com.springboot.credit.mapper.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.credit.entity.common.BusiDataSyncInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 业务数据同步记录表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-21
 */
@Mapper
public interface BusiDataSyncInfoMapper extends BaseMapper<BusiDataSyncInfo> {

}
