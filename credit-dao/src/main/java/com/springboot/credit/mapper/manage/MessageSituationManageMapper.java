package com.springboot.credit.mapper.manage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.credit.bo.QueryMessageSituationBo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;

/**
 * @author liuc
 * @date 2024-09-24 13:52
 */
@Mapper
public interface MessageSituationManageMapper extends BaseMapper<Object> {
    @Select("${sqlStr}")
    List<QueryMessageSituationBo> queryMessageSituationList(String string);
}
