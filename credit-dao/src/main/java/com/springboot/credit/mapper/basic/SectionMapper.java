package com.springboot.credit.mapper.basic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.springboot.credit.entity.basic.Section;

/**
 * 二代公共信息段表 映射层。
 *
 * @author liuc
 * @since 1.0.1
 */
@Mapper
public interface SectionMapper extends BaseMapper<Section> {

}
