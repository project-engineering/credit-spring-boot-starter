package com.springboot.credit.mapper.basic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.credit.entity.basic.BaseSection;

/**
 * <p>
 * 二代基础数据段表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-01-08
 */
public interface BasesectionMapper extends BaseMapper<BaseSection> {

}
