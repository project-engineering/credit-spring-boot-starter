package com.springboot.credit.mapper.basic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.credit.entity.basic.PacketFile;
import org.apache.ibatis.annotations.Mapper;

/**
*  mapper接口 : 二代报文文件表
*  @author liuc
*  @since 2024-01-27
*/
@Mapper
public interface PacketFileMapper extends BaseMapper<PacketFile> {

}