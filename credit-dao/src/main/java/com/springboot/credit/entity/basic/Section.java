package com.springboot.credit.entity.basic;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * 二代公共信息段表 实体类。
 *
 * @author liuc
 * @since 1.0.1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("二代公共信息段表")
@TableName(value = "ci_2g_section")
public class Section implements Serializable {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    @TableId("ID")
    private String id;

    /**
     * 项目表ID
     */
    @ApiModelProperty("项目表ID")
    private String projectId;

    /**
     * 关联ID
     */
    @ApiModelProperty("关联ID")
    private String referenceId;

    /**
     * 业务发生时间
     */
    @ApiModelProperty("业务发生时间")
    private LocalDateTime businessTime;

    /**
     * 生成报文时间
     */
    @ApiModelProperty("生成报文时间")
    private LocalDateTime insertTime;

    /**
     * 段标
     */
    @ApiModelProperty("段标")
    private String sectionFlag;

    /**
     * 段报文
     */
    @ApiModelProperty("段报文")
    private String message;

    /**
     * 信息段分类;除被担保人、债权人、反担保人信息段，其他信息段该字段始终为1
     */
    @ApiModelProperty("信息段分类;除被担保人、债权人、反担保人信息段，其他信息段该字段始终为1")
    private String sectionCategory;

    /**
     * 报文条目ID
     */
    @ApiModelProperty("报文条目ID")
    private String itemId;

    /**
     * 所在报文第几段
     */
    @ApiModelProperty("所在报文第几段")
    private Integer sectionNo;

    /**
     * 报送状态;1：已报送，2：报送失败
     */
    @ApiModelProperty("报送状态;1：已报送，2：报送失败")
    private String sendStatus;

    /**
     * 实际业务发生日期
     */
    @ApiModelProperty("实际业务发生日期")
    private String actualBusinessDate;

    /**
     * 首次报送征信日期
     */
    @ApiModelProperty("首次报送征信日期")
    private String firstReportDate;

    /**
     * 合同表ID
     */
    @ApiModelProperty("合同表ID")
    private String contractId;

    /**
     * 企业表ID
     */
    @ApiModelProperty("企业表ID")
    private String enterId;

    /**
     * 租户号
     */
    @ApiModelProperty("租户号")
    private String tenantId;

    /**
     * 乐观锁
     */
    @ApiModelProperty("乐观锁")
    private Integer revision;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

}
