package com.springboot.credit.entity.basic;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import java.io.Serializable;
import java.util.Date;


/**
* <p>
* 二代报文文件表
* </p>
* @author liuc
* @since 2024-01-27
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("ci_2g_packet_file")
@ApiModel(value = "ci_2g_packet_file对象", description = "二代报文文件表")
public class PacketFile implements Serializable {


    /**
    * ID
    */
    @NotEmpty(message = "ID could not be empty")
    @Length(max = 47, message = "ID length could not exceed 47")
    @ApiModelProperty("ID")
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    @NotNull(message = "ID could not be null")
    private String id;

    /**
    * 文件名
    */
    @NotEmpty(message = "FILE_NAME could not be empty")
    @Length(max = 47, message = "FILE_NAME length could not exceed 47")
    @ApiModelProperty("文件名")
    @TableField("FILE_NAME")
    private String fileName;

    /**
    * 文件内容
    */
    @NotEmpty(message = "CONTENT could not be empty")
    @Length(max = 47, message = "CONTENT length could not exceed 47")
    @ApiModelProperty("文件内容")
    @TableField("CONTENT")
    private String content;

    /**
    * 生成时间
    */
    @ApiModelProperty("生成时间")
    @TableField("GENERATE_DATE")
    private Date generateDate;

    /**
    * 最后下载时间
    */
    @ApiModelProperty("最后下载时间")
    @TableField("DOWNLOAD_TIME")
    private Date downloadTime;

    /**
    * 可用标识
    */
    @NotEmpty(message = "USE_STATE could not be empty")
    @Length(max = 47, message = "USE_STATE length could not exceed 47")
    @ApiModelProperty("可用标识")
    @TableField("USE_STATE")
    private String useState;

    /**
    * 信息记录类型
    */
    @NotEmpty(message = "RECORD_TYPE could not be empty")
    @Length(max = 47, message = "RECORD_TYPE length could not exceed 47")
    @ApiModelProperty("信息记录类型")
    @TableField("RECORD_TYPE")
    private String recordType;

    /**
    * 租户号
    */
    @NotEmpty(message = "tenant_id could not be empty")
    @Length(max = 47, message = "tenant_id length could not exceed 47")
    @ApiModelProperty("租户号")
    @TableField("tenant_id")
    private String tenantId;

    /**
    * 乐观锁
    */
    @ApiModelProperty("乐观锁")
    @TableField("revision")
    private Integer revision;

    /**
    * 创建人
    */
    @NotEmpty(message = "create_by could not be empty")
    @Length(max = 47, message = "create_by length could not exceed 47")
    @ApiModelProperty("创建人")
    @TableField("create_by")
    private String createBy;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
    * 更新人
    */
    @NotEmpty(message = "update_by could not be empty")
    @Length(max = 47, message = "update_by length could not exceed 47")
    @ApiModelProperty("更新人")
    @TableField("update_by")
    private String updateBy;

    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;

}