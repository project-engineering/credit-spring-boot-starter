package com.springboot.credit.entity.common;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * <p>
 * 业务数据同步记录表
 * </p>
 *
 * @author liuc
 * @since 2024-02-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("busi_data_sync_info")
@ApiModel(value = "BusiDataSyncInfo对象", description = "业务数据同步记录表")
public class BusiDataSyncInfo {

    @ApiModelProperty("主键")
    @TableId("id")
    private String id;

    @ApiModelProperty("同步方式：0-系统自动同步；1-手动同步")
    @TableField("sync_type")
    private String syncType;

    @ApiModelProperty("同步日期")
    @TableField("sync_date")
    private Date syncDate;

    @ApiModelProperty("同步状态：0-同步成功；1-同步失败；2-同步中")
    @TableField("sync_status")
    private String syncStatus;

    @ApiModelProperty("失败记录")
    @TableField("failure_record")
    private String failureRecord;

    @ApiModelProperty("租户号")
    @TableField("tenant_id")
    private String tenantId;

    @ApiModelProperty("乐观锁")
    @TableField("revision")
    private Integer revision;

    @ApiModelProperty("创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty("创建时间")
    @TableField(value ="create_time",fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty("更新人")
    @TableField("update_by")
    private String updateBy;

    @ApiModelProperty("更新时间")
    @TableField(value ="update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
}
