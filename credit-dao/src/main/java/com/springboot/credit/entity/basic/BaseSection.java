package com.springboot.credit.entity.basic;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 二代基础数据段表
 * </p>
 *
 * @author liuc
 * @since 2024-01-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ci_2g_base_section")
@ApiModel(value = "BaseSection对象", description = "二代基础数据段表")
public class BaseSection implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private String ID;

    @ApiModelProperty(value = "信息记录类型")
    private String recordType;

    @ApiModelProperty(value = "账户类型")
    private String accountType;

    @ApiModelProperty(value = "账户标识码")
    private String accountId;

    @ApiModelProperty(value = "信息报告日期")
    private String reportDate;

    @ApiModelProperty(value = "报告时点")
    private String reportPoint;

    @ApiModelProperty(value = "被担保人名称")
    private String gpName;

    @ApiModelProperty(value = "被担保人证件类型")
    private String gpIdType;

    @ApiModelProperty(value = "被担保人证件号码")
    private String gpIdNo;

    @ApiModelProperty(value = "业务管理机构代码")
    private String guaranteeCode;

    @ApiModelProperty(value = "租户号")
    private String tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer REVISION;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
