package com.springboot.credit.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;
import java.util.Objects;

@Data
@Api(tags = "报文情况管理查询出参")
public class QueryMessageSituationBo {
    @ApiModelProperty(value = "项目id")
    private String projectId;

    @ApiModelProperty(value = "项目编号")
    private String projectNo;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "客户编号")
    private String custNo;

    @ApiModelProperty(value = "客户名称")
    private String custName;

    @ApiModelProperty(value = "信息记录类型")
    private String recordType;

    @ApiModelProperty(value = "所在文件Id")
    private String packetFileId;

    @ApiModelProperty(value = "文件名")
    private String fileName;

    @ApiModelProperty("生成时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date generateDate;

    @ApiModelProperty("最后下载时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date downloadTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryMessageSituationBo that = (QueryMessageSituationBo) o;
        return Objects.equals(projectId, that.projectId) && Objects.equals(projectNo, that.projectNo) && Objects.equals(projectName, that.projectName) && Objects.equals(custNo, that.custNo) && Objects.equals(custName, that.custName) && Objects.equals(recordType, that.recordType) && Objects.equals(packetFileId, that.packetFileId) && Objects.equals(fileName, that.fileName) && Objects.equals(generateDate, that.generateDate) && Objects.equals(downloadTime, that.downloadTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, projectNo, projectName, custNo, custName, recordType, packetFileId, fileName, generateDate, downloadTime);
    }
}
