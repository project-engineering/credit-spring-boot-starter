# 同步之前将当天已经迁移的数据可用状态置为0-不可用
# 更新数据状态的sql语句 ,将当天已经导入过的数据状态改为0-不可用
update schema1.project_income set use_state = '0' where date_format(import_date,'%y%m%d') <=date_format(now(),'%y%m%d');
 

# 执行数据同步
insert into schema1.project_income (pk_id,
	id,
	project_id,
	project_no,
	project_name,
	income_amount,
	income_date,
	income_type,
	charge_status,
	review_rate,
	charge_date,
	charge_amount,
	pm_opinion,
	file,
	approval_status,
	create_id,
	create_name,
	process_id,
	invoice_flag,
	create_time,
	update_time,
	use_state,
	import_date,
    import_num
) select concat(date_format(now(6), '%y%m%d%h%m%s%f'), id) as pk_id,
	id,
	project_id,
	project_no,
	project_name,
	income_amount,
	income_date,
	income_type,
	charge_status,
	review_rate,
	charge_date,
	charge_amount,
	pm_opinion,
	file,
	approval_status,
	create_id,
	create_name,
	process_id,
	invoice_flag,
	create_time,
	update_time,
	'1',
     sysdate(),
     (SELECT COALESCE(MAX(import_num), 0) + 1 FROM schema1.project_income
      where date_format(import_date,'%y%m%d') = date_format(now(),'%y%m%d'))
  from schema2.project_income
  where project_id in (select
                           project_id
                       from schema2.project_info
                       where product_line in ('WAR','IFS','NIFS') and status_id != '6');