# 同步之前将当天已经迁移的数据可用状态置为0-不可用
# 更新数据状态的sql语句 ,将当天已经导入过的数据状态改为0-不可用
update schema1.counter_guarantee set use_state = '0' where date_format(import_date,'%y%m%d') <=date_format(now(),'%y%m%d');
 

# 执行数据同步
insert into schema1.counter_guarantee (pk_id,
	cou_guar_id,
	cou_guar_name,
	busi_type,
	project_id,
	project_name,
	guar_mode,
	guar_sub_mode,
	assess_value,
	ratio,
	discount_value,
	warrant_status,
	status_id,
	regulator_flag,
	org_id,
	org_name,
	is_notarize,
	is_insure,
	insure_end_date,
	is_register,
	register_end_date,
	process_id,
	create_time,
	update_time,
	use_state,
	import_date,
    import_num
) select concat(date_format(now(6), '%y%m%d%h%m%s%f'), cou_guar_id) as pk_id,
	cou_guar_id,
	cou_guar_name,
	busi_type,
	project_id,
	project_name,
	guar_mode,
	guar_sub_mode,
	assess_value,
	ratio,
	discount_value,
	warrant_status,
	status_id,
	regulator_flag,
	org_id,
	org_name,
	is_notarize,
	is_insure,
	insure_end_date,
	is_register,
	register_end_date,
	process_id,
	create_time,
	update_time,
	'1',
     sysdate(),
     (SELECT COALESCE(MAX(import_num), 0) + 1 FROM schema1.counter_guarantee
      where date_format(import_date,'%y%m%d') = date_format(now(),'%y%m%d'))
  from schema2.counter_guarantee
  where project_id in (select
                           project_id
                       from schema2.project_info
                       where product_line in ('WAR','IFS','NIFS') and status_id != '6');