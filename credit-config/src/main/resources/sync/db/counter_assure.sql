# 同步之前将当天已经迁移的数据可用状态置为0-不可用
# 更新数据状态的sql语句 ,将当天已经导入过的数据状态改为0-不可用
update schema1.counter_assure set use_state = '0' where date_format(import_date,'%y%m%d') <=date_format(now(),'%y%m%d');
 

# 执行数据同步
insert into schema1.counter_assure (pk_id,
	assure_id,
	cou_guar_id,
	core_guar_flag,
	assure_type,
	guar_name,
	guar_type,
	guar_idtype,
	guar_idno,
	file,
	remark,
	create_time,
	update_time,
	use_state,
	import_date,
    import_num
) select concat(date_format(now(6), '%y%m%d%h%m%s%f'), assure_id) as pk_id,
	assure_id,
	cou_guar_id,
	core_guar_flag,
	assure_type,
	guar_name,
	guar_type,
	guar_idtype,
	guar_idno,
	file,
	remark,
	create_time,
	update_time,
	'1',
     sysdate(),
     (SELECT COALESCE(MAX(import_num), 0) + 1 FROM schema1.counter_assure
      where date_format(import_date,'%y%m%d') = date_format(now(),'%y%m%d'))
  from schema2.counter_assure;