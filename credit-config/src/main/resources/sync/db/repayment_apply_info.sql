# 同步之前将当天已经迁移的数据可用状态置为0-不可用
# 更新数据状态的sql语句 ,将当天已经导入过的数据状态改为0-不可用
update schema1.repayment_apply_info set use_state = '0' where date_format(import_date,'%y%m%d') <=date_format(now(),'%y%m%d');
 

# 执行数据同步
insert into schema1.repayment_apply_info (pk_id,
	repayment_id,
	project_id,
	repayment_type,
	repayment_amount,
	repayment_date,
	release_amount,
	liability_amount,
	all_release_flag,
	release_date,
	bond_amount,
	bond_account,
	fee_amount,
	fee_account,
	recover_type,
	remark,
	release_cou_guar_id,
	pm_opinion,
	approval_status,
	process_id,
	file,
	create_time,
	update_time,
	use_state,
	import_date,
    import_num
) select concat(date_format(now(6), '%y%m%d%h%m%s%f'), repayment_id) as pk_id,
	repayment_id,
	project_id,
	repayment_type,
	repayment_amount,
	repayment_date,
	release_amount,
	liability_amount,
	all_release_flag,
	release_date,
	bond_amount,
	bond_account,
	fee_amount,
	fee_account,
	recover_type,
	remark,
	release_cou_guar_id,
	pm_opinion,
	approval_status,
	process_id,
	file,
	create_time,
	update_time,
	'1',
     sysdate(),
     (SELECT COALESCE(MAX(import_num), 0) + 1 FROM schema1.repayment_apply_info
      where date_format(import_date,'%y%m%d') = date_format(now(),'%y%m%d'))
  from schema2.repayment_apply_info
  where project_id in (select
                           project_id
                       from schema2.project_info
                       where product_line in ('WAR','IFS','NIFS') and status_id != '6');