# 同步之前将当天已经迁移的数据可用状态置为0-不可用
# 更新数据状态的sql语句 ,将当天已经导入过的数据状态改为0-不可用
update schema1.cust_enterprise_leadership set use_state = '0' where date_format(import_date,'%y%m%d') <=date_format(now(),'%y%m%d');
 

# 执行数据同步
insert into schema1.cust_enterprise_leadership (pk_id,
	id,
	cust_no,
	leader_name,
	duties,
	leader_idtype,
	leader_idno,
	leader_edu,
	leader_phone,
	leader_mobile,
	birth_date,
	leader_gender,
	practice_years,
	practice_experience,
	age,
	company_service_age,
	guar_type,
	remark,
	create_time,
	update_time,
	use_state,
	import_date,
    import_num
) select concat(date_format(now(6), '%y%m%d%h%m%s%f'), id) as pk_id,
	id,
	cust_no,
	leader_name,
	duties,
	leader_idtype,
	leader_idno,
	leader_edu,
	leader_phone,
	leader_mobile,
	birth_date,
	leader_gender,
	practice_years,
	practice_experience,
	age,
	company_service_age,
	guar_type,
	remark,
	create_time,
	update_time,
	'1',
     sysdate(),
     (SELECT COALESCE(MAX(import_num), 0) + 1 FROM schema1.cust_enterprise_leadership
      where date_format(import_date,'%y%m%d') = date_format(now(),'%y%m%d'))
  from schema2.cust_enterprise_leadership
  where cust_no in (select distinct
                        cust_no
                    from schema3.project_info
                    where product_line in ('WAR','IFS','NIFS') and status_id != '6');