# 回滚的时候，先删除导入的数据，然后再将最近的一批数据还原成正常状态
delete from schema1.counter_guarantee where use_state = '1';

#还原最近一批的数据
update
	schema1.counter_guarantee
set
	use_state = '1'
where
	pk_id in (
	select
		a.pk_id
	from
		(
		select
			pk_id
		from
			schema1.counter_guarantee
		where
			use_state = '0'
			and import_date = (
			select
				max(import_date)
			from
				schema1.counter_guarantee
			where
				use_state = '0')
          and import_num = (
            select
                max(import_num)
            from
                schema1.counter_guarantee
            where
                use_state = '0'
        )) as a);