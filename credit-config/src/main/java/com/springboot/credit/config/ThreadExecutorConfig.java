package com.springboot.credit.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author liuc
 * @date 2024-09-13 11:31
 */
@Configuration
@EnableAsync // 开启异步调用
public class ThreadExecutorConfig {
    @Value("${task.pool.corePoolSize}")
    private int corePoolSize;
    @Value("${task.pool.maxPoolSize}")
    private int maxPoolSize;
    @Value("${task.pool.keepAliveSeconds}")
    private int keepAliveSeconds;
    @Value("${task.pool.queueCapacity}")
    private int queueCapacity;
    @Value("${task.pool.awaitTerminationSeconds}")
    private int awaitTerminationSeconds;
    @Value("${task.pool.threadNamePrefix}")
    private String threadNamePrefix;
    @Value("${task.pool.waitForTasksToCompleteOnShutdown}")
    private boolean waitForTasksToCompleteOnShutdown;
    @Value("${task.pool.allowCoreThreadTimeOut}")
    private boolean allowCoreThreadTimeOut;

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(corePoolSize);
        //配置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        //配置线程池队列容量
        executor.setQueueCapacity(queueCapacity);
        //配置线程池维护线程所允许的空闲时间
        executor.setKeepAliveSeconds(keepAliveSeconds);
        //配置线程名称前缀
        executor.setThreadNamePrefix(threadNamePrefix);
        /*
         * 配置线程池拒绝策略
         * ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
         * ThreadPoolExecutor.CallerRunsPolicy:由调用线程处理该任务。
         * ThreadPoolExecutor.DiscardOldestPolicy:丢弃队列中等待最久的任务，然后重试执行任务（重复此过程，直到成功为止）。
         * ThreadPoolExecutor.DiscardPolicy:直接丢弃任务，不予任何处理。
         */
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //配置线程池是否允许核心线程超时
        executor.setAllowCoreThreadTimeOut(allowCoreThreadTimeOut);
        //配置线程池等待任务完成的超时时间
        executor.setWaitForTasksToCompleteOnShutdown(waitForTasksToCompleteOnShutdown);
        //配置线程池等待时间
        executor.setAwaitTerminationSeconds(awaitTerminationSeconds);
        //配置线程池的线程工厂
        executor.setThreadFactory(Executors.defaultThreadFactory());
        //初始化线程池
        executor.initialize();
        //返回线程池
        return executor;
    }
}
