package com.springboot.credit.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 创建了一个名为WebMvcConfig的配置类，并实现了WebMvcConfigurer接口。
 * 在addFormatters方法中，我们向FormatterRegistry注册了一个DateFormatter，并指定了日期的格
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new DateFormatter("yyyy-MM-dd HH:mm:ss"));
    }
}