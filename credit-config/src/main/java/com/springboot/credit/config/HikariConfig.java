package com.springboot.credit.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
public class HikariConfig {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.hikari.minimum-idle}")
    private Integer minimumIdle;
    @Value("${spring.datasource.hikari.idle-timeout}")
    private Integer idleTimeout;
    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private Integer maximumPoolSize;
    @Value("${spring.datasource.hikari.auto-commit}")
    private boolean autoCommit;
    @Value("${spring.datasource.hikari.max-lifetime}")
    private Integer maxLifetime;
    @Value("${spring.datasource.hikari.connection-timeout}")
    private int connectionTimeout;
    @Value("${spring.datasource.hikari.connection-test-query}")
    private String connectionTestQuery;
    @Value("${mybatis-plus.base-package}")
    private String basePackage;


    @Bean(name = "dataSource", destroyMethod = "close")
    public HikariDataSource initDataSource() {
        log.info("初始化HikariDataSource");
        HikariDataSource kds = new HikariDataSource();
        kds.setJdbcUrl(url);
        kds.setUsername(username);
        kds.setPassword(password);
        kds.setDriverClassName(driverClassName);
        kds.setConnectionTimeout(connectionTimeout);
        kds.setMinimumIdle(minimumIdle);
        kds.setIdleTimeout(idleTimeout);
        kds.setMaximumPoolSize(maximumPoolSize);
        kds.setAutoCommit(autoCommit);
        kds.setMaxLifetime(maxLifetime);
        kds.setConnectionTestQuery(connectionTestQuery);
        log.info("初始化HikariDataSource完成");
        return kds;
    }

}
