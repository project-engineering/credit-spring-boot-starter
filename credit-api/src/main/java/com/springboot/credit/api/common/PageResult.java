package com.springboot.credit.api.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.springboot.credit.common.enums.ResultCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuc
 * @date 2024-07-18 9:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "分页结果")
public class PageResult<T> extends Result {

    @Schema(description = "总记录数")
    private long total;

    @Schema(description = "当前页码")
    private long pageNo;

    @Schema(description = "每页记录数")
    private long pageSize;

    public PageResult(Page<T> page) {
        super(ResultCode.SUCCESS);
        this.total = page.getTotal();
        this.pageNo = page.getCurrent();
        this.pageSize = page.getSize();
        super.setData(page.getRecords());
    }

    public PageResult(PageResult<T> page) {
        super(ResultCode.SUCCESS);
        this.total = page.getTotal();
        this.pageNo = page.getPageNo();
        this.pageSize = page.getPageSize();
        super.setData(page.getData());
    }

    public PageResult(long total, T data) {
        super(ResultCode.SUCCESS);
        this.total = total;
        super.setData(data);
    }

    public PageResult(int pageNo, int pageSize, long total, T data) {
        super(ResultCode.SUCCESS);
        this.total = total;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        super.setData(data);
    }
}