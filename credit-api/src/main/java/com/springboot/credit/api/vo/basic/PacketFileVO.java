package com.springboot.credit.api.vo.basic;

import com.springboot.credit.api.vo.PageVO;
import io.swagger.annotations.*;
import lombok.*;
import org.springframework.data.annotation.Id;
import java.util.Date;


/**
 * <p>
 * 二代报文文件表
 * </p>
 * @author liuc
 * @since 2024-01-28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ci_2g_packet_file", description = "二代报文文件表")
@EqualsAndHashCode(callSuper=false)
public class PacketFileVO extends PageVO {


    /**
    * ID
    */
    @Id
    @ApiModelProperty("ID")
    private String id;

    /**
    * 文件名
    */
    @ApiModelProperty("文件名")
    private String fileName;

    /**
    * 文件内容
    */
    @ApiModelProperty("文件内容")
    private String content;

    /**
    * 生成时间
    */
    @ApiModelProperty("生成时间")
    private Date generateDate;

    /**
    * 最后下载时间
    */
    @ApiModelProperty("最后下载时间")
    private Date downloadTime;

    /**
    * 可用标识
    */
    @ApiModelProperty("可用标识")
    private String useState;

    /**
    * 信息记录类型
    */
    @ApiModelProperty("信息记录类型")
    private String recordType;

    /**
    * 租户号
    */
    @ApiModelProperty("租户号")
    private String tenantId;

    /**
    * 乐观锁
    */
    @ApiModelProperty("乐观锁")
    private Integer revision;

    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
    * 更新人
    */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;

}