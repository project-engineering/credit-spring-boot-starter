package com.springboot.credit.api.common;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author liuc
 * @date 2024-07-18 9:09
 */
@Data
@Schema(description = "分页参数")
public class Page {
    @Schema(description = "页码", defaultValue = "1")
    private Integer pageNo;

    @Schema(description = "每页数量", defaultValue = "10")
    private Integer pageSize;
}
