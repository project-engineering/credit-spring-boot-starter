package com.springboot.credit.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description ="分页对象")
public class PageVO {

    @ApiModelProperty(value = "起始页", required = true)
    private Integer pageNum;

    @ApiModelProperty(value = "页大小", required = true)
    private Integer pageSize;
}
