package com.springboot.credit.api.params.manage;

import com.springboot.credit.api.common.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class QueryMessageSituationParam extends Page {
    @ApiModelProperty(value = "项目id")
    private String projectId;

    @ApiModelProperty(value = "项目编号")
    private String projectNo;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "客户编号")
    private String custNo;

    @ApiModelProperty(value = "客户名称")
    private String custName;

    @ApiModelProperty(value = "报送时间起")
    private String generateDateStart;

    @ApiModelProperty(value = "报送时间止")
    private String generateDateEnd;

    @ApiModelProperty(value = "信息记录类型")
    private String recordType;
}
