package com.springboot.credit.api.params.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SyncBusinessDataParam {
    @ApiModelProperty(value = "同步的业务表类型")
    private String syncType;
}
