package com.springboot.credit.api.dto.manage;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@Api(tags = "报文情况管理查询出参")
public class QueryMessageSituationDto {
    @ApiModelProperty(value = "所在文件Id")
    private String packetFileId;

    @ApiModelProperty(value = "文件名")
    private String fileName;

    @ApiModelProperty(value = "信息记录类型")
    private String recordType;

    @ApiModelProperty("生成时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date generateDate;

    @ApiModelProperty("最后下载时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date downloadTime;

    public QueryMessageSituationDto(String packetFileId, String fileName, String recordType, Date generateDate, Date downloadTime) {
        this.packetFileId = packetFileId;
        this.fileName = fileName;
        this.recordType = recordType;
        this.generateDate = generateDate;
        this.downloadTime = downloadTime;
    }
}
