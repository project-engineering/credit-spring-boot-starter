package com.springboot.credit.controller.common;

import com.springboot.credit.common.annotation.WebLog;
import com.springboot.credit.common.response.Result;
import com.springboot.credit.service.common.ThreadPoolTaskExecutorServcie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuc
 * @date 2024-09-14 13:49
 */
@RestController
@Api(tags = {"测试模块 "})
@RequestMapping("/test")
public class TestController {
    @Resource
    private ThreadPoolTaskExecutorServcie threadPoolTaskExecutorServcie;

    @WebLog
    @ApiOperation("线程池测试")
    @PostMapping("/testThreadPool")
    public Result<Void> testThreadPool() {
        threadPoolTaskExecutorServcie.execute();
        return Result.success();
    }
}
