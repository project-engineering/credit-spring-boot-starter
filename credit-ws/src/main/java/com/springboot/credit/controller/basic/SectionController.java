package com.springboot.credit.controller.basic;

import com.springboot.credit.common.annotation.WebLog;
import com.springboot.credit.common.response.Result;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.springboot.credit.entity.basic.Section;
import com.springboot.credit.service.basic.ISectionService;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.Serializable;
import java.util.List;

/**
 * 二代公共信息段表 控制层。
 *
 * @author liuc
 * @since 1.0.1
 */
@RestController
@Api("二代公共信息段表接口")
@RequestMapping("/section")
public class SectionController {

    @Resource
    private ISectionService iSectionService;

    /**
     * 添加二代公共信息段表。
     *
     * @param section 二代公共信息段表
     * @return {@code true} 添加成功，{@code false} 添加失败
     */
    @PostMapping("save")
    @ApiOperation("保存二代公共信息段表")
    public boolean save(@RequestBody @ApiParam("二代公共信息段表") Section section) {
        return iSectionService.save(section);
    }

    /**
     * 根据主键删除二代公共信息段表。
     *
     * @param id 主键
     * @return {@code true} 删除成功，{@code false} 删除失败
     */
    @DeleteMapping("remove/{id}")
    @ApiOperation("根据主键二代公共信息段表")
    public boolean remove(@PathVariable @ApiParam("二代公共信息段表主键") Serializable id) {
        return iSectionService.removeById(id);
    }

    /**
     * 根据主键更新二代公共信息段表。
     *
     * @param section 二代公共信息段表
     * @return {@code true} 更新成功，{@code false} 更新失败
     */
    @PutMapping("update")
    @ApiOperation("根据主键更新二代公共信息段表")
    public boolean update(@RequestBody @ApiParam("二代公共信息段表主键") Section section) {
        return iSectionService.updateById(section);
    }

    /**
     * 查询所有二代公共信息段表。
     *
     * @return 所有数据
     */
    @WebLog
    @PostMapping("list")
    @ApiOperation("查询所有二代公共信息段表")
    public Result<List<Section>> list() {
        return Result.success(iSectionService.list());
    }

    /**
     * 根据二代公共信息段表主键获取详细信息。
     *
     * @param id 二代公共信息段表主键
     * @return 二代公共信息段表详情
     */
    @GetMapping("getInfo/{id}")
    @ApiOperation("根据主键获取二代公共信息段表")
    public Section getInfo(@PathVariable @ApiParam("二代公共信息段表主键") Serializable id) {
        return iSectionService.getById(id);
    }

    /**
     * 分页查询二代公共信息段表。
     *
     * @param page 分页对象
     * @return 分页对象
     */
//    @GetMapping("page")
//    @ApiOperation("分页查询二代公共信息段表")
//    public Page<Section> page(@ApiParam("分页信息") Page<Section> page) {
//        return iSectionService.page(page);
//    }

}
