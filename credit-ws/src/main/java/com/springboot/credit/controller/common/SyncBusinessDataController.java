package com.springboot.credit.controller.common;

import com.springboot.credit.common.annotation.WebLog;
import com.springboot.credit.common.constants.CommonConstant;
import com.springboot.credit.common.response.Result;
import com.springboot.credit.service.common.SyncBusinessDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = {"业务数据同步 "})
@RequestMapping("/common")
public class SyncBusinessDataController {
    @Resource
    SyncBusinessDataService syncBusinessDataService;

    @WebLog
    @ApiOperation("业务数据同步")
    @PostMapping("/syncBusinessData")
    public Result<Void> syncBusinessData() {
        syncBusinessDataService.syncBusinessData(CommonConstant.SYNC_TYPE_1);
        return Result.success();
    }
}
