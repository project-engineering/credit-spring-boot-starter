package com.springboot.credit.controller.basic;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.springboot.credit.api.vo.basic.PacketFileVO;
import com.springboot.credit.entity.basic.PacketFile;
import com.springboot.credit.service.basic.PacketFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

/**
* <p>
* 二代报文文件表 前端控制器
* </p>
*
* @author liuc
* @since 2024-01-27
* @version v1.0
*/
@Log4j2
@Api(tags = {"二代报文文件表"})
@RestController
@RequestMapping("/packet-file")
public class PacketFileController {
    @Resource
    private PacketFileService acketFileService;

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @GetMapping(value = "/selectPacketFileListByPage")
    public Page<PacketFile> selectPacketFileListByPage(@RequestBody PacketFileVO packetFileVO){
        return acketFileService.selectPacketFileListByPage(packetFileVO);
    }


    /**
     * 根据id查询
     */
    @ApiOperation(value = "根据id查询数据")
    @GetMapping(value = "/selectPacketFileById")
    public PacketFile selectPacketFileById(@RequestBody PacketFileVO packetFileVO){
        return acketFileService.selectPacketFileById(packetFileVO);
    }

}
