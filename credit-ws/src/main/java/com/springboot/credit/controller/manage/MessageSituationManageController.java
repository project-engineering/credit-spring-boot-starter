package com.springboot.credit.controller.manage;

import com.springboot.credit.api.params.manage.QueryMessageSituationParam;
import com.springboot.credit.service.manage.MessageSituationManageService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuc
 * @date 2024-09-24 14:14
 */
@RestController
@RequestMapping("/manage")
@Api(tags = {"报文情况管理 "})
public class MessageSituationManageController {
    @Resource
    private MessageSituationManageService messageSituationManageService;

    @Operation(summary = "查询报文情况管理列表信息")
    @PostMapping("/queryMessageSituationList")
    public Object queryMessageSituationList(@RequestBody QueryMessageSituationParam queryMessageSituationParam) {
        return messageSituationManageService.queryMessageSituationList(queryMessageSituationParam);
    }

}
