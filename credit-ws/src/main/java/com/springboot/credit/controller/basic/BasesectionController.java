package com.springboot.credit.controller.basic;

import com.springboot.credit.entity.basic.BaseSection;
import com.springboot.credit.service.basic.IBasesectionService;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 二代基础数据段表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-01-08
 */
@RestController
@RequestMapping("/base-section")
public class BasesectionController {


    @Autowired
    private IBasesectionService iBasesectionService;

    @GetMapping(value = "/")
    public ResponseEntity<Page<BaseSection>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<BaseSection> aPage = iBasesectionService.page(new Page<>(current, pageSize));
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<BaseSection> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(iBasesectionService.getById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@RequestBody BaseSection params) {
        iBasesectionService.save(params);
        return new ResponseEntity<>("created successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        iBasesectionService.removeById(id);
        return new ResponseEntity<>("deleted successfully", HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@RequestBody BaseSection params) {
        iBasesectionService.updateById(params);
        return new ResponseEntity<>("updated successfully", HttpStatus.OK);
    }
}
