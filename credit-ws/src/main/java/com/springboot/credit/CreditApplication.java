package com.springboot.credit;

import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@Log4j2
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = "com.springboot")
@MapperScan("com.springboot.credit.**.mapper")
public class CreditApplication {
    public static void main(String[] args) {
        SpringApplication.run(CreditApplication.class,args);
        log.info("征信系统启动完成");
    }
}
