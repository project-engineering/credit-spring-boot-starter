use gp_credit;

CREATE TABLE `project_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `project_id` varchar(32) NOT NULL COMMENT '项目id',
  `project_no` varchar(100) DEFAULT NULL COMMENT '项目编号',
  `project_name` varchar(200) DEFAULT NULL COMMENT '项目名称',
  `programme_id` varchar(20) DEFAULT NULL COMMENT '关联方案id',
  `sub_project_type` varchar(1) DEFAULT NULL COMMENT '子项目类型  1 简易  2 常规',
  `approval_date` date DEFAULT NULL COMMENT '立项日期',
  `start_time` datetime DEFAULT NULL COMMENT '初评时间',
  `review_time` datetime DEFAULT NULL COMMENT '评审时间',
  `thru_date` date DEFAULT NULL COMMENT '到期日期',
  `source` varchar(2) DEFAULT NULL COMMENT '项目来源("1-小程序,2-银行推荐,3-担保机构推荐,4-券商推荐,5-其他机构推荐,6-存量新增,7-存量续作,8-公司推荐,9-项目经理营销"',
  `belong_depart` varchar(10) DEFAULT NULL COMMENT '所属单位',
  `belong_branch` varchar(10) DEFAULT NULL COMMENT '所属部门',
  `belong_province` varchar(100) DEFAULT NULL COMMENT '所属省',
  `belong_city` varchar(100) DEFAULT NULL COMMENT '所属市',
  `belong_county` varchar(100) DEFAULT NULL COMMENT '所属地县',
  `is_batch` varchar(1) DEFAULT '0' COMMENT '是否是批量业务(0-否, 1-是)',
  `is_inclusive` varchar(1) DEFAULT NULL COMMENT '是否普惠项目(0-否, 1-是)',
  `credit_limit` decimal(21,2) DEFAULT NULL COMMENT '授信额度 普惠模式化有值',
  `inclusive_auth` varchar(1) DEFAULT NULL COMMENT '普惠权限 "1-报批,2-备案"',
  `inclusive_filings` varchar(1) DEFAULT NULL COMMENT '普惠备案  "1-事前备案,2-事后备案"',
  `review_type` varchar(1) DEFAULT NULL COMMENT '项目评审类型"1-签批,2-报批,3-报备,4-普惠备案",',
  `is_gov` varchar(1) DEFAULT NULL COMMENT '是否为政府项目 "1-政府类,2-非政府类"',
  `gov_name` varchar(255) DEFAULT NULL COMMENT '政府名称 “是否为政府项目”为1时有值',
  `is_stock` varchar(1) DEFAULT '1' COMMENT '是否为存量项目 "0 否,1 是"',
  `product_line` varchar(5) DEFAULT NULL COMMENT '产品条线',
  `product_type` varchar(5) DEFAULT NULL COMMENT '产品分类',
  `product_id` varchar(20) DEFAULT NULL COMMENT '产品编号',
  `product_name` varchar(200) DEFAULT NULL COMMENT '产品名称',
  `is_foreign` varchar(1) DEFAULT '0' COMMENT '是否为外贸业务 "0-否,1-是"',
  `financing_way` varchar(1) DEFAULT NULL COMMENT '"1-银行贷款,2-债券,3-票据,4-信托"\r,',
  `financing_category` varchar(1) DEFAULT '1' COMMENT '融资类别\r,"1-非固定资产类,2-固定资产类,9-其他"',
  `financing_category_rh` varchar(1) DEFAULT NULL COMMENT '融资类别（人行）:1-融资性担保，2-非融资性担保',
  `loan_type` varchar(2) DEFAULT NULL COMMENT '贷款种类 "1-流动资金,2-综合授信,3-承兑汇票担保,4-保函,5-PPP项目,6-农户贷款,7-二手房贷款,8-二手车贷款,9-固定资产,10-债券类,11-其他"',
  `loan_category` varchar(2) DEFAULT NULL COMMENT '贷款分类',
  `funds_source` varchar(2) DEFAULT NULL COMMENT '资金来源',
  `pm_a_id` varchar(20) DEFAULT NULL COMMENT '项目经理A角ID',
  `pm_a_name` varchar(200) DEFAULT NULL COMMENT '项目经理A角姓名',
  `pm_a_remark` varchar(255) NULL COMMENT '项目经理A备注',
  `pm_b_id` varchar(20) DEFAULT NULL COMMENT '项目经理B角ID',
  `pm_b_name` varchar(200) DEFAULT NULL COMMENT '项目经理B角姓名',
  `pm_b_remark` varchar(255) NULL COMMENT '项目经理B备注',
  `cust_type` varchar(1) DEFAULT NULL COMMENT '借款人类型',
  `cust_no` varchar(32) DEFAULT NULL COMMENT '借款人客户号',
  `cust_name` varchar(255) DEFAULT NULL COMMENT '借款人名称',
  `id_no` varchar(60) DEFAULT NULL COMMENT '借款人证件号',
  `is_scorecard` varchar(1) DEFAULT NULL COMMENT '是否有客户打分卡 "0-否,1-是"',
  `loan_amount` decimal(21,2) DEFAULT NULL COMMENT '贷款金额',
  `loan_rate` decimal(16,7) DEFAULT NULL COMMENT '贷款利率',
  `loan_term` varchar(255) DEFAULT NULL COMMENT '贷款期限',
  `loan_unit` varchar(1) DEFAULT NULL COMMENT '贷款期限单位：1-月,2-天',
  `purpose` varchar(200) DEFAULT NULL COMMENT '资金用途',
  `contract_rate` decimal(16,7) DEFAULT NULL COMMENT '合同利率',
  `fixed_investment_asset` decimal(21,2) DEFAULT NULL COMMENT '固定资产总投资额',
  `self_fund_asset` decimal(21,2) DEFAULT NULL COMMENT '自有资金总额',
  `loan_method` varchar(1) DEFAULT NULL COMMENT '放款方式 "1-一次性放款,2-逐笔放款,3-循环使用"',
  `repayment_method` varchar(1) DEFAULT '1' COMMENT '还款方式 "1-一次还款,2-分期还款"',
  `repayment_source` varchar(200) DEFAULT NULL COMMENT '还款来源',
  `status_id` varchar(1) DEFAULT NULL COMMENT '状态 "1-已受理,2-待审查,3-已审查,4-在保,5-解保"',
  `credit_status_id` varchar(1) DEFAULT NULL COMMENT '状态(征信使用) "1-已受理,2-待审查,3-已审查,4-在保,5-解保"',
  `approval_status` varchar(2) DEFAULT NULL COMMENT '审批状态 "1-审批中,2-审批通过,3-审批拒绝"',
  `service_node` varchar(32) DEFAULT NULL COMMENT '当前业务环节 "01-受理,02-尽调,03-分公司上会,04-报备,05-报批,06-总公司上会,07-合同审批,08-放款,09-在保,10-解保"',
  `process_id` varchar(32) DEFAULT NULL COMMENT '工作流实例编号',
  `is_stock_cust` varchar(1) DEFAULT '0' COMMENT '是否存量客户 "0-否,1-是"',
  `insured_entp` varchar(10) DEFAULT NULL COMMENT '受保企业数量',
  `new_sales` decimal(21,2) DEFAULT NULL COMMENT '新增销售收入',
  `new_taxes` decimal(21,2) DEFAULT NULL COMMENT '新增税收',
  `new_employment` int(11) DEFAULT NULL COMMENT '新增就业人数',
  `accept_opinions` longtext DEFAULT NULL COMMENT '项目受理意见',
  `survey_opinions` longtext DEFAULT NULL COMMENT '项目尽调意见',
  `guarantee_type` varchar(2) DEFAULT NULL COMMENT '担保类型 "1-担保,2-再担保"',
  `re_guarantee_type` varchar(2) DEFAULT NULL COMMENT '再担保类型  "1-代出保函,2-技援再担保,3-共同共保,4-按份共保,5-合作再担保,6-分险再担保,7-一般责任增信再担保,8-连带责任增信再担保"',
  `guar_business` varchar(2) DEFAULT NULL COMMENT '担保业务大类（人行）',
  `guar_business_type` varchar(2) DEFAULT NULL COMMENT '担保业务种类细分（人行）',
  `foreign_amount` decimal(21,2) DEFAULT NULL COMMENT '外币金额',
  `guar_liability_type` varchar(2) DEFAULT NULL COMMENT '保证责任类型',
  `liability_rate` decimal(16,7) DEFAULT NULL COMMENT '责任分险比例',
  `guar_amount` decimal(21,2) DEFAULT NULL COMMENT '担保金额,',
  `liability_amount` decimal(21,2) DEFAULT NULL COMMENT '责任金额',
  `guar_fee_rate` decimal(16,7) DEFAULT NULL COMMENT '担保费率',
  `guar_fee_rate_unit` varchar(1) DEFAULT NULL COMMENT '担保费率:1-%/年,2-其他',
  `other_desc` varchar(255) DEFAULT NULL COMMENT '其他说明',
  `guar_loan_term` varchar(255) DEFAULT NULL COMMENT '担保期限',
  `guar_loan_unit` varchar(1) DEFAULT NULL COMMENT '担保期限单位：1-月,2-天',
  `reverse_guar_flag` varchar(1) DEFAULT NULL COMMENT '是否反保费  "0-否,1-是"',
  `elimination_flag` varchar(1) DEFAULT NULL COMMENT '是否属于《产业结构调整指导目录》中的淘汰类',
  `payable_guar_amount` decimal(21,2) DEFAULT NULL COMMENT '应缴保费总额',
  `payable_guar_method` varchar(2) DEFAULT NULL COMMENT '保费缴纳方式',
  `payable_guar_frequency` decimal(16,7) DEFAULT NULL COMMENT '保费缴纳频率',
  `bond_rate` decimal(16,7) DEFAULT NULL COMMENT '风险保证金占责任金额的比例',
  `bond_amount` decimal(21,2) DEFAULT NULL COMMENT '风险保证金总额',
  `bond_date` varchar(10) DEFAULT NULL COMMENT '风险保证金返还期限',
  `remind_date` date DEFAULT NULL COMMENT '项目提醒日期',
  `online_assign_flag` varchar(1) DEFAULT NULL COMMENT '是否线上签章 "0-否,1-是"',
  `intentional_flag` varchar(1) DEFAULT NULL COMMENT '是否生成制式担保意向函 "0-否,1-是"',
  `related_project_id` varchar(32) DEFAULT NULL COMMENT '关联项目id',
  `related_reason` varchar(255) DEFAULT NULL COMMENT '关联原因',
  `policy_flag` varchar(1) DEFAULT NULL COMMENT '是否属于政策性业务  0-否,1-是',
  `support_agriculture_flag` varchar(1) DEFAULT NULL COMMENT '是否属于支持“三农”主体   0-否,1-是',
  `support_innovate_flag` varchar(1) DEFAULT NULL COMMENT '是否属于支持“双创双服”主体  0-否,1-是',
  `support_small_flag` varchar(1) DEFAULT NULL COMMENT '是否属于支持小微企业  0-否,1-是',
  `support_emerging_flag` varchar(1) DEFAULT NULL COMMENT '是否属于支持战略新兴产业  0-否,1-是',
  `policy_other_flag` varchar(1) DEFAULT NULL COMMENT '是否属于其他政策性业务  0-否,1-是',
  `cou_guar_detail` longtext DEFAULT null COMMENT '反担保措施 ',
  `total_loan_amount` decimal(21,2) DEFAULT NULL COMMENT '累计放款金额',
  `total_loan_liability_amount` decimal(21,2) DEFAULT NULL COMMENT '累计放款责任金额',
  `total_repayment_amount` decimal(21,2) DEFAULT NULL COMMENT '累计还款金额',
  `total_repayment_liability_amount` decimal(21,2) DEFAULT NULL COMMENT '累计还款责任金额',
  `total_comp_amount` decimal(21,2) DEFAULT NULL COMMENT '累计代偿金额',
  `total_recovery_amount` decimal(21,2) DEFAULT NULL COMMENT '累计追偿金额',
  `five_class` varchar(2) DEFAULT NULL COMMENT '五级分类',
  `five_date` datetime DEFAULT NULL COMMENT '五级分类认定时间',
  `comp_date` date DEFAULT NULL COMMENT '首次代偿日期',
  `recovery_date` date DEFAULT NULL COMMENT '首次追偿日期',
  `loss_date` date DEFAULT NULL COMMENT '损失日期',
  `whole_comp_flag` varchar(1) DEFAULT NULL COMMENT '是否整笔代偿',
  `continue_recovery_flag` varchar(1) DEFAULT NULL COMMENT '是否继续追偿',
  `liability_start_date` date DEFAULT NULL COMMENT '责任起始日',
  `liability_release_date` date DEFAULT NULL COMMENT '责任解除日',
  `apply_channel` varchar(10) DEFAULT NULL COMMENT '申请渠道',
  `create_user` varchar(100) DEFAULT NULL COMMENT '发起人',
  `create_user_id` varchar(20) DEFAULT NULL COMMENT '发起人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  PRIMARY KEY (`pk_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='项目基本信息表';


create table `compensation_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `project_id` varchar(32) default null comment '项目id',
  `project_no` varchar(200) default null comment '项目编号',
  `project_name` varchar(200) default null comment '项目名称',
  `whole_comp_flag` varchar(1) default null comment '是否整笔代偿(0-否 1-是)',
  `comp_amount` decimal(21,2) default null comment '代偿金额',
  `comp_principal` decimal(21,2) default null comment '代偿本金',
  `comp_date` date default null comment '代偿日期',
  `last_comp_date` date default null comment '最近追偿日期',
  `total_comp_amount` decimal(21,2) default null comment '累计代偿金额',
  `undertake_comp_amount` decimal(21,2) default null comment '承担代偿金额',
  `comp_balance` decimal(21,2) default null comment '代偿余额',
  `undertake_comp_balance` decimal(21,2) default null comment '承担代偿余额',
  `total_recovery_amount` decimal(21,2) default null comment '累计追偿金额',
  `total_loss_amount` decimal(21,2) default null comment '累计损失金额',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中 10-审批通过 20-审批拒绝)',
  `process_id` varchar(32) default null comment '工作流实例编号',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`) USING BTREE,
  key `idx_ci_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='代偿信息表';



create table `contract_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `con_id` varchar(32) not null comment '主键',
  `process_no` varchar(32) default null comment '流程id',
  `contract_id` varchar(64) default null comment '合同编号',
  `contract_name` varchar(200) default null comment '合同名称',
  `type` varchar(5) default null comment '合同类型',
  `format_flag` varchar(1) default null comment '是否制式(0-否 1-是)',
  `online_assign_flag` varchar(1) default null comment '是否线上签章(0-否 1-是)',
  `assign_date` date default null comment '签订日期',
  `begin_date` date default null comment '合同开始日期',
  `end_date` date default null comment '合同结束日期',
  `loaner` varchar(200) default null comment '贷款企业名称',
  `org_name` varchar(200) default null comment '合作银行名称',
  `guar_amount` decimal(21,2) default null comment '担保金额',
  `liability_amount` decimal(21,2) default null comment '责任金额',
  `guar_term` varchar(10) default null comment '担保期限',
  `guar_liability_type` varchar(2) default null comment '保证责任类型',
  `guar_fee_rate` decimal(16,7) default null comment '担保费率',
  `guar_fee` decimal(21,2) default null comment '担保费',
  `loan_amount` decimal(21,2) default null comment '贷款金额',
  `loan_rate` decimal(16,7) default null comment '贷款利率',
  `loan_term` varchar(10) default null comment '贷款期限',
  `project_id` varchar(32) default null comment '项目id',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `first_flag` varchar(2) default null comment '最新委托担保合同（0-否 1-是）',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中 10-审批通过 20-审批拒绝)',
  `process_id` varchar(20) default null comment '工作流实例编号',
  `status_id` varchar(2) default null comment '状态（0-完成签署 1-待客户签署 2-待公司签署 3-审批驳回 4-已签 5-未签）',
  `contract_file` varchar(1000) default null comment '合同文件',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_contract_id` (`contract_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='合同信息表';


create table `counter_assure` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `assure_id` varchar(32) not null comment 'id',
  `cou_guar_id` varchar(32) default null comment '关联反担保措施id',
  `core_guar_flag` varchar(1) default null comment '是否核心反担保措施(0-否 1-是)',
  `assure_type` varchar(2) default null comment '责任类型',
  `guar_name` varchar(200) default null comment '担保人名称',
  `guar_type` varchar(3) default null comment '担保人类型(1-企业或其他组织 2-自然人)',
  `guar_idtype` varchar(2) default null comment '担保人证件类型',
  `guar_idno` varchar(60) default null comment '担保人证件号码',
  `file` varchar(200) default null comment '附件',
  `remark` varchar(200) default null comment '备注',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cs_cou_guar_id` (`cou_guar_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='保证明细表';


create table `counter_guarantee` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `cou_guar_id` varchar(32) not null comment '反担保措施id',
  `cou_guar_name` varchar(200) default null comment '反担保措施名称',
  `busi_type` varchar(2) default null comment '业务类型(1-项目 2-方案)',
  `project_id` varchar(32) default null comment '项目id',
  `project_name` varchar(200) default null comment '项目名称',
  `guar_mode` varchar(2) default null comment '担保方式',
  `guar_sub_mode` varchar(3) default null comment '担保小类',
  `assess_value` decimal(21,2) default null comment '市场价值',
  `ratio` decimal(16,7) default null comment '折率',
  `discount_value` decimal(21,2) default null comment '折后价值',
  `warrant_status` varchar(1) default null comment '权属证明材料状态(0-未交接 1-已交接 2-已释放)',
  `status_id` varchar(1) default null comment '状态(0-未生效,1-已生效,2-已释放,3-暂存中)',
  `regulator_flag` varchar(1) default null comment '是否监管(0-否 1-是)',
  `org_id` varchar(32) default null comment '监管公司编号',
  `org_name` varchar(200) default null comment '监管公司名称',
  `is_notarize` varchar(1) default null comment '是否公证(0-否 1-是)',
  `is_insure` varchar(1) default null comment '是否保险(0-否 1-是)',
  `insure_end_date` date default null comment '保险到期时间',
  `is_register` varchar(1) default null comment '是否登记(0-否 1-是)',
  `register_end_date` date default null comment '登记到期时间',
  `process_id` varchar(20) default null comment '工作流实例id',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cg_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='反担保措施表';


create table `loss_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `project_id` varchar(32) default null comment '项目id',
  `project_no` varchar(200) default null comment '项目编号',
  `project_name` varchar(200) default null comment '项目名称',
  `continue_recovery_flag` varchar(1) default null comment '是否继续追偿(0-否 1-是)',
  `loss_confirm_date` date default null comment '损失确认日期',
  `last_comp_date` date default null comment '最近代偿日期',
  `last_recovery_date` date default null comment '最近追偿日期',
  `total_comp_amount` decimal(21,2) default null comment '累计代偿金额',
  `undertake_comp_amount` decimal(21,2) default null comment '承担代偿金额',
  `comp_balance` decimal(21,2) default null comment '代偿余额',
  `undertake_comp_balance` decimal(21,2) default null comment '承担代偿余额',
  `total_recovery_amount` decimal(21,2) default null comment '累计追偿金额',
  `total_loss_amount` decimal(21,2) default null comment '累计损失金额',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中 10-审批通过 20-审批拒绝)',
  `process_id` varchar(32) default null comment '工作流实例编号',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_li_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='损失信息表';


create table `project_income` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `project_id` varchar(32) default null comment '项目id',
  `project_no` varchar(200) default null comment '项目编号',
  `project_name` varchar(200) default null comment '项目名称',
  `income_amount` decimal(21,2) default null comment '收入金额',
  `income_date` date default null comment '收入时间',
  `income_type` varchar(100) default null comment '收入类型(1-利息 2-保费 3-评审服务费 9-其他)',
  `charge_status` varchar(1) default null comment '交纳状态',
  `review_rate` decimal(16,7) default null comment '评审费率',
  `charge_date` date default null comment '应缴日期',
  `charge_amount` decimal(21,2) default null comment '应缴金额',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `file` varchar(200) default null comment '附件',
  `approval_status` varchar(2) default null comment '审批状态',
  `create_id` varchar(20) default null comment '操作人员id',
  `create_name` varchar(200) default null comment '操作人员姓名',
  `process_id` varchar(20) default null comment '工作流实例编号',
  `invoice_flag` varchar(1) default null comment '是否开具发票(0-否 1-是)',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `inx_proi_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='项目收入表';


create table `recovery_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `project_id` varchar(32) default null comment '项目id',
  `project_no` varchar(200) default null comment '项目编号',
  `project_name` varchar(200) default null comment '项目名称',
  `continue_recovery_flag` varchar(1) default null comment '是否继续追偿(0-否 1-是)',
  `recovery_amount` decimal(21,2) default null comment '追偿金额',
  `recovery_principal` decimal(21,2) default null comment '追偿本金',
  `repay_type` varchar(2) default null comment '还款形式',
  `recovery_date` date default null comment '追偿日期',
  `last_comp_date` date default null comment '最近代偿日期',
  `total_comp_amount` decimal(21,2) default null comment '累计代偿金额',
  `undertake_comp_amount` decimal(21,2) default null comment '承担代偿金额',
  `comp_balance` decimal(21,2) default null comment '代偿余额',
  `undertake_comp_balance` decimal(21,2) default null comment '承担代偿余额',
  `total_recovery_amount` decimal(21,2) default null comment '累计追偿金额',
  `total_loss_amount` decimal(21,2) default null comment '累计损失金额',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中 10-审批通过 20-审批拒绝)',
  `process_id` varchar(32) default null comment '工作流实例编号',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_ri_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='追偿信息表';



create table `repayment_apply_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `repayment_id` varchar(32) not null comment '还款编号',
  `project_id` varchar(32) default null comment '项目id',
  `repayment_type` varchar(2) default null comment '还款方式',
  `repayment_amount` decimal(21,2) default null comment '还款金额',
  `repayment_date` date default null comment '还款日期',
  `release_amount` decimal(21,2) default null comment '解除金额',
  `liability_amount` decimal(21,2) default null comment '责任金额',
  `all_release_flag` varchar(1) default null comment '全解除标志位(0-否,1-是)',
  `release_date` date default null comment '解除日期',
  `bond_amount` decimal(21,2) default null comment '退保证金金额',
  `bond_account` varchar(60) default null comment '退保证金账号',
  `fee_amount` decimal(21,2) default null comment '退保费金额',
  `fee_account` varchar(60) default null comment '退保费账号',
  `recover_type` varchar(2) default null comment '收回方式(1-直接收回,2-转出,3-发行资产支持证券,4-核销,5-其他收回方式,6-其他收回方式（代偿）)',
  `remark` longtext comment '其他',
  `release_cou_guar_id` varchar(500) default null comment '释放反担保措施id',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中,10-审批通过,20-审批拒绝)',
  `process_id` varchar(20) default null comment '工作流实例编号',
  `file` varchar(200) default null comment '附件',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `inx_rai_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='还款申请信息表';


-- gp_customer
create table `cust_enterprise_info` (
 `pk_id` varchar(60) not null comment 'pk_id',
  `cust_no` varchar(32) not null comment '客户号',
  `cust_name` varchar(200) default null comment '企业名称',
  `credit_code` varchar(60) default null comment '统一社会信用代码',
  `credit_rating` varchar(2) default null comment '主体信用评级',
  `cust_grade` varchar(2) default null comment '客户等级',
  `rating_score` varchar(10) default null comment '评级得分',
  `register_address` varchar(255) default null comment '注册地址',
  `business_address` varchar(255) default null comment '经营地址',
  `belong_province` varchar(200) default null comment '所在地省',
  `belong_city` varchar(200) default null comment '所在地市',
  `belong_county` varchar(200) default null comment '所在地县',
  `division` varchar(6) default null comment '注册所在地区',
  `country` varchar(3) default null comment '国别',
  `mail_address` varchar(255) default null comment '通讯地址',
  `survival_status` varchar(2) default null comment '存续状态',
  `found_date` date default null comment '成立日期',
  `expira_date` date default null comment '营业期限截止日',
  `forever_flag` varchar(1) default null comment '是否长期',
  `loan_card_code` varchar(60) default null comment '中征码（原贷款卡编码）',
  `loan_card_code_time` datetime default null comment '贷款卡/中征码变更时间',
  `register_capital` decimal(21,2) default null comment '注册资本',
  `receipts_capital` decimal(21,2) default null comment '实收资本',
  `agriculture_flag` varchar(1) default null comment '是否涉农 0-否 1-是',
  `agriculture3_flag` varchar(1) default '0' comment '是否三农 0-否 1-是',
  `enterprise_scale` varchar(2) default null comment '企业规模',
  `innovation_flag` varchar(1) default null comment '是否双创 0-否 1-是',
  `organization_code` varchar(60) default null comment '组织机构代码',
  `phone_no` varchar(20) default null comment '联系电话',
  `fax_no` varchar(20) default null comment '传真',
  `postal_code` varchar(20) default null comment '邮政编码',
  `email` varchar(200) default null comment '电子邮件地址',
  `licence_no` varchar(20) default null comment '开户许可证核准号',
  `corp_contacts` varchar(200) default null comment '企业联系人',
  `corp_phone` varchar(20) default null comment '企业联系电话',
  `national_tax_code` varchar(60) default null comment '国税登记证号',
  `finance_pers_name` varchar(200) default null comment '财务联系人',
  `finance_pers_phone` varchar(20) default null comment '财务联系电话',
  `finance_sales_income` decimal(21,2) default null comment '企业销售收入',
  `land_tax_code` varchar(60) default null comment '地税登记证号',
  `industry` varchar(10) default null comment '所属行业',
  `organization_type` varchar(10) default null comment '组织机构类型',
  `economic_type` varchar(10) default null comment '经济类型',
  `gov_certificate_no` varchar(60) default null comment '事业单位证书号',
  `social_certificate_no` varchar(60) default null comment '社会团体登记证号',
  `civilian_certificate_no` varchar(60) default null comment '民办非企业登记号',
  `religion_certificate_no` varchar(60) default null comment '宗教证书登记号',
  `foundation_certificate_no` varchar(60) default null comment '基金会登记号',
  `total_assets` decimal(21,2) default null comment '企业资产合计',
  `basic_account` varchar(60) default null comment '基本户账户',
  `basic_account_bank` varchar(200) default null comment '基本户开户行',
  `basic_account_bank_code` varchar(100) default null comment '基本户开户行行号',
  `business_scope` varchar(2000) default null comment '经营范围',
  `group_flag` varchar(1) default null comment '集团客户 0-否 1-是',
  `group_name` varchar(200) default null comment '集团客户名称',
  `asso_entp_flag` varchar(1) default null comment '是否有关联企业',
  `ife_authority` varchar(1) default '0' comment '进出口权',
  `quoted_company_flag` varchar(1) default '0' comment '是否上市 0-否 1-是',
  `core_entp_flag` varchar(1) default '0' comment '是否核心企业',
  `core_entp_grade` varchar(20) default null comment '核心企业等级',
  `cust_class` varchar(2) default null comment '客户分类',
  `cust_category` varchar(2) default null comment '客户类别',
  `legal_pers_name` varchar(200) default null comment '法定代表人姓名',
  `legal_pers_idtype` varchar(6) default null comment '法人代表证件种类',
  `legal_pers_idno` varchar(40) default null comment '法人代表证件号码',
  `legal_pers_gender` varchar(2) default null comment '法人代表性别 01-男性 02-女性 03-未知性别',
  `legal_pers_phone` varchar(20) default null comment '法定代表人手机号',
  `employees_num` int(11) default null comment '员工总数',
  `specialty_num` int(11) default null comment '大专人数',
  `specialty_ratio` decimal(16,7) default null comment '大专占比',
  `undergraduate_num` int(11) default null comment '大本人数',
  `undergraduate_ratio` decimal(16,7) default null comment '大本占比',
  `master_num` int(11) default null comment '硕士及以上人数',
  `master_ratio` decimal(16,7) default null comment '硕士及以上占比',
  `before_sales_value` decimal(21,2) default null comment '前年销售额',
  `before_sales_profit` decimal(21,2) default null comment '前年利润总额',
  `yester_sales_value` decimal(21,2) default null comment '去年销售额',
  `yester_sales_profit` decimal(21,2) default null comment '去年利润总额',
  `this_sales_value` decimal(21,2) default null comment '本年度计划销售额',
  `this_sales_profit` decimal(21,2) default null comment '计划利润总额',
  `end_month` int(11) default null comment '截至本年度n月末',
  `finsh_sales_value` decimal(21,2) default null comment '已完成销售额',
  `finsh_sales_profit` decimal(21,2) default null comment '实现利润',
  `parent_org_type` varchar(2) default null comment '上级机构类型',
  `parent_org_name` varchar(200) default null comment '上级机构名称',
  `parent_org_idtype` varchar(2) default null comment '上级机构证件类型',
  `parent_org_idno` varchar(60) default null comment '上级机构证件号码',
  `elimination_flag` varchar(1) default null comment '是否属于《产业结构调整指导目录》中的淘汰类 0-否 1-是',
  `enterprise_nature` varchar(3) default null comment '企业性质',
  `qualification_situation` varchar(200) default null comment '资质情况',
  `actual_construction_party` varchar(200) default null comment '实际施工方',
  `credit_status` varchar(200) default null comment '信用状况',
  `cust_id` varchar(32) default null comment '客户唯一编码',
  `remark` varchar(200) default null comment '备注',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cei_credit_code` (`credit_code`)
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='企业客户信息表';


create table `cust_enterprise_controller` (
 `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `cust_no` varchar(32) default null comment '客户号',
  `controller_name` varchar(200) default null comment '实际控制人名称',
  `identity_category` varchar(2) default null comment '身份类型',
  `controller_idtype` varchar(2) default null comment '证件类型',
  `controller_idno` varchar(60) default null comment '证件号码',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cec_cust_no` (`cust_no`)
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='企业客户实控人表';


create table `cust_enterprise_investor` (
 `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `cust_no` varchar(32) default null comment '客户号',
  `investor` varchar(200) default null comment '出资人名称',
  `investor_type` varchar(2) default null comment '出资人类型',
  `contact_address` varchar(200) default null comment '联系地址',
  `identity_category` varchar(2) default null comment '身份类别',
  `investor_idtype` varchar(2) default null comment '证件类型',
  `investor_idno` varchar(60) default null comment '证件号码',
  `invest_method` varchar(20) default null comment '出资方式',
  `invest_date` date default null comment '出资日期',
  `invest_amount` decimal(21,2) default null comment '出资金额',
  `invest_ratio` decimal(16,2) default null comment '持有比例',
  `property_flag` varchar(1) default null comment '是否办理产权变更手续',
  `controller_relationship` varchar(200) default null comment '与实控人关系',
  `investor_year` int(11) default null comment '股东年限',
  `join_flag` varchar(1) default null comment '是否参与经营',
  `remark` varchar(200) default null comment '备注',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cei_cust_no` (`cust_no`)
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='企业客户股权结构表';



create table `cust_enterprise_leadership` (
 `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) not null comment 'id',
  `cust_no` varchar(32) default null comment '客户号',
  `leader_name` varchar(200) default null comment '姓名',
  `duties` varchar(50) default null comment '职务',
  `leader_idtype` varchar(2) default null comment '证件类型',
  `leader_idno` varchar(60) default null comment '证件号码',
  `leader_edu` varchar(2) default null comment '学历',
  `leader_phone` varchar(20) default null comment '手机',
  `leader_mobile` varchar(20) default null comment '电话',
  `birth_date` date default null comment '出生日期',
  `leader_gender` varchar(1) default null comment '性别',
  `practice_years` varchar(2) default null comment '从业年限',
  `practice_experience` longtext comment '从业经历',
  `age` int(11) default null comment '年龄',
  `company_service_age` int(11) default null comment '司龄',
  `guar_type` varchar(1) default null comment '是否担保',
  `remark` varchar(200) default null comment '备注',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_cel_cust_no` (`cust_no`)
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='企业客户管理人表';



create table `loan_report_info` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `report_id` varchar(32) not null comment '报告编号',
  `project_id` varchar(32) default null comment '项目id',
  `project_no` varchar(100) default null comment '项目编号',
  `project_name` varchar(200) default null comment '项目名称',
  `receipt_no` varchar(60) default null comment '单据编号',
  `issuing_unit` varchar(100) default null comment '出具单位',
  `loan_amount` decimal(21,2) default null comment '项目放款金额',
  `loan_date` date default null comment '放款日期',
  `liability_amount` decimal(21,2) default null comment '放款责任金额',
  `release_date` date default null comment '责任解除日期',
  `repay_end_date` date default null comment '还款截止日期',
  `pm_opinion` varchar(200) default null comment '项目经理意见',
  `create_id` varchar(20) default null comment '操作人id',
  `create_name` varchar(100) default null comment '操作人名称',
  `approval_status` varchar(2) default null comment '审批状态(00-审批中 10-审批通过 20-审批拒绝)',
  `process_id` varchar(20) default null comment '工作流实例编号',
  `file` varchar(200) default null comment '附件',
  `create_time` datetime default null comment '创建时间',
  `update_time` datetime default null comment '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  key `idx_lai_project_id` (`project_id`) using btree
) engine=innodb default charset=utf8mb4,COLLATE=utf8mb4_general_ci comment='放款报告记录表';

CREATE TABLE `finance_report_info` (
  `pk_id` varchar(60) NOT NULL COMMENT 'pk_id',
  `id` varchar(32) NOT NULL COMMENT 'id',
  `cust_no` varchar(32) DEFAULT NULL COMMENT '客户号',
  `type` varchar(1) DEFAULT NULL COMMENT '报表类型',
  `year` varchar(4) DEFAULT NULL COMMENT '年',
  `periods` varchar(100) DEFAULT NULL COMMENT '期数',
  `subject` varchar(100) DEFAULT NULL COMMENT '科目名称',
  `amount` decimal(21,2) DEFAULT NULL COMMENT '金额',
  `subject_type` varchar(2) DEFAULT NULL COMMENT '科目分类',
  `subject_name` varchar(100) DEFAULT NULL COMMENT '科目名称(中文)',
  `seq_id` int(11) DEFAULT NULL COMMENT '序号',
  `reference_id` varchar(32) DEFAULT NULL COMMENT '关联人行表id',
  `asset_type` varchar(2) DEFAULT NULL COMMENT '资产类别：01-流动资产,02-非流动资产,11-流动负债,12-非流动负债,13-所有者权益',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `use_state` varchar(1) DEFAULT NULL COMMENT '可用状态;0-否，1-是',
  `import_date` datetime DEFAULT NULL COMMENT '导入时间',
  `import_num` int default null comment '当天导入次数',
  PRIMARY KEY (`pk_id`),
  KEY `idx_fri_cust_no` (`cust_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='财务报表信息表';

CREATE TABLE `finance_report_base` (
  `pk_id` varchar(60) NOT NULL COMMENT 'pk_id',
  `cust_no` varchar(32) NOT NULL COMMENT '客户号',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_type_sub` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `audit_unit` varchar(200) DEFAULT NULL COMMENT '审计单位',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `use_state` varchar(1) DEFAULT NULL COMMENT '可用状态;0-否，1-是',
  `import_date` datetime DEFAULT NULL COMMENT '导入时间',
  `import_num` int default null comment '当天导入次数',
  PRIMARY KEY (`pk_id`),
  KEY `finance_report_base_cust_no_IDX` (`cust_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='财务报表基本信息表';

CREATE TABLE `finance_report_rh` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) NOT NULL COMMENT 'id',
  `cust_no` varchar(32) DEFAULT NULL COMMENT '客户号',
  `project_id` varchar(32) NOT NULL COMMENT '项目id',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_type_sub` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `YEAR` varchar(4) DEFAULT NULL COMMENT '年',
  `type` varchar(1) DEFAULT NULL COMMENT '报表类型',
  `cust_id` varchar(32) DEFAULT NULL COMMENT '客户唯一编码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  primary key (`pk_id`),
  KEY `idx_frr_cust_no` (`cust_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='财务报表信息表(人行)';


CREATE TABLE `busi_data_sync_info` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `sync_type` varchar(1) NULL COMMENT '同步方式：0-系统自动同步；1-手动同步',
  `sync_date` datetime DEFAULT NULL COMMENT '同步日期',
  `sync_status` varchar(1) NULL COMMENT '同步状态：0-同步成功；1-同步失败；2-同步中',
  `failure_record` varchar(3000) NULL COMMENT '失败记录',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='业务数据同步记录表';


CREATE TABLE `credit_report_gen_result` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `bussiness_type` varchar(10) NULL COMMENT '业务类型：业务类型：310-企业基本信息记录;410-企业借贷账户信息记录;440-企业担保账户信息记录;610-企业资产负债表信息记录;620-企业利润表信息记录;630-企业现金流量表信息记录',
  `packet_num` varchar(10) DEFAULT NULL COMMENT '本次生成报文文件数',
  `message_num` varchar(10) NULL COMMENT '本次生成报文数据的数目',
  `abnormal_info_num` varchar(10) NULL COMMENT '异常信息数',
  `status_id` varchar(1) NULL COMMENT '是否已短信通知：0-待通知;1-通知成功;2-通知失败',
  `failure_reason` longtext NULL COMMENT '通知失败原因',
  `phone` varchar(1000) NULL COMMENT '手机号',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='征信报文生成结果通知表';

CREATE TABLE `new_old_project_relationship` (
  `project_id` varchar(32) NOT NULL COMMENT '新项目id',
  `old_project_id` varchar(32) DEFAULT NULL COMMENT '旧项目id(原征信系统项目Id)',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='新旧项目关系表';

CREATE TABLE `contract_guar_relation` (
  `pk_id` varchar(60) not null comment 'pk_id',
  `id` varchar(32) NOT NULL COMMENT 'id',
  `con_id` varchar(32) DEFAULT NULL COMMENT '合同表主键',
  `cou_guar_id` varchar(32) DEFAULT NULL COMMENT '反担保措施id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `use_state` varchar(1) default null comment '可用状态;0-否，1-是',
  `import_date` datetime default null comment '导入时间',
  `import_num` int default null comment '当天导入次数',
  PRIMARY KEY (`pk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='合同反担保措施关联表';


CREATE TABLE `ci_2g_alter_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `account_id` varchar(60) DEFAULT NULL COMMENT '待更正业务标识码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `account_type` varchar(2) DEFAULT NULL COMMENT '账户类型',
  `alter_section_flag` varchar(1) DEFAULT NULL COMMENT '待更正段段标',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代担保按段更正基础段';


-- gp_credit_uat.ci_2g_base_info_section definition

CREATE TABLE `ci_2g_base_info_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `busi_type_first` varchar(1) DEFAULT NULL COMMENT '担保业务大类',
  `busi_type_second` varchar(2) DEFAULT NULL COMMENT '担保业务种类细分',
  `open_date` varchar(10) DEFAULT NULL COMMENT '开户日期',
  `guarantee_amount` varchar(15) DEFAULT NULL COMMENT '担保金额',
  `currency_type` varchar(3) DEFAULT NULL COMMENT '币种',
  `end_date` varchar(10) DEFAULT NULL COMMENT '到期日期',
  `cg_type` varchar(1) DEFAULT NULL COMMENT '反担保方式',
  `other_repay_type` varchar(1) DEFAULT NULL COMMENT '其他还款保证方式',
  `deposit_percent` varchar(4) DEFAULT NULL COMMENT '保证金百分比',
  `gc_no` varchar(120) DEFAULT NULL COMMENT '担保合同号码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代基本信息段表';


-- gp_credit_uat.ci_2g_base_section definition

CREATE TABLE `ci_2g_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `account_type` varchar(2) DEFAULT NULL COMMENT '账户类型',
  `account_id` varchar(60) DEFAULT NULL COMMENT '账户标识码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `gp_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `gp_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `gp_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `guarantee_code` varchar(14) DEFAULT NULL COMMENT '业务管理机构代码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代基础数据段表';


-- gp_credit_uat.ci_2g_base_section_for_alter definition

CREATE TABLE `ci_2g_base_section_for_alter` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `gp_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `gp_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `gp_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `guarantee_code` varchar(14) DEFAULT NULL COMMENT '业务管理机构代码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代待更正基础数据段表';


-- gp_credit_uat.ci_2g_c1_base_info_section definition

CREATE TABLE `ci_2g_c1_base_info_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `busi_type_first` varchar(2) DEFAULT NULL COMMENT '担保业务大类',
  `busi_type_second` varchar(2) DEFAULT NULL COMMENT '担保业务种类细分',
  `open_date` varchar(10) DEFAULT NULL COMMENT '开户日期',
  `guarantee_amount` varchar(15) DEFAULT NULL COMMENT '担保金额',
  `currency_type` varchar(3) DEFAULT NULL COMMENT '币种',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业借贷交易基本信息段表';


-- gp_credit_uat.ci_2g_c1_base_section definition

CREATE TABLE `ci_2g_c1_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `account_type` varchar(2) DEFAULT NULL COMMENT '账户类型',
  `account_id` varchar(60) DEFAULT NULL COMMENT '账户标识码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `gp_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `gp_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `gp_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `guarantee_code` varchar(14) DEFAULT NULL COMMENT '业务管理机构代码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业借贷交易基础数据段表';


-- gp_credit_uat.ci_2g_c1_guarantee_resp_info definition

CREATE TABLE `ci_2g_c1_guarantee_resp_info` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `account_status` varchar(2) DEFAULT NULL COMMENT '账户状态',
  `act_gua_balance` varchar(15) DEFAULT NULL COMMENT '在保余额',
  `balance_update_date` varchar(10) DEFAULT NULL COMMENT '余额变化日期',
  `five_category` varchar(1) DEFAULT NULL COMMENT '五级分类',
  `category_date` varchar(10) DEFAULT NULL COMMENT '五级分类认定日期',
  `last_repay_date` varchar(10) DEFAULT NULL COMMENT '最近一次还款日期',
  `last_repay_amount` varchar(15) DEFAULT NULL COMMENT '最近一次还款金额',
  `last_repay_principal` varchar(15) DEFAULT NULL COMMENT '最近一次实际归还本金',
  `repay_form` varchar(2) DEFAULT NULL COMMENT '还款形式',
  `close_date` varchar(10) DEFAULT NULL COMMENT '账户关闭日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业借贷交易还款表现信息段表';


-- gp_credit_uat.ci_2g_c1_related_pay_person_section definition

CREATE TABLE `ci_2g_c1_related_pay_person_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `person_num` varchar(2) DEFAULT NULL COMMENT '责任人个数',
  `person_type` varchar(1) DEFAULT NULL COMMENT '身份类别',
  `person_name` varchar(160) DEFAULT NULL COMMENT '责任人名称',
  `person_id_type` varchar(2) DEFAULT NULL COMMENT '责任人身份标识类型',
  `person_id_no` varchar(80) DEFAULT NULL COMMENT '责任人身份标识号码',
  `duty_type` varchar(1) DEFAULT NULL COMMENT '还款责任人类型',
  `duty_amount` varchar(15) DEFAULT NULL COMMENT '还款责任金额',
  `union_flag` varchar(1) DEFAULT NULL COMMENT '联保标识',
  `contract_no` varchar(60) DEFAULT NULL COMMENT '保证合同号码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业借贷交易相关还款责任人段表';


-- gp_credit_uat.ci_2g_daily_packet_record definition

CREATE TABLE `ci_2g_daily_packet_record` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `report_date` varchar(10) DEFAULT NULL COMMENT '日期',
  `institution_code` varchar(14) DEFAULT NULL COMMENT '机构代码',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息类型;15：正常，16：变更，17：删除',
  `packet_num` int(11) DEFAULT NULL COMMENT '文件数',
  `record_num` int(11) DEFAULT NULL COMMENT '信息记录数',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代日报文记录表';


-- gp_credit_uat.ci_2g_delete_enterprise_finance_section definition

CREATE TABLE `ci_2g_delete_enterprise_finance_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `enter_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `enter_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `enter_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `year` varchar(4) DEFAULT NULL COMMENT '报表年份',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_sec_cat` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业财务信息整笔删除信息记录表';


-- gp_credit_uat.ci_2g_delete_enterprise_section definition

CREATE TABLE `ci_2g_delete_enterprise_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `name` varchar(160) DEFAULT NULL COMMENT '企业名称',
  `id_type` varchar(2) DEFAULT NULL COMMENT '企业证件类型',
  `id_no` varchar(80) DEFAULT NULL COMMENT '企业证件号码',
  `institution_code` varchar(20) DEFAULT NULL COMMENT '信息来源编码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业信息记录整笔删除段';


-- gp_credit_uat.ci_2g_enterprise_act_controller_section definition

CREATE TABLE `ci_2g_enterprise_act_controller_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `stuff_num` varchar(2) DEFAULT NULL COMMENT '主要组成人员个数',
  `name` varchar(160) DEFAULT NULL COMMENT '姓名',
  `id_type` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `id_no` varchar(80) DEFAULT NULL COMMENT '证件号码',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `type` varchar(1) DEFAULT NULL COMMENT '实际控制人身份类别',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息实际控制人段表';


-- gp_credit_uat.ci_2g_enterprise_base_info_section definition

CREATE TABLE `ci_2g_enterprise_base_info_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `nation_code` varchar(3) DEFAULT NULL COMMENT '国别代码',
  `regist_address` varchar(200) DEFAULT NULL COMMENT '登记地址',
  `district_code` varchar(6) DEFAULT NULL COMMENT '登记地行政区划代码',
  `startup_date` varchar(10) DEFAULT NULL COMMENT '成立日期',
  `license_valid` varchar(10) DEFAULT NULL COMMENT '营业许可证到期日',
  `bussiness_range` varchar(800) DEFAULT NULL COMMENT '业务范围',
  `industry_code` varchar(5) DEFAULT NULL COMMENT '行业分类代码',
  `economy_code` varchar(3) DEFAULT NULL COMMENT '经济类型代码',
  `enter_size` varchar(1) DEFAULT NULL COMMENT '企业规模',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息基本概况段表';


-- gp_credit_uat.ci_2g_enterprise_base_section definition

CREATE TABLE `ci_2g_enterprise_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `enter_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `enter_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `enter_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `guarantee_code` varchar(20) DEFAULT NULL COMMENT '信息来源编码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `maintain_org_code` varchar(14) DEFAULT NULL COMMENT '客户资料维护机构代码',
  `profile_source` varchar(1) DEFAULT NULL COMMENT '客户资料类型',
  `enter_status` varchar(1) DEFAULT NULL COMMENT '存续状态',
  `enter_type` varchar(2) DEFAULT NULL COMMENT '组织机构类型',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息基础段表';


-- gp_credit_uat.ci_2g_enterprise_contact_section definition

CREATE TABLE `ci_2g_enterprise_contact_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `district_code` varchar(6) DEFAULT NULL COMMENT '联系地址行政区划代码',
  `address` varchar(200) DEFAULT NULL COMMENT '联系地址',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `finance_phone` varchar(50) DEFAULT NULL COMMENT '财务部门联系电话',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息联系方式段表';


-- gp_credit_uat.ci_2g_enterprise_finance_cash_base_section definition

CREATE TABLE `ci_2g_enterprise_finance_cash_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `enter_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `enter_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `enter_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `maintain_org_code` varchar(14) DEFAULT NULL COMMENT '客户资料维护机构代码',
  `year` varchar(4) DEFAULT NULL COMMENT '报表年份',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_sec_cat` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业现金流量表信息基础段表';


-- gp_credit_uat.ci_2g_enterprise_finance_cash_section definition

CREATE TABLE `ci_2g_enterprise_finance_cash_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `total_operating_in` varchar(21) DEFAULT NULL COMMENT '经营活动现金流入小计',
  `total_operating_out` varchar(21) DEFAULT NULL COMMENT '经营活动现金流出小计',
  `net_operating` varchar(21) DEFAULT NULL COMMENT '经营活动产生的现金流量净额',
  `total_investing_in` varchar(21) DEFAULT NULL COMMENT '投资活动现金流入小计',
  `total_investing_out` varchar(21) DEFAULT NULL COMMENT '投资活动现金流出小计',
  `net_investing` varchar(21) DEFAULT NULL COMMENT '投资活动产生的现金流量净额',
  `total_financing_in` varchar(21) DEFAULT NULL COMMENT '筹集活动现金流入小计',
  `total_financing_out` varchar(21) DEFAULT NULL COMMENT '筹集活动现金流出小计',
  `net_financing` varchar(21) DEFAULT NULL COMMENT '筹集活动产生的现金流量净额',
  `effect` varchar(21) DEFAULT NULL COMMENT '汇率变动对现金及现金等价物的影响',
  `net_increase_in_cash` varchar(21) DEFAULT NULL COMMENT '现金及现金等价物净增加额',
  `initial_balance` varchar(21) DEFAULT NULL COMMENT '期初现金及现金等价物余额',
  `closing_balance` varchar(21) DEFAULT NULL COMMENT '期末现金及现金等价物余额',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业现金流量表段表';


-- gp_credit_uat.ci_2g_enterprise_finance_debt_base_section definition

CREATE TABLE `ci_2g_enterprise_finance_debt_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `enter_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `enter_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `enter_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `maintain_org_code` varchar(14) DEFAULT NULL COMMENT '客户资料维护机构代码',
  `year` varchar(4) DEFAULT NULL COMMENT '报表年份',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_sec_cat` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业资产负债表基本信息基础段表';


-- gp_credit_uat.ci_2g_enterprise_finance_debt_section definition

CREATE TABLE `ci_2g_enterprise_finance_debt_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `total_current_assets` varchar(20) DEFAULT NULL COMMENT '流动资产合计',
  `total_noncurrent_assets` varchar(20) DEFAULT NULL COMMENT '非流动资产合计',
  `total_assets` varchar(20) DEFAULT NULL COMMENT '资产总计',
  `total_current_liabilities` varchar(20) DEFAULT NULL COMMENT '流动负债合计',
  `total_noncurrent_liabilities` varchar(20) DEFAULT NULL COMMENT '非流动负债合计',
  `total_liabilities` varchar(20) DEFAULT NULL COMMENT '负债合计',
  `total_equity` varchar(20) DEFAULT NULL COMMENT '所有者权益合计',
  `total_equity_and_liabilities` varchar(20) DEFAULT NULL COMMENT '负债和所有者权益合计',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业资产负债表段表';


-- gp_credit_uat.ci_2g_enterprise_finance_profit_base_section definition

CREATE TABLE `ci_2g_enterprise_finance_profit_base_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `enter_name` varchar(160) DEFAULT NULL COMMENT '被担保人名称',
  `enter_id_type` varchar(2) DEFAULT NULL COMMENT '被担保人证件类型',
  `enter_id_no` varchar(80) DEFAULT NULL COMMENT '被担保人证件号码',
  `report_date` varchar(10) DEFAULT NULL COMMENT '信息报告日期',
  `report_point` varchar(2) DEFAULT NULL COMMENT '报告时点',
  `maintain_org_code` varchar(14) DEFAULT NULL COMMENT '客户资料维护机构代码',
  `year` varchar(4) DEFAULT NULL COMMENT '报表年份',
  `report_type` varchar(2) DEFAULT NULL COMMENT '报表类型',
  `report_sec_cat` varchar(1) DEFAULT NULL COMMENT '报表类型细分',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业利润表信息基础段表';


-- gp_credit_uat.ci_2g_enterprise_finance_profit_section definition

CREATE TABLE `ci_2g_enterprise_finance_profit_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `revenue_of_sales` varchar(20) DEFAULT NULL COMMENT '营业收入',
  `operating_profits` varchar(20) DEFAULT NULL COMMENT '营业利润',
  `profit_before_tax` varchar(20) DEFAULT NULL COMMENT '利润总额',
  `net_profit` varchar(20) DEFAULT NULL COMMENT '净利润',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业利润表段表';


-- gp_credit_uat.ci_2g_enterprise_main_stock_holder_section definition

CREATE TABLE `ci_2g_enterprise_main_stock_holder_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `stuff_num` varchar(2) DEFAULT NULL COMMENT '主要组成人员个数',
  `name` varchar(160) DEFAULT NULL COMMENT '姓名',
  `id_type` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `id_no` varchar(80) DEFAULT NULL COMMENT '证件号码',
  `inv_ratio` varchar(6) DEFAULT NULL COMMENT '出资比例',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `capital` varchar(15) DEFAULT NULL COMMENT '注册资本',
  `type` varchar(2) DEFAULT NULL COMMENT '出资人类型',
  `holder_type` varchar(1) DEFAULT NULL COMMENT '出资人身份类别',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息注册资本及主要出资人段表';


-- gp_credit_uat.ci_2g_enterprise_main_stuff_section definition

CREATE TABLE `ci_2g_enterprise_main_stuff_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `stuff_num` varchar(2) DEFAULT NULL COMMENT '主要组成人员个数',
  `name` varchar(60) DEFAULT NULL COMMENT '姓名',
  `id_type` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `id_no` varchar(40) DEFAULT NULL COMMENT '证件号码',
  `duty` varchar(1) DEFAULT NULL COMMENT '职位',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息主要组成人员段表';


-- gp_credit_uat.ci_2g_enterprise_other_info_section definition

CREATE TABLE `ci_2g_enterprise_other_info_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `id_num` varchar(2) DEFAULT NULL COMMENT '其他标识个数',
  `id_type` varchar(2) DEFAULT NULL COMMENT '企业身份标识类型',
  `id_no` varchar(80) DEFAULT NULL COMMENT '企业身份标识号码',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息其他标识段表';


-- gp_credit_uat.ci_2g_enterprise_sup_org_section definition

CREATE TABLE `ci_2g_enterprise_sup_org_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `name` varchar(160) DEFAULT NULL COMMENT '姓名',
  `id_type` varchar(2) DEFAULT NULL COMMENT '证件类型',
  `id_no` varchar(80) DEFAULT NULL COMMENT '证件号码',
  `last_date` varchar(10) DEFAULT NULL COMMENT '信息更新日期',
  `sup_org_type` varchar(1) DEFAULT NULL COMMENT '上级机构类型',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代企业基本信息上级机构段表';


-- gp_credit_uat.ci_2g_exception definition

CREATE TABLE `ci_2g_exception` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `project_id` varchar(32) DEFAULT NULL COMMENT '项目id',
  `column` varchar(300) DEFAULT NULL COMMENT '数据来源',
  `err_value` varchar(100) DEFAULT NULL COMMENT '异常数据',
  `err_msg` varchar(1000) DEFAULT NULL COMMENT '异常信息',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '插入时间',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代报文异常表';


-- gp_credit_uat.ci_2g_guarantee_resp_info definition

CREATE TABLE `ci_2g_guarantee_resp_info` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `account_status` varchar(1) DEFAULT NULL COMMENT '账户状态',
  `act_gua_balance` varchar(15) DEFAULT NULL COMMENT '在保余额',
  `balance_update_date` varchar(10) DEFAULT NULL COMMENT '余额变化日期',
  `five_category` varchar(1) DEFAULT NULL COMMENT '五级分类',
  `category_date` varchar(10) DEFAULT NULL COMMENT '五级分类认定日期',
  `risk_amount` varchar(15) DEFAULT NULL COMMENT '风险敞口',
  `compensatory_flag` varchar(1) DEFAULT NULL COMMENT '代偿标志',
  `close_date` varchar(10) DEFAULT NULL COMMENT '账户关闭日期',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代在保责任信息段表';


-- gp_credit_uat.ci_2g_packet_file definition

CREATE TABLE `ci_2g_packet_file` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `file_name` varchar(50) DEFAULT NULL COMMENT '文件名',
  `content` mediumblob COMMENT '文件内容',
  `generate_date` datetime DEFAULT NULL COMMENT '生成时间',
  `download_time` datetime DEFAULT NULL COMMENT '最后下载时间',
  `use_state` varchar(1) DEFAULT NULL COMMENT '可用标识',
  `record_type` varchar(3) DEFAULT NULL COMMENT '信息记录类型',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代报文文件表';


-- gp_credit_uat.ci_2g_packet_item definition

CREATE TABLE `ci_2g_packet_item` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `project_id` varchar(32) DEFAULT NULL COMMENT '项目id',
  `business_no` varchar(60) DEFAULT NULL COMMENT '业务编号',
  `packet_type` varchar(2) DEFAULT NULL COMMENT '报文类别;10正常，20变更，30删除',
  `item_content` text COMMENT '报文内容',
  `packet_file_id` varchar(32) DEFAULT NULL COMMENT '所在文件',
  `line_no` int(11) DEFAULT NULL COMMENT '所属文件行数',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '报文生成时间',
  `guarantee_code` varchar(14) DEFAULT NULL COMMENT '机构码',
  `send_status` varchar(1) DEFAULT NULL COMMENT '报送状态;1：已报送，2：报送失败',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代报文条目表';


-- gp_credit_uat.ci_2g_related_pay_person_section definition

CREATE TABLE `ci_2g_related_pay_person_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `person_num` varchar(2) DEFAULT NULL COMMENT '责任人个数',
  `person_type` varchar(1) DEFAULT NULL COMMENT '身份类别',
  `person_name` varchar(160) DEFAULT NULL COMMENT '责任人名称',
  `person_id_type` varchar(2) DEFAULT NULL COMMENT '责任人身份标识类型',
  `person_id_no` varchar(80) DEFAULT NULL COMMENT '责任人身份标识号码',
  `duty_type` varchar(1) DEFAULT NULL COMMENT '还款责任人类型',
  `duty_amount` varchar(15) DEFAULT NULL COMMENT '还款责任金额',
  `union_flag` varchar(1) DEFAULT NULL COMMENT '联保标识',
  `contract_no` varchar(60) DEFAULT NULL COMMENT '保证合同号码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代相关还款责任人段表';


-- gp_credit_uat.ci_2g_section definition

CREATE TABLE `ci_2g_section` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `project_id` varchar(32) DEFAULT NULL COMMENT '项目表id',
  `reference_id` varchar(32) DEFAULT NULL COMMENT '关联id',
  `business_time` datetime DEFAULT NULL COMMENT '业务发生时间',
  `insert_time` datetime DEFAULT NULL COMMENT '生成报文时间',
  `section_flag` varchar(1) DEFAULT NULL COMMENT '段标',
  `message` text DEFAULT null COMMENT '段报文',
  `section_category` varchar(2) DEFAULT NULL COMMENT '信息段分类;除被担保人、债权人、反担保人信息段，其他信息段该字段始终为1',
  `item_id` varchar(32) DEFAULT NULL COMMENT '报文条目id',
  `section_no` int(11) DEFAULT NULL COMMENT '所在报文第几段',
  `send_status` varchar(1) DEFAULT NULL COMMENT '报送状态;1：已报送，2：报送失败',
  `actual_business_date` varchar(10) DEFAULT NULL COMMENT '实际业务发生日期',
  `first_report_date` varchar(10) DEFAULT NULL COMMENT '首次报送征信日期',
  `contract_id` varchar(32) DEFAULT NULL COMMENT '合同表id',
  `enter_id` varchar(32) DEFAULT NULL COMMENT '企业表id',
  `section_name` varchar(100) DEFAULT NULL COMMENT '段名',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户号',
  `revision` int(11) DEFAULT NULL COMMENT '乐观锁',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4,COLLATE=utf8mb4_general_ci COMMENT='二代公共信息段表';

CREATE INDEX ci_2g_packet_item_packet_file_id_IDX USING BTREE ON ci_2g_packet_item (packet_file_id);
CREATE INDEX ci_2g_section_item_id_IDX USING BTREE ON ci_2g_section (item_id);
CREATE INDEX ci_2g_section_project_id_IDX USING BTREE ON ci_2g_section (project_id);
CREATE INDEX ci_2g_section_reference_id_IDX USING BTREE ON ci_2g_section (reference_id);
CREATE INDEX ci_2g_section_enter_id_IDX USING BTREE ON ci_2g_section (enter_id);
CREATE INDEX contract_info_process_id_IDX USING BTREE ON contract_info (process_id);
CREATE INDEX contract_info_project_id_IDX USING BTREE ON contract_info (project_id);
CREATE INDEX cust_enterprise_info_cust_id_IDX USING BTREE ON cust_enterprise_info (cust_id);
CREATE INDEX cust_enterprise_info_cust_no_IDX USING BTREE ON cust_enterprise_info (cust_no);
CREATE INDEX project_info_cust_no_IDX USING BTREE ON project_info (cust_no);
CREATE INDEX project_info_project_id_IDX USING BTREE ON project_info (project_id);
CREATE INDEX project_info_project_no_IDX USING BTREE ON project_info (project_no);
CREATE INDEX finance_report_rh_id_IDX USING BTREE ON finance_report_rh (id);
CREATE INDEX ci_2g_packet_file_generate_date_IDX USING BTREE ON ci_2g_packet_file (generate_date);
CREATE INDEX project_info_import_date_num_IDX USING BTREE ON project_info (import_date,import_num);
CREATE INDEX cust_enterprise_info_import_date_num_IDX USING BTREE ON cust_enterprise_info (import_date,import_num);
CREATE INDEX contract_info_import_date_num_IDX USING BTREE ON contract_info (import_date,import_num);
CREATE INDEX finance_report_info_reference_id_IDX USING BTREE ON finance_report_info (reference_id);