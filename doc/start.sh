#!/bin/bash
# 启动jvm工程

DisPlayMain() {
    clear
    echo ""
    echo "|------------------------------|"
    echo "|        jvm服务进程            |"
    echo "|   =====服务启停脚本=====      |"
    echo "|                               |"
    echo "|     1、 启动credit系统           |"
    echo "|                               |"
    echo "|     2、 停止credit系统           |"
    echo "|                               |"
    echo "|     0|q、退出                 |"
    echo "|                               |"
    echo "|------------------------------|"
    echo "请选择：\c"
}

StartProcess() {
    # 启动jvm进程
    echo "检查是否包含进程"
    num=0
    num=$(ps -ef | grep -v grep | grep -v 'sh' | grep 'credit' | wc -l)

    if [ $num -ne 0 ]; then
        echo "$(date +"%Y%m%d %H:%M:%S") 服务进程未完全停止，请退出脚本后检查服务进程"
        exit
    else
        echo "$(date +"%Y%m%d %H:%M:%S") 服务已完全停止，即将启动服务"
    fi

    # 执行启动脚本
    echo "sh /app/credit/credit_start.sh"
    sh /app/credit/credit_start.sh

    echo "正在启动中......"
    sleep 2

    # 检查是否启动成功
    num=0
    num=$(ps -ef | grep -v grep | grep -v 'sh' | grep 'credit' | wc -l)
    if [ $num -gt 0 ]; then
        echo "$(date +"%Y%m%d %H:%M:%S") 服务进程启动成功，执行tail -f /app/credit/credit.log 查看启动日志"
        exit
    else
        echo "$(date +"%Y%m%d %H:%M:%S") 服务启动失败!!"
    fi
}

StopProcess() {
    # 查找并杀死名为 credit 的进程

    # 查找进程
    echo "查找进程"
    ps -ef | grep '[credit]'

    # 获取进程ID
    PID=$(ps -ef | grep -w 'credit' | grep -v 'grep' | grep -v 'sh' | awk '{ print $2}')
    echo "获取进程ID"
    echo $PID

    # 向进程发送SIGTERM信号
    echo "向进程发送SIGTERM信号"
    kill $PID

    # 等待进程退出
    echo "等待进程退出中"
    sleep 5

    # 检查进程是否被杀死
    if [ $? -eq 0 ]; then
        echo "Process has killed successfully."
    else
        echo "Failed to kill the process!!"
    fi
}

while true; do
    DisPlayMain
    read choice

    case $choice in
        1)
            clear
            StartProcess
            echo ""
            echo "按任意键继续... \c"
            read
            ;;
        2)
            clear
            StopProcess
            echo ""
            echo "按任意键继续... \c"
            read
            ;;
        0 | q)
            break
            ;;
    esac
done