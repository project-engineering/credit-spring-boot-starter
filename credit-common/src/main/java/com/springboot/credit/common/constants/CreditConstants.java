package com.springboot.credit.common.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 二代征信模块常量类
 * @author liuc
 * @date 2023-12-16
 */
public class CreditConstants {
    /**
     * 信息记录类型 440-企业担保账户信息记录
     */
    public static String RECORD_TYPE_GUARANTEE_ACCOUNT = "440";

    /**
     * 信息记录类型 442-企业担保账户按段更正请求记录
     */
    public static String RECORD_TYPE_GUARANTEE_ACCOUNT_ALTER = "442";

    /**
     * 信息记录类型 443-企业担保账户按段删除记录
     */
    public static String RECORD_TYPE_GUARANTEE_ACCOUNT_SECTION_DELETE = "443";

    /**
     * 信息记录类型 444-企业担保账户整笔删除记录
     */
    public static String RECORD_TYPE_GUARANTEE_ACCOUNT_DELETE = "444";

    /**
     * 信息记录类型 510-抵质押合同信息记录
     */
    public static String RECORD_TYPE_MORTEGATE_CONTRACT = "510";

    /**
     * 信息记录类型 310-企业基本信息记录
     */
    public static String RECORD_TYPE_ENTERPRISE_INFO = "310";

    /**
     * 信息记录类型 314-企业基本信息整笔删除请求记录
     */
    public static String RECORD_TYPE_ENTERPRISE_DELETE = "314";

    /**
     * 信息记录类型 410-企业借贷账户信息记录
     */
    public static String RECORD_TYPE_DEBITCREDIT_ACCOUNT = "410";

    /**
     * 信息记录类型 412-企业借贷账户按段更正请求记录
     */
    public static String RECORD_TYPE_C1_ACCOUNT_ALTER = "412";

    /**
     * 信息记录类型 610-企业资产负债表信息记录
     */
    public static String RECORD_TYPE_ENTERPRISE_BALANCE_SHEET_INFO = "610";

    /**
     * 信息记录类型 620-企业利润表信息记录
     */
    public static String RECORD_TYPE_ENTERPRISE_PROFIT_INFO = "620";

    /**
     * 信息记录类型 630-企业现金流量表信息记录
     */
    public static String RECORD_TYPE_ENTERPRISE_CASH_INFO = "630";

    /**
     * 信息记录类型 614-企业资产负债表信息整笔删除请求记录
     */
    public static String RECORD_TYPE_ENTERPRISE_BALANCE_DELETE = "614";

    /**
     * 信息记录类型 624-企业利润表信息整笔删除请求记录
     */
    public static String RECORD_TYPE_ENTERPRISE_PROFIT_DELETE = "624";

    /**
     * 信息记录类型 634-企业现金流量表信息整笔删除请求记录
     */
    public static String RECORD_TYPE_ENTERPRISE_CASH_DELETE = "634";

    /**
     * 账户类型 G1-融资担保账户
     */
    public static String ACCOUNT_TYPE_FINANCE = "G1";

    /**
     * 账户类型 G2-非融资担保账户
     */
    public static String ACCOUNT_TYPE_NON_FINANCE = "G2";

    /**
     * 账户类型 G3-支付担保账户
     */
    public static String ACCOUNT_TYPE_PAY = "G3";

    /**
     * 账户类型 C1-催收账户
     */
    public static String ACCOUNT_TYPE_C1 = "C1";

    /**
     * 报告时点 10-新开户，首次上报
     */
    public static String REPORT_POINT_OPEN = "10";

    /**
     * 报告时点 20-账户关闭
     */
    public static String REPORT_POINT_CLOSE = "20";

    /**
     * 报告时点 30-在保责任变化
     */
    public static String REPORT_POINT_DUTY_CHANGE = "30";

    /**
     * 报告时点 40-五级分类调整
     */
    public static String REPORT_POINT_FIVE_CATEGORY = "40";

    /**
     * 报告时点 50-其他信息变化（包括相关还款责任人、抵质押合同等信息发生变化）
     */
    public static String REPORT_POINT_OTHER_CHANGE = "50";

    /**
     * 报告时点 10-合同生效
     */
    public static String REPORT_POINT_CONTRACT_ACTIVE = "10";

    /**
     * 报告时点 20-合同到期/失效
     */
    public static String REPORT_POINT_CONTRACT_INACTIVE = "20";

    /**
     * 报告时点 30-抵质押物变更
     */
    public static String REPORT_POINT_CG_CHANGE = "30";

    /**
     * 报告时点 10-新增客户资料/首次上报
     */
    public static String REPORT_POINT_ENTERPRISE_CREATE = "10";

    /**
     * 报告时点 20-更新客户资料
     */
    public static String REPORT_POINT_ENTERPRISE_UPDATE = "20";

    /**
     * 报告时点 10-首次获取财务报表
     */
    public static String REPORT_POINT_FINANCE_CREATE = "10";

    /**
     * 报告时点 20-更新财务报表
     */
    public static String REPORT_POINT_FINANCE_UPDATE = "20";

    /**
     * 报告时点 33-实际还款
     */
    public static String REPORT_POINT_C1_ACT_REPAY = "33";

    /**
     * 报告时点 41-C1账户五级分类调整
     */
    public static String REPORT_POINT_C1_FIVE_CATEGORY = "41";

    /**
     * 报告时点 49-C1其他调整
     */
    public static String REPORT_POINT_C1_OTHER = "49";

    /**
     * 信息段所属类别，44-担保交易信息
     */
    public static String SECTION_CATEGORY_GUARANTEE = "44";

    /**
     * 信息段所属类别，31-企业基本信息
     */
    public static String SECTION_CATEGORY_ENTERPRISE = "31";

    /**
     * 信息段所属类别，41-借贷交易信息
     */
    public static String SECTION_CATEGORY_LOAN = "41";

    /**
     * 信息段所属类别，51-抵质押物信息
     */
    public static String SECTION_CATEGORY_MORTGAGE = "51";

    /**
     * 信息段所属类别，61-企业资产负债表信息
     */
    public static String SECTION_CATEGORY_BALANCE_SHEET = "61";

    /**
     * 信息段所属类别，62-企业利润表信息
     */
    public static String SECTION_CATEGORY_PROFIT = "62";

    /**
     * 信息段所属类别，63-企业现金流量表信息
     */
    public static String SECTION_CATEGORY_CASH = "63";

    /**
     * 报文头
     */
    public static String HEADER_SECTION = "A";

    /**
     * 基础段
     */
    public static String BASE_SECTION = "B";

    /**
     * 基本信息段
     */
    public static String BASE_INFO_SECTION = "C";

    /**
     * 在保责任信息段
     */
    public static String GUARANTEE_RESP_INFO_SECTION = "D";

    /**
     * 相关还款责任人段
     */
    public static String RELATED_PAY_PERSON_SECTION = "E";

    /**
     * 抵质押物信息段（预留，不报）
     */
    public static String MORTGAGE_SECTION = "F";

    /**
     * 抵押物信息段
     */
    public static String MORTGAGE_INFO_SECTION = "E";

    /**
     * 质物信息段
     */
    public static String PLEDGE_INFO_SECTION = "F";

    /**
     * 授信额度信息段（没有用到）
     */
    public static String CREDIT_SECTION = "G";

    /**
     * 企业基本信息记录，其他标识段
     */
    public static String ENTERPRISE_OTHER_INFO_SECTION = "C";

    /**
     * 企业基本信息记录，基本概况信息段
     */
    public static String ENTERPRISE_BASE_INFO_SECTION = "D";

    /**
     * 企业基本信息记录，主要组成人员段
     */
    public static String ENTERPRISE_MAIN_STUFF_SECTION = "E";

    /**
     * 企业基本信息记录，联系方式段，规范里没写，推断的，因为上一个段的段标识H
     */
    public static String ENTERPRISE_CONTACT_SECTION = "I";

    /**
     * 企业基本信息记录，注册资本及主要出资人信息段
     */
    public static String ENTERPRISE_MAIN_STOCK_HOLDER_SECTION = "F";

    /**
     * 企业基本信息记录，实际控制人信息段
     */
    public static String ENTERPRISE_ACT_CONTROL_SECTION = "G";

    /**
     * 企业基本信息记录，上级机构段
     */
    public static String ENTERPRISE_SUP_ORG_SECTION = "H";

    /**
     * 删除请求记录信息段，二代没有规定，沿用一代的标志
     */
    public static String DELETE_RECORD_SECTION = "S";

    /**
     * 按段更正类请求记录 基础段，二代没有规定，自己编的，数据库中区分段用
     */
    public static String ALTER_RECORD_SECTION = "U";

    /**
     * 企业借贷交易信息-还款表现信息段
     */
    public static String REPAY_PERFORMANCE_SECTION = "H";

    /**
     * 企业借贷交易信息-相关还款责任人段
     */
    public static String C1_RELATED_PAY_PERSON_SECTION = "D";

    /**
     * 2007版资产负债表段，利润表段，资产负债表段（规范里面没看到，推测应该是D。。。）
     */
    public static String ENTERPRISE_FINANCE_BALANCE = "D";

    /**
     * 2007版利润表段（规范里面没看到，推测应该是D。。。）
     */
    public static String ENTERPRISE_FINANCE_PROFIT = "D";

    /**
     * 2007版现金流量表段
     */
    public static String ENTERPRISE_FINANCE_CASH = "D";

    /**
     * 担保业务大类 1-融资担保
     */
    public static String BUSI_TYPE_FIRST_FINANCE = "1";

    /**
     * 担保业务大类 2-非融资担保
     */
    public static String BUSI_TYPE_FIRST_NON_FINANCE = "2";

    /**
     * 业务系统中的默认币种
     */
    public static String CURRENCY_TYPE_DEFAULT = "CNY";

    /**
     * 业务系统counterGuarantorInfo表保证人类型--法人
     */
    public static String BUSI_COU_TYPE_LAW = "1";

    /**
     * 业务系统counterGuarantorInfo表保证人类型--自然人
     */
    public static String BUSI_COU_TYPE_PERSON = "2";

    /**
     * 相关还款责任人身份类型--法人
     */
    public static String PERSON_TYPE_LAW = "2";

    /**
     * 相关还款责任人身份类型--自然人
     */
    public static String PERSON_TYPE_PERSON = "1";

    /**
     * 企业证件类型--中征码（原贷款卡编码），对应业务系统中的"c"
     */
    public static String ENT_ID_TYPE_C = "10";

    /**
     * 企业证件类型--统一社会信用代码，对应业务系统中的"u"
     */
    public static String ENT_ID_TYPE_U = "20";

    /**
     * 企业证件类型--组织结构代码，对应业务系统中的"a"
     */
    public static String ENT_ID_TYPE_A = "30";

    /**
     * 企业证件类型--事业单位证书号
     */
    public static String ENT_ID_TYPE_INSTITUTION = "02";

    /**
     * 企业证件类型--社会团体登记证号
     */
    public static String ENT_ID_TYPE_SOCIAL = "03";

    /**
     * 企业证件类型--民办非企业登记号
     */
    public static String ENT_ID_TYPE_NON_ENT = "04";

    /**
     * 企业证件类型--基金会登记号
     */
    public static String ENT_ID_TYPE_FUNDATION = "05";

    /**
     * 企业证件类型--宗教证书登记号
     */
    public static String ENT_ID_TYPE_RELIGION = "06";

    /**
     * 账户状态 1-正常
     */
    public static String ACCOUNT_STATUS_NORMAL = "1";

    /**
     * 账户状态 2-关闭
     */
    public static String ACCOUNT_STATUS_CLOSE = "2";

    /**
     * C1账户状态 10-正常活动
     */
    public static String C1_ACCOUNT_STATUS_NORMAL = "10";

    /**
     * C1账户状态 21-关闭
     */
    public static String C1_ACCOUNT_STATUS_CLOSE = "21";

    /**
     * C1账户状态 31-呆账（已核销）
     */
    public static String C1_ACCOUNT_STATUS_BAD_END = "31";

    /**
     * C1账户状态 32-呆账（进入核销认定程序，等待核销）
     */
    public static String C1_ACCOUNT_STATUS_BAD_PROCESSING = "32";

    /**
     * 五级分类 1-正常
     */
    public static String FIVE_CATEGORY_PERFORMING = "1";

    /**
     * 账户状态 2-关注
     */
    public static String FIVE_CATEGORY_SPECIAL_MENTION = "2";

    /**
     * 账户状态 3-次级
     */
    public static String FIVE_CATEGORY_SUBSTANDARD = "3";

    /**
     * 账户状态 4-可疑
     */
    public static String FIVE_CATEGORY_DOUBTFUL = "4";

    /**
     * 账户状态 5-损失
     */
    public static String FIVE_CATEGORY_LOSS = "5";

    /**
     * 账户状态 9-未分类
     */
    public static String FIVE_CATEGORY_NONE = "9";

    /**
     * 代偿标志1-是
     */
    public static String COMPENSATORY_FLAG_YES = "1";
    /**
     * 代偿标志0-否
     */
    public static String COMPENSATORY_FLAG_NO = "0";

    /**
     * 整笔代偿标志1-是
     */
    public static String LUMP_SUM_COMPENSATORY_YES = "1";

    /**
     * 整笔代偿标志0-否
     */
    public static String LUMP_SUM_COMPENSATORY_NO = "0";

    /**
     * 状态位：有效
     */
    public static String STATE_VALID = "1";

    /**
     * 状态位：无效
     */
    public static String STATE_INVALID = "2";

    /**
     * 合同类型1-抵押合同
     */
    public static String CONTRACT_TYPE_MORTGAGE = "1";

    /**
     * 合同类型2-质押合同
     */
    public static String CONTRACT_TYPE_PLEDGE = "2";

    /**
     * 最高额担保标志0-否
     */
    public static String TOP_FLAG_NO = "0";

    /**
     * 抵质押合同状态1-有效
     */
    public static String CONTRACT_STATUS_ACTIVE = "1";

    /**
     * 抵质押合同状态2-到期/失效
     */
    public static String CONTRACT_STATUS_INACTIVE = "2";

    /**
     * 报文生成、报送机构代码：由总公司统一生成报文并报送
     */
    public static String INSTITUTION_CODE_REPORT = "X2201000015603";

    /**
     * 报文格式版本号
     */
    public static String VERSION = "2.0.0";

    /**
     * 信息记录模板类型，空值--2个空格
     */
    public static String RECORD_TEMPLATE_TYPE_NONE = "  ";

    /**
     * 信息记录模板类型，企业基本信息基础模板--10
     */
    public static String RECORD_TEMPLATE_TYPE_ENT = "10";

    /**
     * 换行符 \r\n
     */
    public static String LINE_SEPARATOR = "\r\n";

    /**
     * 业务系统中的注册类型，400-个体经营
     */
    public static String REGIST_TYPE_PERSON = "400";

    /**
     * 业务系统中的注册类型，500-其他
     */
    public static String REGIST_TYPE_OTHER = "500";

    /**
     * 组织机构类型代码 1-企业
     */
    public static String ENTER_TYPE_ENTERPRISE = "1";

    /**
     * 组织机构类型代码 A-个体工商户
     */
    public static String ENTER_TYPE_PERSON = "A";

    /**
     * 企业注册有效期长期关键字
     */
    public static String ENTER_LIC_VALID_LONG = "期";

    /**
     * 正常报文
     */
    public static String MESSAGETYPE_NORMAL = "10";

    /**
     * 报文类别：变更，来自一代系统，二代规范没有提
     */
    public static String MESSAGETYPE_ALTER = "20";

    /**
     * 报文类别：删除，来自一代系统，二代规范没有提
     */
    public static String MESSAGETYPE_DELETE = "30";

    /**
     * 主要组成成员职务类型，1-法定代表人/非法人组织负责人
     */
    public static String DUTY_LEGAL_REPRESENTATIVE = "1";

    /**
     * 主要组成成员职务类型，2-董事长/执行董事
     */
    public static String DUTY_CHAIRMAN = "2";

    /**
     * 主要组成成员职务类型，5-监事长/执行监事
     */
    public static String DUTY_CHIEF_SUPERVISOR = "5";

    /**
     * 数据提供机构区段码，就用总部的，全部通过总部来报
     */
    public static String CI_UNIT_CODE = "X2201000015603";

    /**
     * 二代系统给分配的区段码，14位，用于机构代码，同为14位
     */
    public static Map<String, String> CI_ORG_CODE_MAP = new HashMap<String, String>();

    /**
     * 担保业务种类细分
     */
    public static Map<String, String> busiTypeSecondMap = new HashMap<String, String>();

    /**
     * 担保业务大类
     * key是担保业务种类细分，value是大类
     */
    public static Map<String, String> busiTypeFirstMap = new HashMap<String, String>();

    /**
     * 自然人证件类型对照表
     * key是系统中（业务系统）证件类型，value是二代系统证件类型
     */
    public static Map<String, String> idTypeMap = new HashMap<String, String>();

    /**
     * 五级分类对应
     */
    public static Map<String, String> fiveCategoryMap = new HashMap<String, String>();

    /**
     * 主要组成人员职务
     */
    public static Map<String, String> dutyMap = new HashMap<String, String>();

    /**
     * 担保业务大类显示名称
     */
    public static Map<String, String> busiTypeFirstShowMap = new HashMap<String, String>();

    /**
     * 反担保方式名称
     */
    public static Map<String, String> cgTypeShowMap = new HashMap<String, String>();

    /**
     * 账户状态显示名称
     */
    public static Map<String, String> projectStatusShowMap = new HashMap<String, String>();

    /**
     * 五级分类显示名称
     */
    public static Map<String, String> fiveCategoryShowMap = new HashMap<String, String>();

    /**
     * 代偿（垫款）标志显示名称
     */
    public static Map<String, String> compensatoryFlagShowMap = new HashMap<String, String>();

    /**
     * C1账户状态显示名称
     */
    public static Map<String, String> c1AccountStatusShowMap = new HashMap<String, String>();

    /**
     * 相关还款责任人类型显示名称
     */
    public static Map<String, String> relatedPayPersonTypeShowMap = new HashMap<String, String>();

    /**
     * 企业证件类型显示名称
     */
    public static Map<String, String> enterpriseIdTypeShowMap = new HashMap<String, String>();

    static {
        //用于报送信息记录中的机构代码，和区段码相同，但报送记录中的区段码只能用总部的
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_ZONG, "X2201000015603"); // 总公司
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_L, "X2101000273905"); // 辽
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_J, "X2201000108951"); // 吉
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_H, "X2301000389219"); // 黑
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_N, "X1501000190053"); // 内
        CI_ORG_CODE_MAP.put(BpConstants.DEPART_F_DL, "X2100010000309"); // 大连
        CI_ORG_CODE_MAP.put("90000000", "X2201000015603"); // 进出口担保业务部，历史问题，为了避免报错。。。归为总公司

        busiTypeSecondMap.put("01", "贷款担保");
        busiTypeSecondMap.put("02", "票据承兑担保");
        busiTypeSecondMap.put("03", "贸易融资担保");
        busiTypeSecondMap.put("04", "项目融资担保");
        busiTypeSecondMap.put("05", "信用证担保");
        busiTypeSecondMap.put("06", "其他融资性担保");
        busiTypeSecondMap.put("07", "诉讼保全担保");
        busiTypeSecondMap.put("08", "履约担保");
        busiTypeSecondMap.put("09", "其他非融资性担保");
        busiTypeSecondMap.put("10", "债券发行担保");
        busiTypeSecondMap.put("11", "再担保");


        busiTypeFirstMap.put("01", "1");
        busiTypeFirstMap.put("02", "1");
        busiTypeFirstMap.put("03", "1");
        busiTypeFirstMap.put("04", "1");
        busiTypeFirstMap.put("05", "1");
        busiTypeFirstMap.put("06", "1");
        busiTypeFirstMap.put("10", "1");
        busiTypeFirstMap.put("07", "2");
        busiTypeFirstMap.put("08", "2");
        busiTypeFirstMap.put("09", "2");
        //新增
        busiTypeFirstMap.put("11", "3");
        busiTypeFirstMap.put("12", "4");
        busiTypeFirstMap.put("51", "6");
        busiTypeFirstMap.put("62", "6");
        busiTypeFirstMap.put("71", "7");
        busiTypeFirstMap.put("72", "7");


        idTypeMap.put("0", "10");//原身份证，对应二代系统身份证
        idTypeMap.put("1", "1");//户口簿，没有变化
        idTypeMap.put("2", "2");//护照，没有变化
        idTypeMap.put("3", "20");//原军官证，对应到二代军人身份证件
        idTypeMap.put("4", "20");//原士兵证，对应到二代军人身份证件
        idTypeMap.put("5", "5");//港澳居民往来内地通行证，没变化
        idTypeMap.put("6", "6");//台湾同袍来往内地通行证，没变化
        idTypeMap.put("7", "10");//原临时身份证，对应到二代 居民身份证及其他以公民身份证号为标识的证件
        idTypeMap.put("8", "8");//外国人居留证，没变化
        idTypeMap.put("9", "9");//警官证，没变化
        idTypeMap.put("X", "X");//其他证件，没有变化
        idTypeMap.put("A", "A");//二代新增类型，香港身份证
        idTypeMap.put("B", "B");//二代新增类型，澳门身份证
        idTypeMap.put("C", "C");//二代新增类型，台湾身份证
        idTypeMap.put("20", "20");//二代新增类型，军人身份证件

        idTypeMap.put("c", "10");//中征码/贷款卡
        idTypeMap.put("d", "20");//机构信用代码/统一社会信用代码
        idTypeMap.put("a", "30");//组织机构代码证
        idTypeMap.put("u", "20");//统一社会信用代码
        //业务系统中的z跟二代征信文档中的数据匹配不上，所以归类为二代征信的码值 X-其他证件
        // z-金融机构代码
        idTypeMap.put("z", "X");
        idTypeMap.put("x", "X");

        //ZC-正常
        fiveCategoryMap.put("ZC", "1");
        //GZ-关注
        fiveCategoryMap.put("GZ", "2");
        //CJ-次级
        fiveCategoryMap.put("CJ", "3");
        //KY-可疑
        fiveCategoryMap.put("KY", "4");
        //SS-损失
        fiveCategoryMap.put("SS", "5");
        fiveCategoryMap.put("未分类", "9");

        //实际上用不到这个映射，留着作为以后对照参考
        //dutyMap.put("5", "1");//法定代表人
        dutyMap.put("1", "1");//法定代表人/企业实际控制者
        dutyMap.put("2", "4");//董事长
        dutyMap.put("3", "2");//总经理/主要负责人
        dutyMap.put("4", "3");//财务负责人
        dutyMap.put("5", "5");//监事长/执行监事
        dutyMap.put("6", "9");//其他懂事、监事及高级管理人员
        dutyMap.put("9", "9");//其他懂事、监事及高级管理人员

        busiTypeFirstShowMap.put("1", "融资担保");
        busiTypeFirstShowMap.put("2", "非融资担保");
        busiTypeFirstShowMap.put("3", "再担保");

        cgTypeShowMap.put("0", "信用/免担保");
        cgTypeShowMap.put("1", "保证");
        cgTypeShowMap.put("2", "质押");
        cgTypeShowMap.put("3", "抵押");
        cgTypeShowMap.put("4", "组合");

        projectStatusShowMap.put("1", "正常");
        projectStatusShowMap.put("2", "关闭");

        fiveCategoryShowMap.put("1", "正常");
        fiveCategoryShowMap.put("2", "关注");
        fiveCategoryShowMap.put("3", "次级");
        fiveCategoryShowMap.put("4", "可疑");
        fiveCategoryShowMap.put("5", "损失");
        fiveCategoryShowMap.put("9", "未分类");

        compensatoryFlagShowMap.put("0", "否");
        compensatoryFlagShowMap.put("1", "是");

        c1AccountStatusShowMap.put("10", "正常");
        c1AccountStatusShowMap.put("20", "关闭");
        c1AccountStatusShowMap.put("31", "呆账（已核销）");
        c1AccountStatusShowMap.put("32", "呆账（进入核销认定程序，等待核销）");

        relatedPayPersonTypeShowMap.put("1", "自然人");
        relatedPayPersonTypeShowMap.put("2", "组织机构");

        enterpriseIdTypeShowMap.put("10", "贷款卡/中征码");
        enterpriseIdTypeShowMap.put("20", "统一社会信用代码");
        enterpriseIdTypeShowMap.put("30", "组织结构代码");
    }

    /**
     * 报送状态 1-已报送
     */
    public static final String SEND_STATUS_1 = "1";
    /**
     * 报送状态 2-报送失败
     */
    public static final String SEND_STATUS_2 = "2";

    /** 担保方式:1-抵押**/
    public static final String GUAR_MODE_1 = "1";
    /** 担保方式:2-质押**/
    public static final String GUAR_MODE_2 = "2";
    /** 担保方式:3-保证**/
    public static final String GUAR_MODE_3 = "3";
    /** 担保方式:9-其它**/
    public static final String GUAR_MODE_9 = "9";
    /** 反担保措施状态:0-未生效**/
    public static final String COUNTER_GUARANTEE_STATUS_0 = "0";
    /** 反担保措施状态:1-已生效**/
    public static final String COUNTER_GUARANTEE_STATUS_1 = "1";
    /** 反担保措施状态:2-已释放**/
    public static final String COUNTER_GUARANTEE_STATUS_2 = "2";
    /** 反担保措施状态:3-暂存中**/
    public static final String COUNTER_GUARANTEE_STATUS_3 = "3";

    /** 借款人类型:1-个人 **/
    public static final String CUST_TYPE_1 = "1";
    /** 借款人类型:2-企业 **/
    public static final String CUST_TYPE_2 = "2";

    /** 担保人类型:1-企业或其他组织 **/
    public static final String GUAR_TYPE_1 = "1";
    /** 担保人类型:2-自然人 **/
    public static final String GUAR_TYPE_2 = "2";

    /** 是否长期:0-否 **/
    public static final String FOREVER_FLAG_0 = "0";
    /** 是否长期:1-是 **/
    public static final String FOREVER_FLAG_1 = "1";

    /** 可用状态:0-否 **/
    public static final String USE_STATUS_0 = "0";
    /** 可用状态:1-是 **/
    public static final String USE_STATUS_1 = "1";

    /** 审批状态:00-审批中 **/
    public static final String APPROVAL_STATUS_00 = "00";
    /** 审批状态:10-审批通过 **/
    public static final String APPROVAL_STATUS_10 = "10";
    /** 审批状态:20-审批拒绝 **/
    public static final String APPROVAL_STATUS_20 = "20";
    /** 是否整笔代偿:0-否 **/
    public static final String WHOLE_COMP_FLAG_0 = "0";
    /** 是否整笔代偿:1-是 **/
    public static final String WHOLE_COMP_FLAG_1 = "1";

    /**
     * 资产负债表（工业）资产类别：流动资产 01
     */
    public static final String DEBTASSET_INDUSTRYTYPE_CURRENTASSETS = "01";

    /**
     * 资产负债表（工业）资产类别：非流动资产 02
     */
    public static final String DEBTASSET_INDUSTRYTYPE_NONCURRENTASSETS = "02";

    /**
     * 资产负债表（工业）负债类别：流动负债 11
     */
    public static final String DEBTASSET_INDUSTRYTYPE_CURRENTDEBTS = "11";

    /**
     * 资产负债表（工业）负债类别：非流动负债 12
     */
    public static final String DEBTASSET_INDUSTRYTYPE_NONCURRENTDEBTS = "12";

    /**
     * 资产负债表（工业）负债类别：所有者权益 13
     */
    public static final String DEBTASSET_INDUSTRYTYPE_OWNEREQUITIES = "13";

    /**
     * 流动资产合计
     */
    public static final String SUBJECT_TOTAL_CURRENT_ASSETS = "totalCurrentAssets";
    /**
     * 非流动资产合计
     */
    public static final String SUBJECT_TOTAL_NON_CURRENT_ASSETS = "totalNonCurrentAssets";
    /**
     * 资产总计
     */
    public static final String SUBJECT_ASSETS_TOTAL = "assetsTotal";
    /**
     * 流动负债合计
     */
    public static final String SUBJECT_TOTAL_CURRENT_DEBT = "totalCurrentDebt";
    /**
     * 非流动负债合计
     */
    public static final String SUBJECT_TOTAL_NON_CURRENT_DEBT = "totalNonCurrentDebt";
    /**
     * 负债合计
     */
    public static final String SUBJECT_DEBT_TOTAL = "debtTotal";
    /**
     * 负债和所有者权益合计
     */
    public static final String SUBJECT_DEBT_OWER_TOTAL = "debtOwerTotal";
    /**
     * 所有者权益合计
     */
    public static final String SUBJECT_OWER_EQUITY_TOTAL = "owerEquityTotal";
    /**
     * 经营活动现金流入小计
     */
    public static final String SUBJECT_TOTAL_CASH_RECEIVED = "totalCashReceived";
    /**
     * 经营活动现金流出小计
     */
    public static final String SUBJECT_TOTAL_OPERATING_CASH = "totalOperatingCash";
    /**
     * 经营活动产生的现金流量净额
     */
    public static final String SUBJECT_ACTIVITY_CASH = "activityCash";
    /**
     * 投资活动现金流入小计
     */
    public static final String SUBJECT_TOTAL_ACTIVITY_CASH = "totalActivityCash";
    /**
     * 投资活动现金流出小计
     */
    public static final String SUBJECT_TOTAL_ACTIVITY_PAY_CASH = "totalActivityPayCash";
    /**
     * 投资活动产生的现金流量净额
     */
    public static final String SUBJECT_INVESTING_ACTIVITY_CASH = "investingActivityCash";
    /**
     * 筹资活动现金流入小计
     */
    public static final String SUBJECT_TOTAL_RECEIVABLE_CASH = "totalReceivableCash";
    /**
     * 筹资活动现金流出小计
     */
    public static final String SUBJECT_TOTAL_REPAY_CASH = "totalRepayCash";
    /**
     * 筹资活动产生的现金流量净额
     */
    public static final String SUBJECT_FUND_RAISING_CASH = "fundRaisingCash";
    /**
     * 汇率变动对现金的影响
     */
    public static final String SUBJECT_EXCHANGE_RATE_CASH = "exchangeRateCash";
    /**
     * 现金及现金等价物净增加额
     */
    public static final String SUBJECT_INCREASE_CASH = "increaseCash";
    /**
     * 期初现金及现金等价物余额
     */
    public static final String SUBJECT_START_INCREASE_CASH = "startIncreaseCash";
    /**
     * 期末现金及现金等价物余额
     */
    public static final String SUBJECT_END_INCREASE_CASH = "endIncreaseCash";
    /**
     * 营业总收入
     */
    public static final String SUBJECT_BUSINESS_INCOME_TOTAL = "businessIncomeTotal";
    /**
     * 营业利润
     */
    public static final String SUBJECT_BUSINESS_PROFIT = "businessProfit";
    /**
     * 利润总额
     */
    public static final String SUBJECT_PROFIT_TOTAL = "profitTotal";
    /**
     * 净利润
     */
    public static final String SUBJECT_NET_PROFIT = "netProfit";
    /** 合同类型: A-委托担保合同 **/
    public static final String CONTRACT_TYPE_A = "A";
    /** 合同类型: B-反担保保证合同 **/
    public static final String CONTRACT_TYPE_B = "B";
    /** 合同类型: C-反担保抵押合同 **/
    public static final String CONTRACT_TYPE_C = "C";
    /** 合同类型: D-反担保质押合同 **/
    public static final String CONTRACT_TYPE_D = "D";
    /** 合同类型: E-反担保保证金合同 **/
    public static final String CONTRACT_TYPE_E = "E";
    /** 合同类型: F-保证合同 **/
    public static final String CONTRACT_TYPE_F = "F";
    /** 合同类型: G-再担保合同 **/
    public static final String CONTRACT_TYPE_G = "G";
    /** 合同类型: H-抵押合同 **/
    public static final String CONTRACT_TYPE_H = "H";
    /** 合同类型: I-质押合同 **/
    public static final String CONTRACT_TYPE_I = "I";
    /** 合同类型: J-保证金合同 **/
    public static final String CONTRACT_TYPE_J = "J";
    /** 合同类型: K-借款合同 **/
    public static final String CONTRACT_TYPE_K = "K";
    /** 合同类型: L-补充/协议类合同 **/
    public static final String CONTRACT_TYPE_L = "L";
    /** 合同类型: O-金融机构借款合同 **/
    public static final String CONTRACT_TYPE_O = "O";
    /** 合同类型: P-金融机构保证合同 **/
    public static final String CONTRACT_TYPE_P = "P";
    /** 合同类型: Q-非标准合同 **/
    public static final String CONTRACT_TYPE_Q = "Q";
    /** 合同类型: R-其他合同 **/
    public static final String CONTRACT_TYPE_R = "R";
    /** 担保类型: 1-担保 **/
    public static final String GUARANTEE_TYPE_1 = "1";
    /** 担保类型: 2-再担保 **/
    public static final String GUARANTEE_TYPE_2 = "2";
    /** 担保业务大类: 1-融资担保 **/
    public static final String GUAR_BUSINESS_1 = "1";
    /** 担保业务大类: 2-非融资担保 **/
    public static final String GUAR_BUSINESS_2 = "2";
    /** 担保业务大类: 3-再担保 **/
    public static final String GUAR_BUSINESS_3 = "3";
    /** 担保业务大类: 4-保证保险 **/
    public static final String GUAR_BUSINESS_4 = "4";
    /** 担保业务大类: 5-信用证 **/
    public static final String GUAR_BUSINESS_5 = "5";
    /** 担保业务大类: 6-承兑汇票 **/
    public static final String GUAR_BUSINESS_6 = "6";
    /** 担保业务大类: 7-银行保函 **/
    public static final String GUAR_BUSINESS_7 = "7";
    /** 是否是批量业务: 0-否 **/
    public static final String IS_BATCH_0 = "0";
    /** 是否是批量业务: 1-是 **/
    public static final String IS_BATCH_1 = "1";
    /** 报表类型 1-资产负债表 **/
    public static final String REPORT_TYPE_1 = "1";
    /** 报表类型 2-现金流量表 **/
    public static final String REPORT_TYPE_2 = "2";
    /** 报表类型 3-利润表 **/
    public static final String REPORT_TYPE_3 = "3";
    /**
     * 项目状态
     * 1-已受理
     * 2-待审查
     * 3-已审查
     * 4-在保
     * 5-解保
     * 6-终止
     */
    public static final String PROJECT_STATUS_4 = "4";
    public static final String PROJECT_STATUS_5 = "5";
    /**
     * 需要短信提醒的岗位
     * CREDIT_REPORT - 征信报送短信通知人员
     */
    public static final String POST_ID_CREDIT_REPORT = "CREDIT_REPORT";
    /**
     * 短信提醒模板编号
     */
    public static final String CREDIT_SMS_TEMPLATE_ID = "CR0000000001";
    /**
     * 征信报送短信通知
     * 是否已短信通知：0-待通知;1-通知成功;2-通知失败
     */
    public static final String SMS_STATUS_ID_0 = "0";
    public static final String SMS_STATUS_ID_1 = "1";
    public static final String SMS_STATUS_ID_2 = "2";
    /**
     * 最新委托担保合同（0-否 1-是）
     */
    public static final String FIRST_FLAG_0 = "0";
    public static final String FIRST_FLAG_1 = "1";
    /**
     * 企业证件类型 a-组织机构代码
     */
    public static final String CERTIFICATE_TYPE_A = "a";
    /**
     * 企业证件类型 c-贷款卡/中征码
     */
    public static final String CERTIFICATE_TYPE_C = "c";
    /**
     * 企业证件类型 d-机构信用代码
     */
    public static final String CERTIFICATE_TYPE_D = "d";
    /**
     * 企业证件类型 u-统一社会信用代码
     */
    public static final String CERTIFICATE_TYPE_U = "u";
    /**
     * 企业证件类型 z-金融机构代码
     */
    public static final String CERTIFICATE_TYPE_Z = "z";
    /**
     * 企业证件类型 x-其他证件
     */
    public static final String CERTIFICATE_TYPE_X = "x";
    /**
     * 业务分类/产品条线：WAR-债券(再)担保业务
     */
    public static final String PRODUCT_LINE_WAR = "WAR";
    /**
     * 业务分类/产品条线：IFS-普惠(再)担保业务
     */
    public static final String PRODUCT_LINE_IFS = "IFS";
    /**
     * 业务分类/产品条线：NIFS-非普惠(再)担保业务
     */
    public static final String PRODUCT_LINE_NIFS = "NIFS";

    /**
     * 全解除标志位：0-否
     */
    public static final String ALL_RELEASE_FLAG_0 = "0";
    /**
     * 全解除标志位：1-是
     */
    public static final String ALL_RELEASE_FLAG_1 = "1";
}
