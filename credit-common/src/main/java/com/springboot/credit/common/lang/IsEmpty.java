package com.springboot.credit.common.lang;

/**
 * @author liuc
 */
public interface IsEmpty {
    boolean isEmpty();
}
