package com.springboot.credit.common.constants;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class BpConstants {

	/**
	 * ------------------BpConstants目录------------------------------------------
	 * ---------
	 * 
	 * 你好！我是这个类的目录，很高兴为你服务（LZ）
	 * 
	 * 1.一般通用信息
	 * 
	 * 2.意见常量
	 * 
	 * 3.流程常量
	 * 
	 * 4.
	 * 
	 * 5.
	 * 
	 * 6.上传附件常量
	 * 
	 * 7.角色信息
	 * 
	 * 8.保后管理流程变量信息
	 * 
	 * 9.银行信息
	 * 
	 * 10.转换打印文档常量
	 * 
	 * ------------------------------------------------------------------------
	 * ------------
	 */

	// -------------------------一般通用信息-----------------------------------------------------

	/**
	 * 总公司编号
	 */
	public static final String DEPART_F_ZONG = "00000000";

	/**
	 * 辽宁编号
	 */
	public static final String DEPART_F_L = "10000000";

	/**
	 * 吉林编号
	 */
	public static final String DEPART_F_J = "20000000";

	/**
	 * 黑龙江编号
	 */
	public static final String DEPART_F_H = "30000000";

	/**
	 * 内蒙古编号
	 */
	public static final String DEPART_F_N = "40000000";
	
	/**
	 * 小贷公司编号
	 */
	public static final String DEPART_F_XD = "50000000";

	/**
	 * 大连分公司编号
	 */
	public static final String DEPART_F_DL = "60000000";

	/**
	 * 北京资产公司编号
	 */
	public static final String DEPART_F_BJ = "70000000";

	/**
	 * 资金运营管理部 部门编号
	 * 20200401 资金运营中心
	 */
	public static final String DEPART_S_ZG = "8ZJYYGLB";
	
	public static final String DEPARTNAME_S_ZG = "资金运营中心";

	/**
	 * 非融中心
	 */
	public static final String DEPART_S_NF = "5FFR0000";
	
	public static final String DEPARTNAME_S_NF = "非融资业务运营中心";

	/**
	 * 进出口担保业务部 部门编号
	 */
	public static final String DEPART_S_JCK = "4ZJCKDB0";
	
	public static final String DEPARTNAME_S_JCK = "债券业务运营中心";
	
	/**
	 * 资金财务部 部门编号
	 * 20201225 资金财务部
	 */
	public static final String DEPART_S_FINANCE = "7ZJHCW00";
	
	public static final String DEPARTNAME_S_FINANCE = "资金财务部";
	
	
	/**
	 * 系统特殊用户
	 */
	
	public static final String  SYS_USER_SENDER_ID="99999990" ;    //sysSender 
	
	
	/**
	 * 机构业务合作中心 部门编号
	 */
	public static final String DEPART_S_DW = "2ZDWHZ00";
	
	public static final String DEPARTNAME_S_DW = "普惠业务中心";

	/**
	 * 项目状态 立项 没签订合同
	 */
	public static final String PROJECT_STATUS_0 = "0";

	/**
	 * 项目状态 1-已受理
	 */
	public static final String PROJECT_STATUS_1 = "1";

	/**
	 * 项目状态 2-待审查
	 */
	public static final String PROJECT_STATUS_2 = "2";

	/**
	 * 项目状态 3-已审查
	 */
	public static final String PROJECT_STATUS_3 = "3";

	/**
	 * 项目状态 4-在保
	 */
	public static final String PROJECT_STATUS_4 = "4";
	/**
	 * 项目状态 5-解保
	 */
	public static final String PROJECT_STATUS_5= "5";
	/**
	 * 项目状态 6-终止
	 */
	public static final String PROJECT_STATUS_6 = "6";
	/**
	 * 流程状态标志  1--进行中
	 */
	public static final String AUDIT_STATUS_PROCESSING = "1";
	
	/**
	 * 流程状态标志  2--成功结束
	 */
	public static final String AUDIT_STATUS_PUBLISH = "2";
	
	/**
	 * 流程状态标志  3--已取消
	 */
	public static final String AUDIT_STATUS_CANCEL = "3";
	
	/**
	 * 流程状态标志  4--草稿
	 */
	public static final String AUDIT_STATUS_DRAFT = "4";
	
	/**
	 * 业务分类--再担保
	 */
	public static final String BUSI_TYPE_REGUARANTEE = "1";
	
	/**
	 * 业务分类--担保
	 */
	public static final String BUSI_TYPE_GUARANTEE = "2";

	/**
	 * 用来生成项目编号
	 */
	public static final Map<String, String> BUSI_TYPE_MAP = new HashMap<String, String>() {
		{
			put("委托贷款", "WD");
			put("短期委托贷款", "GQ");
			put("担保业务", "DB");
			put("分险再担保", "ZA");
			put("增信再担保", "ZB");
			put("综合再担保", "ZC");
			put("代出保函", "ZD");
			put("技援再担保", "ZE");
			put("共同共保", "ZF");
			put("按份共保", "ZG");
			put("共保保函", "ZH");
		}
	};

	public static final Map<String, String> DEPART_MAP = new HashMap<String, String>() {
		{
			put("00000000", "公司");
			put("10000000", "辽宁分公司");
			put("20000000", "吉林分公司");
			put("30000000", "黑龙江分公司");
			put("40000000", "内蒙古分公司");
		}
	};
	
	/**
	 * 企业规模
	 */
	public static final Map<String, String> ENT_SCALE_MAP = new HashMap<String, String>() {
		{
			put("2", "大型企业");
			put("3", "中型企业");
			put("4", "小型企业");
			put("5", "微型企业");
			put("9", "其他");
		}
	};
	
	/**
	 * 会议纪要类型  风险工作委员会会议纪要
	 */
	public static final String MEETING_TYPE_FX = "FX";
	
	/**
	 * 会议纪要类型 非融
	 */
	public static final String MEETING_TYPE_NF = "NF";

	public static final String BUSS_TYPE_ZGRD = "资格认定";

	public static final String BUSS_TYPE_PJSX = "评级授信";

	// 业务品种 - 再担保业务
	public static final String BUSS_TYPE_ZDB = "再担保业务";

	// 业务品种- 共同担保
	public static final String BUSS_TYPE_GTDB = "共同担保";

	/**
	 * 担保类别--融资性担保
	 */
	public static final String GUARANT_Flag_R = "1";
	
	/**
	 * 担保类别--非融资性担保
	 */
	public static final String GUARANT_Flag_F = "2";
	
	/**
	 * 业务类型 委托贷款
	 */
	public static final String BUSI_TYPE_WD = "委托贷款";

	/**
	 * 业务类型 短期委托贷款 短期过桥业务
	 */
	public static final String BUSI_TYPE_GQ = "短期委托贷款";

	/**
	 * 业务类型 担保业务
	 */
	public static final String BUSI_TYPE_DB = "担保业务";

	/**
	 * 业务类型 再担保 分险再担保
	 */
	public static final String BUSI_TYPE_ZA = "分险再担保";

	/**
	 * 业务类型 再担保 增信再担保
	 */
	public static final String BUSI_TYPE_ZB = "增信再担保";

	/**
	 * 业务类型 再担保 综合再担保
	 */
	public static final String BUSI_TYPE_ZC = "综合再担保";

	/**
	 * 业务类型 再担保 代出保函
	 */
	public static final String BUSI_TYPE_ZD = "代出保函";

	/**
	 * 业务类型 再担保 技援再担保
	 */
	public static final String BUSI_TYPE_ZE = "技援再担保";

	/**
	 * 业务类型 再担保 共同共保
	 */
	public static final String BUSI_TYPE_ZF = "共同共保";

	/**
	 * 业务类型 再担保 按份共保
	 */
	public static final String BUSI_TYPE_ZG = "按份共保";

	/**
	 * 业务类型 再担保 共保保函
	 */
	public static final String BUSI_TYPE_ZH = "共保保函";

	/**
	 * 业务类型 再担保 多元组合再担保
	 */
	public static final String BUSI_TYPE_DY = "多元组合再担保";

	/**
	 * 业务类型 再担保 合作再担保
	 */
	public static final String BUSI_TYPE_HZ = "合作再担保";
	
	/**
	 * 业务类型 其他
	 */
	public static final String BUSI_TYPE_OTHER = "其他";
	
	/**
	 * 业务类型 履约担保
	 */
	public static final String BUSI_TYPE_LY = "履约担保";
	
	/**
	 * 业务类型 诉讼保全
	 */
	public static final String BUSI_TYPE_SS = "诉讼保全";
	
	/**
	 * 业务类型 债券类阶段性批量再担保
	 */
	public static final String BUSI_TYPE_ZZ = "债券类阶段性批量再担保";
	
	/**
	 * 保函大类-工程担保
	 */
	public static final String BID_TYPE_FIRST_CONSTRUCTION = "1";

	/**
	 * 保函大类-商务担保
	 */
	public static final String BID_TYPE_FIRST_BUSI = "2";
	
	/**
	 * 保函类型-投标
	 */
	public static final String BID_TYPE_BID = "T";
	
	/**
	 * 保函类型-履约
	 */
	public static final String BID_TYPE_PERFORMANCE = "L";
	
	/**
	 * 保函类型-预付款
	 */
	public static final String BID_TYPE_ADVANCES = "Y";
	
	/**
	 * 保函类型-质量
	 */
	public static final String BID_TYPE_QUALITY = "Z";
	
	/**
	 * 保函类型-农民工
	 */
	public static final String BID_TYPE_FARMER = "N";

	/**
	 * 保函类型-支付
	 */
	public static final String BID_TYPE_PAY = "P";
	
	/**
	 * 发票类型-增值税专用发票
	 */
	public static final String INVOICE_TYPE_SPECIAL = "1";
	
	/**
	 * 发票类型-增值税普通发票
	 */
	public static final String INVOICE_TYPE_NORMAL = "2";

	/**
	 * 编号：合同
	 * 
	 */
	public static final String NO_WF_HT = "HT";

	/**
	 * 编号：合规性检查通知书
	 * 
	 */
	public static final String NO_WF_HG = "HG";

	/**
	 * 编号：合作协议 银行 机构
	 * 
	 */
	public static final String NO_WF_XY = "XY";


	// -------------------------------意见常量-----------------------------------------------------------------

	/**
	 * 系统自动提交意见类型
	 */
	public static final String COMMENTS_NOCOMENTS = "0";

	/**
	 * 项目评审意见 OR 贷款审批签报内容 OR 项目经理意见
	 */
	public static final String COMMENTS_PM = "1";

	/**
	 * 分公司风控意见
	 */
	public static final String COMMENTS_BRANCH_RISKCONTROL = "2";

	/**
	 * 分公司部门主任意见
	 */
	public static final String COMMENTS_BRANCH_DIRECTOR = "3";

	/**
	 * 分公司副总经理意见
	 */
	public static final String COMMENTS_BRANCH_VICEMANAGER = "4";

	/**
	 * 分公司总经理意见
	 */
	public static final String COMMENTS_BRANCH_MANAGER = "5";

	/**
	 * 分公司领导意见，评审委员意见
	 */
	public static final String COMMENTS_BRANCH_LEADER = "6";

	/**
	 * 风险管理部法律岗意见
	 */
	public static final String COMMENTS_RISK_LEGAL = "7";

	/**
	 * 风险管理部经理意见
	 */
	public static final String COMMENTS_RISK_DIRECTOR = "8";

	/**
	 * 总公司主管风险领导意见
	 */
	// public static final String COMMENTS_MAIN_RISK_ZHUGUAN = "81";

	/**
	 * 总公司主管领导意见
	 */
	public static final String COMMENTS_MAIN_ZHUGUAN = "9";

	/**
	 * 总公司担保业务部综合岗
	 */
	public static final String COMMENTS_YW_ZH = "10";

	/**
	 * 总公司担保业务部经理意见
	 */
	public static final String COMMENTS_YW_DIRECTOR = "11";

	/**
	 * 项目评审会秘书意见
	 */
	public static final String COMMENTS_XM_MS = "12";

	/**
	 * 财务岗意见
	 */
	public static final String COMMENTS_BRANCH_FINANCE = "13";

	/**
	 * 总公司评审委员意见
	 */
	public static final String COMMENTS_MAINXMPSWY = "14";

	/**
	 * 分公司反担保措施保管人意见
	 */
	public static final String COMMENTS_BRANCH_KEEP = "15";

	/**
	 * 总裁 意见
	 */
	public static final String COMMENTS_ZC = "16";

	/**
	 * 总公司担保业务部 评审岗意见
	 */
	public static final String COMMENTS_YW_PSG = "17";

	/**
	 * 总公司对外合作部 银行岗意见
	 */
	public static final String COMMENTS_DW_YH = "18";

	/**
	 * 总公司对外合作部 经理意见
	 */
	public static final String COMMENTS_DW_MANAGER = "19";

	/**
	 * 总公司对外合作部 副总裁意见
	 */
	public static final String COMMENTS_DW_FZC = "20";

	/**
	 * 总公司对外合作部 机构岗意见
	 */
	public static final String COMMENTS_DW_JG = "21";

	/**
	 * 总公司风险管理部 综合岗意见
	 */
	public static final String COMMENTS_RISK_ZH = "22";

	/**
	 * 总公司风险管理部 副总裁
	 */
	public static final String COMMENTS_RISK_FZC = "23";

	/**
	 * 总公司担保业务部 副总裁
	 */
	public static final String COMMENTS_YW_FZC = "24";

	/**
	 * 总公司对外合作部 综合岗意见
	 */
	public static final String COMMENTS_DW_ZH = "25";

	/**
	 * 总公司法律合规部 总经理意见
	 */
	public static final String COMMENTS_LAW_MANAGER = "26";

	/**
	 * 总公司法律合规部 总经理助理意见
	 */
	public static final String COMMENTS_LAW_MANASS = "27";

	/**
	 * 总公司法律合规部 综合岗意见
	 */
	public static final String COMMENTS_LAW_ZH = "28";

	/**
	 * 总公司法律合规部 法律岗意见
	 */
	public static final String COMMENTS_LAW_LAW = "29";

	/**
	 * 总公司风险管理部 保后岗意见
	 */
	public static final String COMMENTS_RISK_BAOHOU = "30";

	/**
	 * 2014版会议纪要 通用意见类型
	 */
	public static final String COMMENTS_MEETING2014_GENERAL = "31";
	

	/**
	 * 总公司资管部 经办人意见
	 */
	public static final String COMMENTS_ZGB_DEALER = "32";
	
	/**
	 * 总公司资管部 负责人意见
	 */
	public static final String COMMENTS_ZGB_MANAGER = "33";
	
	/**
	 * 董事长意见
	 */
	public static final String COMMENTS_CHAIRMAN = "34";

	/**
	 * 总公司计划财务部 经办人意见
	 */
	public static final String COMMENTS_JC_DEALER = "35";
	
	/**
	 * 总公司计划财务部 负责人意见
	 */
	public static final String COMMENTS_JC_MANAGER = "36";
	
	/**
	 * 总公司 法律合规 副总裁
	 */
	public static final String COMMENTS_LAW_FZC = "37";
	
	/**
	 * 总公司  计划财务部 综合岗意见
	 */
	public static final String COMMENTS_JC_ZH = "38";

	// ----------------------------------流程常量--------------------------------------------------------------

	/**
	 * 项目审批流程
	 */
	public static final String WORKFLOW_PROJECTVERIFY = "projectVerify";
	
	/**
	 * 项目审批流程2021
	 */
	public static final String WORKFLOW_PROJECTVERIFY2021 = "projectVerify2021";

	public static final String WORKFLOW_PROJECTVERIFY_CHNAME = "项目审批";
	
	/**
	 * 分公司普惠项目审批流程
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECTVERIFY = "inclusiveProjectVerify";
	
	/**
	 * 总公司普惠项目审批流程
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECTVERIFY_MAIN = "inclusiveProjectVerifyMain";
	
	public static final String WORKFLOW_INCLUSIVEPROJECTVERIFY_CHNAME = "普惠业务项目审批";

	/**
	 * 业务变更审批流程 变更审批
	 */
	public static final String WORKFLOW_CHANGEVERIFY = "changeVerify";
	
	/**
	 * 业务变更审批流程 变更审批2021评审员
	 */
	public static final String WORKFLOW_CHANGEVERIFY2021 = "changeVerify2021";

	/**
	 * （分公司）理财项目审批流程
	 */
	public static final String WORKFLOW_LCPROJECTVERIFY = "lcProjectVerifyFen";

	public static final String WORKFLOW_LCPROJECTVERIFY_CHNAME = "理财项目审批";

//	/**
//	 * （总公司）理财项目审批流程
//	 */
//	public static final String WORKFLOW_LCPROJECTVERIFY_ZONG = "lcProjectVerifyAllZong";
//
//	public static final String WORKFLOW_LCPROJECTVERIFY_ZONG_CHNAME = "理财项目审批";

	/**
	 * 放款审批流程
	 */
	public static final String WORKFLOW_LOANREVIEW = "loanReview2018";
	
	/**
	 * 放款审批流程2021，增加评审员
	 */
	public static final String WORKFLOW_LOANREVIEW2021 = "loanReview2021";

	public static final String WORKFLOW_LOANREVIEW_CHNAME = "放款审批";

	/**
	 * 评审会会议纪要流程 （总公司，现暂时分公司也共用这个流程）
	 */
	public static final String WORKFLOW_MEETING = "meetingMain";

	/**
	 * 评审会会议纪要 签发标志
	 */
	public static final String MEETING_2014_SIGNED = "1";
//
//	/**
//	 * 通知流程
//	 */
//	public static final String WORKFLOW_NOTICEPASS = "noticePass";
//
//	/**
//	 * 合同审批流程，报批
//	 */
//	public static final String WORKFLOW_CONTRACT_BAOPI = "contractApprovalBaopi2018";

	/**
	 * 公司业务合同审批流程
	 */
	public static final String WORKFLOW_CONTRACT_BAOPI_LAW = "contractAppBp_Law";

	/**
	 * 公司业务合同审批流程2021
	 */
	public static final String WORKFLOW_CONTRACT_BAOPI_LAW2021 = "contractAppBp_Law2021";

	/**
	 * 公司业务合同审批流程2023
	 */
	public static final String WORKFLOW_CONTRACT_BAOPI_LAW2023 = "contractAppBp_Law2023";

	/**
	 * 分公司业务合同审批流程
	 */
	public static final String WORKFLOW_CONTRACT_BAOBEI = "contractApprovalBaobei";

	/**
	 * 非融中心 业务合同审批流程
	 */
	public static final String WORKFLOW_NON_FINANCING_PROJECT_CONTRACT = "nonFinancingProjectContractApprovalMain";
	
	// 不再使用！
//	/**
//	 * 项目报备
//	 */
//	public static final String WORKFLOW_PROJECT_BAOBEI = "projectBaobei";

//	/**
//	 * 项目报批
//	 */
//	public static final String WORKFLOW_PROJECT_BAOPI = "projectBaopi";
//
//	/**
//	 * 一类业务变更报备
//	 */
//	public static final String WORKFLOW_PROJECT_CHANGE_BAOBEI = "changeBaobei"; 
//	/**
//	 * 二类业务变更报备
//	 */
//	public static final String WORKFLOW_PROJECT_CHANGE_BAOPI = "changeBaopi";

	/**
	 * 保（贷）后管理检查流程
	 */
	public static final String WORKFLOW_AFTER_GUAN_CHECK = "afterGuanCheck";

	/**
	 * 贷后管理检查流程
	 */
	public static final String WORKFLOW_AFTER_GUAN_CHECK2022 = "afterGuanCheck2022";

	/**
	 * 贷后管理检查流程
	 */
	public static final String WORKFLOW_AFTER_GUAN_CHECK202205 = "afterGuanCheck202205";
	
	/**
	 * 贷后管理检查流程
	 */
	public static final String WORKFLOW_AFTER_GUAN_CHECK2023 = "afterGuanCheck2023";

	public static final String WORKFLOW_AFTER_GUA_MANAGE = "afterGC2016";
	
	public static final String WORKFLOW_AFTER_GUA_MANAGE2023 = "afterGC2023";
	
//	public static final String WORKFLOW_AFTER_GUA_MANAGE2021 = "afterGC2021"; 不再使用

	public static final String WORKFLOW_AFTER_GUAN_CHECK_CHNAME = "首次保后管理检查";
	
	public static final String WORKFLOW_AFTER_GUAN_CHECK_MIDDLE_CHNAME = "期间保后管理检查";
	
	public static final String WORKFLOW_AFTER_GUAN_CHECK_DUE_CHNAME = "到期前保后管理检查";

	public static final String WORKFLOW_AFTER_GUAN_CHECK_CHNAME_XD = "贷后管理检查";

	/**
	 * 项目信息补正流程
	 */
	public static final String WORKFLOW_REPLENISH_B = "replenishBranch";

	/**
	 * 放款报告
	 */
	public static final String WORKFLOW_PAY_INFO = "payInfo2019";
	
	/**
	 * 小贷放款报告
	 */
	public static final String WORKFLOW_XD_PAYINFO = "xdPayInfo";

	/**
	 * 还款报告
	 */
	public static final String WORKFLOW_REPAY_INFO = "repayInfo2019";

	/**
	 * 小贷还款报告
	 */
	public static final String WORKFLOW_XD_REPAYINFO = "xdRepayInfo";

	/**
	 * 小贷还款报告2022
	 */
	public static final String WORKFLOW_XD_REPAYINFO2022 = "xdRepayInfo2022";

	/**
	 * 担保责任解除流程
	 */
	public static final String WORKFLOW_RELEASE_REVIEW = "releaseReview2018";

	public static final String WORKFLOW_RELEASE_REVIEW_CHNAME = "担保责任解除";
	
	public static final String WORKFLOW_GUARANTEE_REVIEW = "guaranteeReview";
	
	public static final String WORKFLOW_GUARANTEE_REVIEW_CHNAME = "出具保函审批";

	/**
	 * 资产分类流程
	 */
	public static final String WORKFLOW_ASSET_CLASS = "assetClass";
	
	/**
	 * 资产分类流程2022
	 */
	public static final String WORKFLOW_ASSET_CLASS2022 = "assetClass2022";

	/**
	 * 资产分类流程2023
	 * 担保公司资产分类，无论什么情况，都是三级审批，初分和复审增加部门负责人意见，最终由经营单位负责人确定结果
	 */
	public static final String WORKFLOW_ASSET_CLASS2023 = "assetClass2023";

	public static final String WORKFLOW_ASSET_CLASS_CHNAME = "资产分类";
	
// 废弃
//	/**
//	 * 项目复议报批
//	 */
//	public static final String WORKFLOW_PROJECT_REVIEW_APPROVAL = "proReApp";
//
//	public static final String WORKFLOW_PROJECT_REVIEW_APPROVAL_CHNAME = "项目复议";

	/**
	 * 立项流程
	 */
	public static final String WORKFLOW_CREATE_PROJECT2022 = "createProject2022";

	/**
	 * 反担保措施登记
	 */
	public static final String WORKFLOW_COUNTER_GUA_IN = "counterGuaIn";

	/**
	 * 反担保措施释放
	 */
	public static final String WORKFLOW_COUNTER_GUA_OUT2022 = "counterGuaOut2022";

	/**
	 * 理财项目受理
	 */
	public static final String WORKFLOW_LC_CREPRO = "lcCreateProject";

	public static final String WORKFLOW_LC_CREPRO_CHNAME = "理财项目受理";

//	public static final String WORKFLOW_LC_PROJECBAOPI = "lcProjectBaopi";

	public static final String WORKFLOW_LC_PROJECBAOPI_CHNAME = "理财项目报批";

	public static final String WORKFLOW_LC_PROJECBAOPI2015 = "lcProjectBaopi2015";

	public static final String WORKFLOW_LC_PROJECBAOPI2015_CHNAME = "理财项目报批";

	/**
	 * 机构合作协议，报批
	 */
	public static final String WORKFLOW_CONTRACT_JG_BAOPI = "jgContractApprovalBPZong";
	
	/**
	 * 法律合规部 机构合作协议审批
	 */
	public static final String WORKFLOW_CONTRACT_JG_BAOPI_LAW = "jgConAppBPZong_Law2018";
	
	/**
	 * 法规审计部 机构合作协议审批
	 */
	public static final String WORKFLOW_CONTRACT_JG_BAOPI_LAW2021 = "jgConAppBPZong_Law2021";

	public static final String WORKFLOW_CONTRACT_JG_CHNAME = "机构框架合作协议审批";
	
	/**
	 * 法规审计部 机构合作协议审批2023，增加内部审核
	 */
	public static final String WORKFLOW_CONTRACT_JG_BAOPI_LAW2023 = "jgConAppBPZong_Law2023";

	/**
	 * 银行合作协议，报批
	 */
	public static final String WORKFLOW_CONTRACT_YH_BAOPI = "yhContractApprovalBPZong";

	public static final String WORKFLOW_CONTRACT_YH_BAOPI_CHNAME = "银行框架合作协议审批";

	/**
	 * 分公司机构项目受理
	 */
	public static final String WORKFLOW_JG_CREPRO = "jgCreateProject";

	public static final String WORKFLOW_JG_CR_ZG_CHNAME = "机构资格认定";

	public static final String WORKFLOW_JG_CR_PJ_CHNAME = "机构评级授信";
	
	public static final String WORKFLOW_XD_JG_CR_ZG_CHNAME = "小贷公司资格认定";
	
	public static final String WORKFLOW_XD_JG_CR_PJ_CHNAME = "小贷公司评级授信";

	/**
	 * 分公司机构项目审批
	 */
	public static final String WORKFLOW_JG_PROVERIFY = "jgProjectVerifyFen";

	public static final String WORKFLOW_JG_PROVERIFY_CHNAME = "机构项目审批";
	
	public static final String WORKFLOW_XD_JG_PROVERIFY_CHNAME = "小贷公司项目审批";

//	/**
//	 * 机构项目报批
//	 */
//	public static final String WORKFLOW_JG_BP = "jgProjectBaopi";

	public static final String WORKFLOW_JG_BP_ZG_CHNAME = "机构项目报批";

	public static final String WORKFLOW_JG_BP_PJ_CHNAME = "机构项目报批";

	/**
	 * 风险信号报告
	 */
	public static final String WORKFLOW_RISK_REPORT = "riskSignalReport2022";

	public static final String WORKFLOW_RISK_REPORT_CHNAME = "风险信号报告";

	public static final String WF_VARIABLE_RISK_RESOURCE = "RISK_XXX_OUT";

	/**
	 * 项目到期提醒
	 */
	public static final String WORKFLOW_PROJECT_REMINDER_THIRTY = "projectReminderThirty";

	public static final String WORKFLOW_PROJECT_REMINDER_THIRTY_CHNAME = "将到期项目还款情况说明";

	/**
	 * 分发材料
	 */
	public static final String WORKFLOW_DISTRIBUTION = "distribution";

	/**
	 * 收入报告
	 */
	public static final String WORKFLOW_INCOME = "income2019";
	
	/**
	 * 小贷收入报告
	 */
	public static final String WORKFLOW_XD_INCOME = "xdIncome";
	
	/**
	 * 批量收入录入
	 */
	public static final String WORKFLOW_INCOME_BATCH = "incomeBatch";

	/**
	 * （老的报备项目审查 内部流程 风险部，已经不用了），现为项目展期报批，审查人审查流程
	 */
	public static final String WORKFLOW_RISKINN = "riskInn2018";
	
	/**
	 * 项目展期，风险部审查流程
	 */
	public static final String WORKFLOW_RISKINN2021 = "riskInn2021";

	/**
	 * 项目展期，法规风险部审查流程2022，是的，又改名了
	 */
	public static final String WORKFLOW_RISKINN2022 = "riskInn2022";

	/**
	 * 项目报批
	 */
	public static final String WORKFLOW_PROJECT_BAOPI2014 = "projectBaopi2014";
	
	public static final String WORKFLOW_PROJECT_BAOPI2019 = "projectBaopi2019";
	
	public static final String WORKFLOW_PROJECT_BAOPI2021 = "projectBaopi2021";
	
	public static final String WORKFLOW_PROJECT_BAOPI202109 = "projectBaopi202109";
	
	public static final String WORKFLOW_PROJECT_BAOPI2022 = "projectBaopi2022";
	
	public static final String WORKFLOW_PROJECT_BAOPI2023 = "projectBaopi2023";
	
	public static final String WORKFLOW_PROJECT_BAOPI202305 = "projectBaopi202305";

	public static final String WORKFLOW_PROJECT_BAOPI2014_CHNAME = "项目报批";

	/**
	 * 评审会会议纪要流程 总公司
	 */
	public static final String WORKFLOW_MEETING2014 = "meetingMain2014";

	/**
	 * 评审会会议纪要流程 总公司
	 */
	public static final String WORKFLOW_NF_MEETING = "nonFinancingMeetingMain";
	
	/**
	 * 风险工作委员会会议纪要流程 总公司
	 */
	public static final String WORKFLOW_FX_MEETING = "meetingRisk";

	/**
	 * 材料补充、事项说明回函
	 */
	public static final String WORKFLOW_HUIHAN = "huihan";

	/**
	 * 材料补充、事项说明回函2021，增加评审员
	 */
	public static final String WORKFLOW_HUIHAN2021 = "huihan2021";

	public static final String WORKFLOW_HUIHAN_CHNAME = "项目审查记录单";

	/**
	 * 项目报批材料分发
	 */
	public static final String WORKFLOW_DIS2014 = "dis2014";

	/**
	 * 合作部内部审查流程：项目复议、展期、报批，报批类变更，机构项目报批
	 */
	public static final String WORKFLOW_DW_INN_BP = "dwInnBP2018";
	
	/**
	 * 合作部内部审查流程：项目复议、展期、报批，报批类变更，机构项目报批
	 */
	public static final String WORKFLOW_ZQ_INN_BP = "zqInnBP";

	/**
	 * 业务部内部审查流程：项目预报、复议、展期、报批，报批类变更
	 */
	public static final String WORKFLOW_BUSSIINN2014BP = "bussiInn2014BP";
	
	/**
	 * 非融中心内部审查流程
	 */
	public static final String WORKFLOW_NF_PROJECT_REPORT_INN = "nonFinancingProjectReportInn";
	
	/**
	 * 批量化普惠业务上报，审批小流程
	 */
	public static final String WORKFLOW_INCLUSIVE_BR_INN = "inclusiveProjectBatchRecordInn";

	/**
	 * 批量化普惠业务上报，评委会签小流程
	 */
	public static final String WORKFLOW_INCLUSIVE_BR_COUNTERSIGN = "inclusiveProjectBatchRecordCountersign";
	
	/**
	 * 模式化普惠业务，审批小流程
	 */
	public static final String WORKFLOW_INCLUSIVE_BP_INN = "inclusiveProjectPatternRecordInn";

	/**
	 * 项目报批，签批小流程2019 业务部 风险部
	 */
	public static final String WORKFLOW_BPINN2019 = "bpInn2019";

	/**
	 * 项目报批，法规风险部合法合规性审查小流程
	 */
	public static final String WORKFLOW_LAWINN = "lawInn";

	/**
	 * 项目报批，风险管理部风险审查小流程
	 */
	public static final String WORKFLOW_RISK_REVIEW = "riskReview";

	/**
	 * 机构项目报批 2014新
	 */
	public static final String WORKFLOW_JG_BAOPI2014 = "jgprojectBaopi2014";
	
	/**
	 * 机构资格认定项目报批
	 */
	public static final String WORKFLOW_JG_BAOPI_ZGRD = "guaQualificationApproval";

	/**
	 * 机构资格认定项目报批2023，增加党委书记审批节点
	 */
	public static final String WORKFLOW_JG_BAOPI_ZGRD2023 = "guaQualificationApproval2023";

	/**
	 * 机构报批项目审查 内部流程 对外合作部
	 */
	public static final String WORKFLOW_DWJGBP = "dwInnJGBP";

	/**
	 * 理财报批项目审查 内部流程 对外合作部
	 */
	public static final String WORKFLOW_DWLCBP = "dwInnLCBP2018";

	/**
	 * 保后管理 担保机构
	 */
	public static final String WORKFLOW_AFTER_GC2014 = "afterGC2014";

	/**
	 * 业务变更 报批类变更
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BP = "projectChangeBP";

	/**
	 * 业务变更 报批类变更2021，增加评审员节点
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BP2021 = "projectChangeBP2021";

	
	/**
	 * 报批类变更流程中发起的业务部内部审查小流程使用的是报批流程中使用的同一个小流程
	 */
//	/**
//	 * 报批类变更项目审查 内部流程 担保业务部
//	 */
//	public static final String WORKFLOW_BUSSIINNBGBP = "bussiInnBGBP";
//
//	/**
//	 * 业务变更 备案类变更
//	 */
//	public static final String WORKFLOW_PROJECTCHANGE_BA = "projectChangeBA";
//
//	/**
//	 * 业务变更 备案类变更 业务部内部审查流程
//	 */
//	public static final String WORKFLOW_BUSSIINNBABG = "bussiInnBABG";
//
//	/**
//	 * 业务变更 备案类变更 风险管理部内部审查流程
//	 */
//	public static final String WORKFLOW_RISKINNBABG = "riskInnBABG";

	/**
	 * 风险项目解除流程
	 */
	public static final String WORKFLOW_RISKREMOVE = "riskSignalRelease";

	/**
	 * 项目复议 2015新
	 */
	public static final String WORKFLOW_FUYI = "fuyi";

	/**
	 * 项目复议 2021增加评审员
	 */
	public static final String WORKFLOW_FUYI2021 = "fuyi2021";

	/**
	 * 项目预报
	 */
	public static final String WORKFLOW_YUBAO = "yubao";

	/**
	 * 项目展期报批
	 */
	public static final String WORKFLOW_EXTENSION = "extension";

	/**
	 * 项目展期报批2021，增加评审员
	 */
	public static final String WORKFLOW_EXTENSION2021 = "extension2021";
	
	/**
	 * 小贷项目展期报批2021
	 */
	public static final String WORKFLOW_PETTYLOAN_EXTENSION = "xdExtension";

	/**
	 * 小贷项目展期报批2023，调换主管风险领导和主管业务领导的审批顺序
	 */
	public static final String WORKFLOW_PETTYLOAN_EXTENSION2023 = "xdExtension2023";
	
	/**
	 * 项目代偿追偿损失信息录入
	 */
	public static final String WORKFLOW_DAICHINFO = "daichangInfo";
	
	/**
	 * 项目代偿追偿方案损失报告审批（分公司）
	 */
	public static final String WORKFLOW_DAICHANGF = "daichangF";
	/**
	 * 项目代偿追偿方案损失报告上报公司流程
	 */
	public static final String WORKFLOW_DAICHANGM = "daichangM2018";
	
	/**
	 * 借用资金审批流程
	 */
	public static final String WORKFLOW_FUND_BORROW = "fundBorrow";
	
	/**
	 * 借用资金审批流程2018
	 */
	public static final String WORKFLOW_FUND_BORROW2018 = "fundBorrow2018";

	/**
	 * 借用资金审批流程2023，只保留书记审批节点，没有董事长
	 */
	public static final String WORKFLOW_FUND_BORROW2023 = "fundBorrow2023";

	/**
	 * 借用资金审批流程202303，中心增加负责人和分管领导
	 */
	public static final String WORKFLOW_FUND_BORROW202303 = "fundBorrow202303";

	/**
	 * 借用资金审批流程202305，审批部门分为财务和资金
	 */
	public static final String WORKFLOW_FUND_BORROW202305 = "fundBorrow202305";

	/**
	 * 借用资金审批流程202306，资金审批的流程要经过总裁
	 */
	public static final String WORKFLOW_FUND_BORROW202306 = "fundBorrow202306";

	/**
	 * 借用资金审批流程202308，送审批部门之前增加会签部门
	 */
	public static final String WORKFLOW_FUND_BORROW202308 = "fundBorrow202308";
	
//	/**
//	 * 项目报备    2017新
//	 */
//	public static final String WORKFLOW_BB_2017 = "projectBaobei2017";
	
//	/**
//	 * 项目报备    2018新
//	 */
//	public static final String WORKFLOW_BB_2018 = "projectBaobei2018";

	/**
	 * 项目报备    2021新
	 */
	public static final String WORKFLOW_BB_2021 = "projectBaobei2021";

	/**
	 * 项目报备    202110，增加评审员
	 */
	public static final String WORKFLOW_BB_202110 = "projectBaobei202110";

	/**
	 * 项目报备    2022，风险管理中心改名为法规风险部
	 */
	public static final String WORKFLOW_BB_2022 = "projectBaobei2022";

	/**
	 * 项目报备    2023，风险管理中心改名为风险管理部，恢复法律合规部
	 */
	public static final String WORKFLOW_BB_2023 = "projectBaobei2023";

	/**
	 * 普惠业务项目备案
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_RECORD = "inclusiveProjectRecord";

	/**
	 * 普惠业务项目备案2022，增加经办人
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_RECORD2022 = "inclusiveProjectRecord2022";

	/**
	 * 普惠业务项目备案202207，增加主管领导
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_RECORD202207 = "inclusiveProjectRecord202207";
	
	/**
	 * 普惠业务批量报备
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_BATCHRECORD = "inclusiveProjectBatchRecord";

	/**
	 * 普惠业务批量报备2023，没有董事长节点，只有党委书记
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_BATCHRECORD2023 = "inclusiveProjectBatchRecord2023";
	
	/**
	 * 普惠业务中心--普惠业务批量报备
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_BATCHRECORD_MAIN = "inclusiveProjectBatchRecordMain";

	/**
	 * 普惠业务中心--普惠业务批量报备2023，没有董事长节点，只有党委书记
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_BATCHRECORD_MAIN2023 = "inclusiveProjectBatchRecordMain2023";
	
	/**
	 * 模式化普惠业上报
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_PATTERNRECORD = "inclusiveProjectPatternRecord";

	/**
	 * 模式化普惠业上报2023，去掉董事长节点，只有书记节点
	 */
	public static final String WORKFLOW_INCLUSIVEPROJECT_PATTERNRECORD2023 = "inclusiveProjectPatternRecord2023";
	/**
	 * 项目报备    内部审查流程      2017
	 */
	public static final String WORKFLOW_BB_INN = "baobeiInn";
	
	/**
	 * 业务变更 备案类变更  2018年 按新制度修改
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BA2017 = "projectChangeBA20180930";
	

	/**
	 * 业务变更 备案类变更  2021新增评审员节点
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BA2021 = "projectChangeBA2021";

	/**
	 * 业务变更 备案类变更202111，增加副主任委员节点
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BA202111 = "projectChangeBA202111";

	/**
	 * 业务变更 备案类变更20213，增加法律合规部，法规风险部改名为风险管理部
	 */
	public static final String WORKFLOW_PROJECTCHANGE_BA2023 = "projectChangeBA2023";
	
	/**
	 * 备案类变更    内部审查流程      2017
	 * 各部门使用同一内部审查流程：业务、法规、机构、财务
	 */
	public static final String WORKFLOW_BABG_INN = "babgInn";
	
	/**
	 * 小贷合同审批   2017
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP = "xdContractApp";
	
	/**
	 * 小贷合同审批   2018
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP_2018 = "xdContractApp2018";

	/**
	 * 小贷合同审批   2022
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP_2022 = "xdContractApp2022";

	/**
	 * 小贷合同审批   202202
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP_202202 = "xdContractApp202202";

	/**
	 * 小贷合同审批   202205
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP_202205 = "xdContractApp202205";

	/**
	 * 小贷合同审批   202211 增加主管风险领导
	 */
	public static final String WORKFLOW_XD_CONTRACTAPP_202211 = "xdContractApp202211";
	
	/**
	 * 分公司 合作协议审批流程    2017
	 */
	public static final String WORKFLOW_JG_CONTRACTAPP_BB = "jgcontractAppBBF";
	
	/**
	 * 信贷资金申请流程
	 */
	public static final String WORKFLOW_FUNDAPPLY_CREDIT = "xdfundApply_credit";
	
	/**
	 * 非信贷资金申请流程
	 */
	public static final String WORKFLOW_FUNDAPPLY_NONCREDIT = "xdfundApply_noncredit";
	
	/**
	 * 域外项目登记流程
	 */
	public static final String WORKFLOW_OUTSIDE_PROJECT_REGIST = "outsideProjectRegist";
	
	/**
	 * 域外项目登记信息变更
	 */
	public static final String WORKFLOW_OUTSIDE_PROJECT_REGIST_UPDATE = "outsideProjectRegistUpdate";
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	/**
//	 * 小贷流程  项目审批（报备、报批） 2017
//	 */
//	public static final String XD_WORKFLOW_PV = "xdProjectVerify";
	
	/**
	 * 小贷流程  项目审批（报备、报批） 2018
	 */
	public static final String XD_WORKFLOW_PROJECTVERIFY_2018 = "xdProjectVerify2018";

	/**
	 * 小贷流程  项目审批（报备、报批） 2022
	 */
	public static final String XD_WORKFLOW_PROJECTVERIFY_2022 = "xdProjectVerify2022";

	/**
	 * 小贷流程  项目审批（报备、报批） 202211，增加风险部门负责人、主管业务领导审批节点
	 */
	public static final String XD_WORKFLOW_PROJECTVERIFY_202211 = "xdProjectVerify202211";

	/**
	 * 小贷流程  项目审批（报备、报批） 2023，主管风险领导、主管业务领导审批节点顺序调换
	 */
	public static final String XD_WORKFLOW_PROJECTVERIFY_2023 = "xdProjectVerify2023";
	
	/**
	 * 小贷流程 资金借用   2017
	 */
	public static final String XD_WORKFLOW_FUNDBORROW = "xdFundBorrow";
	/**
	 * 小贷流程  放款审批 2018
	 */
	public static final String XD_WORKFLOW_LOANREVIEW_2018 = "xdLoanReview2018";
	/**
	 * 小贷流程  放款审批 2022
	 */
	public static final String XD_WORKFLOW_LOANREVIEW_2022 = "xdLoanReview2022";
	/**
	 * 小贷流程  放款审批 202207，增加风险负责人，调整业务负责人和风控岗顺序
	 */
	public static final String XD_WORKFLOW_LOANREVIEW_202207 = "xdLoanReview202207";
	
	/**
	 * 小贷流程  放款审批 202211，增加主管风险领导
	 */
	public static final String XD_WORKFLOW_LOANREVIEW_202211 = "xdLoanReview202211";

	/**
	 * 小贷流程  放款审批 2023，调换主管风险领导、主管业务领导顺序
	 */
	public static final String XD_WORKFLOW_LOANREVIEW_2023 = "xdLoanReview2023";
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static final String WORKFLOW_PROJECT_BID_NAME = "工程担保";
	
	public static final String WORKFLOW_PROJECT_BID = "projectBid";
	
	public static final String WORKFLOW_PROJECT_BID201901 = "projectBid201901";
	
	public static final String WORKFLOW_PROJECT_BID2021 = "projectBid2021";
	
	public static final String WORKFLOW_PROJECT_BID2023 = "projectBid2023";
	
	/**
	 * 非融资担保业务推荐机构合作签批
	 */
	public static final String WORKFLOW_REC_ORG_VERIFY_NAME = "推荐机构";

	/**
	 * 非融中心推荐机构合作协议签订审批
	 */
	public static final String WORKFLOW_COOP_AGREEMENT_VERIFY_NAME = "合作协议签订";

	/**
	 * 客户档案更新审批
	 */
	public static final String WORKFLOW_CLIENT_UPDATE_VERIFY_NAME = "客户档案更新";
	
	/**
	 * 工程担保 合同审批
	 */
	public static final String WORKFLOW_PROJECT_BID_CONTRACT = "projectBidContract";
	
	/**
	 * 吉林分公司，模式化业务审批
	 */
	public static final String WORKFLOW_BATCH_PROJECT_VERIFY = "batchProjectVerify";

	/**
	 * 小贷公司，模式化业务审批
	 */
	public static final String WORKFLOW_BATCH_PETTY_LOAN_PROJECT_VERIFY = "batchPettyLoanProjectVerify";

	/**
	 * 小贷公司，模式化业务审批2023，增加主管风险领导
	 */
	public static final String WORKFLOW_BATCH_PETTY_LOAN_PROJECT_VERIFY2023 = "batchPettyLoanProjectVerify2023";

	/**
	 * 吉林分公司，普惠业务批量审批
	 */
	public static final String WORKFLOW_MULTI_PROJECT_VERIFY = "multiProjectVerify";
	
	/**
	 * 推荐机构合作审批
	 */
	public static final String WORKFLOW_REC_ORG_VERIFY = "recOrgVerify";

	/**
	 * 推荐机构签订合作协议审批
	 */
	public static final String WORKFLOW_COOP_AGREEMENT_VERIFY = "coopAgreementVerify";
	
	/**
	 * 非融项目报批
	 */
	public static final String WORKFLOW_NF_PROJECT_REPORT = "nonFinancingProjectReport";

	/**
	 * 客户档案更新流程
	 */
	public static final String WORKFLOW_CLIENT_UPDATE_VERIFY = "clientUpdateVerify";
	
	/**
	 * 项目发行前管理流程
	 */
	public static final String WORKFLOW_BOND_BEFORE_RELEASE_VERIFY = "bondBeforeReleaseVerify";

	/**
	 * 非融项目按月报备流程
	 */
	public static final String WORKFLOW_NON_FINANCING_PROJECT_MONTHLY_RECORD = "nonFinancingProjectMonthlyRecord";
	
	/**
	 * 材料交接表的不同类型的业务流程 项目报备
	 */
	public static final String TABLE_HANDOVER_PROJECT_BAOBEI = "BB";

	/**
	 * 材料交接表的不同类型的业务流程 项目报批
	 */
	public static final String TABLE_HANDOVER_PROJECT_BAOPI = "BP";
	
	/**
	 * 材料交接表的不同类型的业务流程 批量报备
	 */
	public static final String TABLE_HANDOVER_PROJECT_BR = "BR";
	
	/**
	 * 材料交接表的不同类型的业务流程 批量报备--工程再担保
	 */
	public static final String TABLE_HANDOVER_PROJECT_BBR = "BBR";
	
	/**
	 * 材料交接表的不同类型的业务流程 批量报备--工程担保
	 */
	public static final String TABLE_HANDOVER_PROJECT_BBD = "BBD";

	/**
	 * 材料交接表的不同类型的业务流程 一类变更
	 */
	public static final String TABLE_HANDOVER_CHANGE_FIRST = "FI";

	/**
	 * 材料交接表的不同类型的业务流程 二类变更
	 */
	public static final String TABLE_HANDOVER_CHANGE_SECOND = "SE";

	/**
	 * 材料交接表的不同类型的业务流程 项目复议
	 */
	public static final String TABLE_HANDOVER_PROJECT_FUYI = "FY";

	/**
	 * 材料交接表的不同类型的业务流程 备案变更
	 */
	public static final String TABLE_HANDOVER_CHANGE_BEIAN = "BA";

	/*
	 * 业务变更类别常量
	 */
	public static final String CHANGETYPE_BEIAN = "备案类业务变更";

	public static final String CHANGETYPE_YILEI = "一类业务变更";

	public static final String CHANGETYPE_ERLEI = "二类业务变更";

//	/*
//	 * 贷款金额 ,单位万元
//	 */
//	public static final double DebitAmount_10000WY = 10000;
//
//	public static final double DebitAmount_5000WY = 5000;
	
	/**
	 * 保后管理类型--期间保后
	 */
	public static final String CHECK_TYPE_MIDDLE = "QJ";
	
	/**
	 * 保后管理类型--到期保后
	 */
	public static final String CHECK_TYPE_DUE = "DQ";
	
	/**
	 * 保后管理类型--首次保后
	 */
	public static final String CHECK_TYPE_FIRST = "SC";

	/**
	 * 选择，报批
	 */
	public static final String SELECT_BAOPI = "BP";

	/**
	 * 选择，报备
	 */
	public static final String SELECT_BAOBEI = "BB";

	/**
	 * 项目类型 ：担保 ，再担保 ， 委贷 过桥 项目（应保存在项目信息表中）
	 */
	public static final String PROJECT_TYPE_XIANGMU = "XM";

	/**
	 * 项目类型： 理财
	 */
	public static final String PROJECT_TYPE_LICAI = "LC";

	/**
	 * 项目类型 ： 机构
	 */
	public static final String PROJECT_TYPE_JIGOU = "JG";
	
	/**
	 * 项目类型 ： 小贷公司
	 */
	public static final String PROJECT_TYPE_XDGS = "XDGS";

	/**
	 * 项目类型 ： 小贷
	 */
	public static final String PROJECT_TYPE_XD = "XD";

	/**
	 * 项目信息补正状态 ： 申请
	 */
	public static final String REPLENISH_STATUS_SQ = "SQ";

	/**
	 * 项目信息补正状态 ： 通过 （只有在这个状态，申请人才能更新项目信息）
	 */
	public static final String REPLENISH_STATUS_TG = "TG";

	/**
	 * 项目信息补正状态 ： 驳回
	 */
	public static final String REPLENISH_STATUS_BH = "BH";

	/**
	 * 项目信息补正状态 ： 结束
	 */
	public static final String REPLENISH_STATUS_JS = "JS";

	/**
	 * 放还款标识 ： 放款
	 */
	public static final String REPAY_FLAG_FK = "FK";

	/**
	 * 放还款标识 ： 还款
	 */
	public static final String REPAY_FLAG_HK = "HK";

	/**
	 * 反担保措施流程类型 登记
	 */
	public static final String COUNTER_GUA_TYPE_DJ = "登记";

	/**
	 * 反担保措施流程类型 释放
	 */
	public static final String COUNTER_GUA_TYPE_SF = "释放";

	/**
	 * 反担保措施流程 状态 未交接
	 */
	public static final String COUNTER_GUA_STATUS_0 = "未交接";

	/**
	 * 反担保措施流程 状态 交接中
	 */
	public static final String COUNTER_GUA_STATUS_A = "交接中";

	/**
	 * 反担保措施流程 状态 已交接
	 */
	public static final String COUNTER_GUA_STATUS_B = "已交接";

	/**
	 * 反担保措施流程 状态 释放中
	 */
	public static final String COUNTER_GUA_STATUS_C = "释放中";

	/**
	 * 反担保措施流程 状态 已释放
	 */
	public static final String COUNTER_GUA_STATUS_D = "已释放";

	// ------------------------------------上传附件常量--------------------------------------------------------------------

	

	/**
	 * 是否在页面展示真实删除按钮(显示)
	 */
	public static final String DELETE_ATTACHMENT_SHOW_FLAG = "1";

	/**
	 * 是否在页面展示真实删除按钮（不显示）
	 */
	public static final String DELETE_ATTACHMENT_NOT_SHOW_FLAG = "0";

	/**
	 * 文件转换标识： 可转换：1 不能转换：0
	 */
	public static final String FILE_CONVERT_YES_FLAG = "1";

	/**
	 * 文件转换标识： 可转换：1 不能为转换：0
	 */
	public static final String FILE_CONVERT_NO_FLAG = "0";

	/**
	 * 总公司理财项目**银行岗**上传立项附件
	 */
	public static final String LCPROJECTVERIFY_ZONG_ATTA_YHG = "LC_Y";

	/**
	 * 总公司理财项目**项目经理**上传项目报告
	 */
	public static final String LCPROJECTVERIFY_ZONG_ATTA_XMJL = "LC_X";

	/**
	 * 总公司理财项目**风控岗**上传风险评估报告
	 */
	public static final String LCPROJECTVERIFY_ZONG_ATTA_FKG = "LC_F";

	// -------------------------------角色信息-----------------------------------------------------------------

	/**
	 * 角色，基本用户角色
	 */
	public static final String ROLE_USER = "ROLE_USER";

	/**
	 * 角色，系统管理员
	 */
	public static final String ROLE_ADMIN = "ROLE_ADMIN";

	/**
	 * 角色，分公司业务主任
	 */
	public static final String ROLE_YWZR = "ROLE_YWZR";

	/**
	 * 角色，分公司副总经理
	 */
	public static final String ROLE_FZJL = "ROLE_FZJL";

	/**
	 * 角色，分公司总经理
	 */
	public static final String ROLE_ZJL = "ROLE_ZJL";

	/**
	 * 角色，分公司评审员
	 */
	public static final String ROLE_REVIEWER = "ROLE_REVIEWER";

	/**
	 * 角色，分公司项目审批评审委员
	 */
	public static final String ROLE_XMPSWY = "ROLE_XMPSWY";

	/**
	 * 角色，非融项目审批评审委员
	 */
	public static final String ROLE_NON_FINANCING_COMMITTEE = "ROLE_NON_FINANCING_COMMITTEE";

	/**
	 * 角色，非融项目审批评审会主任委员
	 */
	public static final String ROLE_NON_FINANCING_CHIEF_COMMITTEE = "ROLE_NON_FINANCING_CHIEF_COMMITTEE";
	
	/**
	 * 角色，风险工作委员会委员
	 */
	public static final String ROLE_FXPSWY = "ROLE_FXGZWY";

	/**
	 * 角色，分公司法律岗
	 */
	public static final String ROLE_FLAW = "ROLE_FLAW";

	/**
	 * 角色，分公司出纳岗
	 */
	public static final String ROLE_FFINANCE = "ROLE_FFINANCE";

	/**
	 * 角色，分公司风控岗
	 */
	public static final String ROLE_FRISKCTRL = "ROLE_FRISKCTRL";

	/**
	 * 角色，分公司客户管理部员工
	 */
	public static final String ROLE_CM_USER = "ROLE_CM_USER";

	/**
	 * 角色，分公司印章管理员
	 */
	public static final String ROLE_FSEALER = "ROLE_FSEALER";

	/**
	 * 角色，分公司反担保措施保管人
	 */
	public static final String ROLE_FKEEPER = "ROLE_FKEEPER";

	/**
	 * 角色，总公司风险部综合岗
	 */
	public static final String ROLE_RISKZH = "ROLE_RISKZH";

	/**
	 * 角色，总公司风险部法律岗
	 */
	public static final String ROLE_RISKLAW = "ROLE_RISKLAW";

	/**
	 * 角色，总公司法规部外部审核岗
	 */
	public static final String ROLE_LAW_OUTSIDELAWYER = "ROLE_LAW_OUTSIDELAWYER";

	/**
	 * 角色，总公司风险部风险信号报告 风险岗
	 */
	public static final String ROLE_RISK_SIGNAL = "ROLE_RISK_SIGNAL";

	/**
	 * 角色，总公司风险部合同审批法律岗
	 */
	public static final String ROLE_HTSPFLG = "ROLE_HTSPFLG";

	/**
	 * 角色，总公司风险部经理
	 */
	public static final String ROLE_RISKMANAGER = "ROLE_RISKMANAGER";

	/**
	 * 角色，总公司风险部职员
	 */
	public static final String ROLE_RISKUSER = "ROLE_RISKUSER";

	/**
	 * 角色，对外合作部秘书（理财秘书）
	 */
	public static final String ROLE_DW_MS = "ROLE_DW_MS";

	/**
	 * 角色，对外合作部收件人
	 */
	public static final String ROLE_DW_REC = "ROLE_DW_REC";

	/**
	 * 角色，对外合作部理财岗
	 */
	public static final String ROLE_DW_LCG = "ROLE_DW_LCG";

	/**
	 * 角色，对外合作部机构岗
	 */
	public static final String ROLE_DW_JG = "ROLE_DW_JG";

	/**
	 * 角色，对外合作部机构秘书
	 */
	public static final String ROLE_DW_JG_MS = "ROLE_DW_JG_MS";

	/**
	 * 角色，对外合作部职员
	 */
	public static final String ROLE_DW_USER = "ROLE_DW_USER";

	/**
	 * 角色，对外合作部综合岗
	 */
	public static final String ROLE_DW_ZH = "ROLE_DW_ZH";

	/**
	 * 角色，对外合作部经理
	 */
	public static final String ROLE_DWMANAGER = "ROLE_DWMANAGER";

	/**
	 * 角色，非融中心经理
	 */
	public static final String ROLE_NON_FINANCING_MANAGER = "ROLE_NON_FINANCING_MANAGER";

	/**
	 * 角色，债券中心职员
	 */
	public static final String ROLE_ZQ_USER = "ROLE_ZQ_USER";
	
	/**
	 * 角色，债券中心综合岗
	 */
	public static final String ROLE_ZQ_ZH = "ROLE_ZQ_ZH";

	/**
	 * 角色，债券中心经理
	 */
	public static final String ROLE_ZQ_MANAGER = "ROLE_ZQ_MANAGER";

	/**
	 * 角色，债券中心副经理
	 */
	public static final String ROLE_ZQ_VICE_MANAGER = "ROLE_ZQ_VICE_MANAGER";

	/**
	 * 角色，总公司主管风险的副总裁（这个角色在总公司理财里用到了）
	 */
	public static final String ROLE_FZC_RISK = "ROLE_FZC_RISK";

	/**
	 * 角色，总公司主管法规的副总裁
	 */
	public static final String ROLE_FZC_LAW = "ROLE_FZC_LAW";

	/**
	 * 角色，总公司对外合作的副总裁
	 */
	public static final String ROLE_FZC_DW = "ROLE_FZC_DW";
	
	/**
	 * 角色，主管财务副总裁
	 */
	public static final String ROLE_FZC_LC = "ROLE_FZC_LC";
	
	/**
	 * 角色，主管债券中心的副总裁
	 */
	public static final String ROLE_FZC_ZQ = "ROLE_FZC_ZQ";

	/**
	 * 角色，主管资金运营中心的副总裁
	 */
	public static final String ROLE_FZC_ZG = "ROLE_FZC_ZG";

	/**
	 * 角色，主管非融中心的副总裁
	 */
	public static final String ROLE_FZC_NF = "ROLE_FZC_NF";

	/**
	 * 角色，主管小贷公司的副总裁
	 */
	public static final String ROLE_FZC_XD = "ROLE_FZC_XD";

	/**
	 * 角色，副总裁
	 */
	public static final String ROLE_FZC = "ROLE_FZC";
	
	/**
	 * 角色，副主任委员
	 */
	public static final String ROLE_DEPUTY_COMMITTEE = "ROLE_DEPUTY_COMMITTEE";

	/**
	 * 角色，总公司印章管理员
	 */
	public static final String ROLE_ZSEALER = "ROLE_ZSEALER";

	/**
	 * 角色，总公司项目评审会秘书，业务部综合岗,现在这两个角色是一个人
	 */
	public static final String ROLE_YWSEC = "ROLE_YWSEC";

	/**
	 * 角色，总公司业务部经理
	 */
	public static final String ROLE_YWMANAGER = "ROLE_YWMANAGER";

	/**
	 * 角色，总公司业务部职员
	 */
	public static final String ROLE_YWUSER = "ROLE_YWUSER";

	/**
	 * 角色，总公司总裁
	 */
	public static final String ROLE_ZC = "ROLE_ZC";

	/**
	 * 角色，总公司项目评审委员
	 */
	public static final String ROLE_MAINXMPSWY = "ROLE_MAINXMPSWY";

	/**
	 * 角色，总公司副总裁 担保业务
	 */
	public static final String ROLE_FZC_YW = "ROLE_FZC_YW";

	/**
	 * 角色，分公司 资产分类审查评审委员意见填写人
	 */
	public static final String ROLE_ASSCLASS_PS = "ROLE_ASSCLASS_PS";

	/**
	 * 角色，总公司业务部收件人
	 */
	public static final String ROLE_YW_REC = "ROLE_YW_REC";

	/**
	 * 角色，总公司非融中心收件人
	 */
	public static final String ROLE_NF_REC = "ROLE_NF_REC";

	/**
	 * 角色，总公司非融中心 职员
	 */
	public static final String ROLE_NF_USER = "ROLE_NF_USER";

	/**
	 * 角色，总公司财务部总经理
	 */
	public static final String ROLE_CWMANAGER = "ROLE_CWMANAGER";

	/**
	 * 角色，总公司风险部风险信号报告 风险部收件人
	 */
	public static final String ROLE_FXSJ = "ROLE_FXSJ";

	/**
	 * 角色，总公司风险部风险信号报告 业务部收件人
	 */
	public static final String ROLE_YWSJ = "ROLE_YWSJ";
	
	/**
	 * 角色，档案查看角色 前缀
	 */
	public static final String ROLE_ARCHIVE_ = "ROLE_ARCHIVE_";
//	/**
//	 * 角色，档案查看角色 分公司领导
//	 */
//	public static final String ROLE_ARCHIVE_LEADER = "ROLE_ARCHIVE_LEADER";
//
//	/**
//	 * 角色，档案查看角色 公司领导
//	 */
//	public static final String ROLE_ARCHIVE_LEADER_M = "ROLE_ARCHIVE_LEADER_M";
//	
//	/**
//	 * 角色，档案查看角色 只查看在保项目
//	 */
//	public static final String ROLE_ARCHIVE_LEADER_ZB = "ROLE_ARCHIVE_LEADER_ZB";
	
	/**
	 * 角色，档案查看角色 查阅全部机构所有项目档案
	 */
	public static final String ROLE_ARCHIVE_GLOBAL = "ROLE_ARCHIVE_GLOBAL";
	
	/**
	 * 角色，档案查看角色 查阅本机构全部项目档案
	 */
	public static final String ROLE_ARCHIVE_LOCAL = "ROLE_ARCHIVE_LOCAL";
	
//	/**
//	 * 角色，档案查看角色 查阅全部机构在保项目档案
//	 */
//	public static final String ROLE_ARCHIVE_GLOBAL_ZB = "ROLE_ARCHIVE_GLOBAL_ZB";

	/**
	 * 角色，生成报表
	 */
	public static final String ROLE_GEN_REPORT = "ROLE_GEN_REPORT";

	/**
	 * 角色，查看报表
	 */
	public static final String ROLE_READ_REPORT = "ROLE_READ_REPORT";

	/**
	 * 角色，查看财务报表
	 */
	public static final String ROLE_READ_CWREPORT = "ROLE_READ_CWREPORT";

	/**
	 * 角色，生成财务报表
	 */
	public static final String ROLE_GEN_CWREPORT = "ROLE_GEN_CWREPORT";

	/**
	 * 角色，项目报备风险意见出具人
	 * 
	 * 该角色没有使用，暂时注释掉 20170401
	 */
	//public static final String ROLE_BBRISK_YIJIAN = "ROLE_BBRISK_YIJIAN";

	/**
	 * 角色，报备项目审查人 风险部
	 */
	public static final String ROLE_BBASSIGN_FX = "ROLE_BBASSIGN_FX";

	/**
	 * 角色，变更项目审查人 风险部
	 */
	public static final String ROLE_BGASSIGN_FX = "ROLE_BGASSIGN_FX";

	/**
	 * 角色，总公司法律合规部经理
	 */
	public static final String ROLE_LAWMANAGER = "ROLE_LAWMANAGER";

	/**
	 * 角色，总公司法律合规部经理助理
	 */
	public static final String ROLE_LAW_ASSIST = "ROLE_LAW_ASSIST";

	/**
	 * 角色，总公司法律合规部职员
	 */
	public static final String ROLE_LAWUSER = "ROLE_LAWUSER";

	/**
	 * 角色，总公司法律合规部综合岗
	 */
	public static final String ROLE_LAW_ZH = "ROLE_LAW_ZH";

	/**
	 * 角色，总公司法律合规部法律岗
	 */
	public static final String ROLE_LAW_LAW = "ROLE_LAW_LAW";

	/**
	 * 角色，总公司风险管理部将到期项目还款说明岗
	 */
	public static final String ROLE_RISK_DXHKSM = "ROLE_RISK_DXHKSM";

	/**
	 * 角色，报备项目审查人 对外合作部
	 */
	public static final String ROLE_BBASSIGN_DW = "ROLE_BBASSIGN_DW";

	/**
	 * 角色，董事长
	 */
	public static final String ROLE_CHAIRMAN = "ROLE_CHAIRMAN";

	/**
	 * 角色，党委书记
	 */
	public static final String ROLE_PARTY_SECRETARY = "ROLE_PARTY_SECRETARY";

	/**
	 * 角色，会议主持人
	 */
	public static final String ROLE_ZHUCHIREN = "ROLE_ZHUCHIREN";
	
	/**
	 * 角色，风险工作委员会会议主持人
	 */
	public static final String ROLE_FX_ZHUCHIREN = "ROLE_FX_ZHUCHIREN";

	/**
	 * 角色，系统 待办任务查阅
	 */
	public static final String ROLE_SYS_VIEWWORKLIST = "ROLE_SYS_VIEWWORKLIST";

	/**
	 * 角色，系统 已办任务查阅
	 */
	public static final String ROLE_SYS_VIEWWORKHIS = "ROLE_SYS_VIEWWORKHIS";

	/**
	 * 角色，会计岗
	 */
	public static final String ROLE_ACCOUNTANT = "ROLE_ACCOUNTANT";

	/**
	 * 角色，业务统计岗
	 */
	public static final String ROLE_STAT = "ROLE_STAT";

	/**
	 * 角色，系统 补录角色
	 */
	public static final String ROLE_BULU = "ROLE_BULU";
	
	
	/**
	 * 角色，分公司超级 补录角色 
	 */
	public static final String ROLE_SUPER_BULU = "ROLE_SUPER_BULU";
	
	
	/**
	 * 角色，分公司 风险部负责人
	 */
	public static final String ROLE_RISK_DIRECTOR = "ROLE_RISK_DIRECTOR";
	
	/**
	 * 角色，总公司主管合规部的副总裁
	 */
	public static final String ROLE_FZC_HG = "ROLE_FZC_HG";
	// TODO 这两个角色还不能删，有别的地方用到，如果资管部将这些流程交接给计划财务部，还需继续修改
	/**
	 * 角色，总公司资金运营管理部 经办人
	 */
	public static final String ROLE_ZGB_DEALER = "ROLE_ZGB_DEALER";
	
	/**
	 * 角色，总公司资金运营管理部 副总经理
	 */
	public static final String ROLE_ZGB_VICE_MANAGER = "ROLE_ZGB_VICE_MANAGER";
	
	/**
	 * 角色，总公司资金运营管理部 负责人
	 */
	public static final String ROLE_ZGB_MANAGER = "ROLE_ZGB_MANAGER";
	
	/**
	 * 角色，总公司计划财务部 经办人
	 */
	public static final String ROLE_JC_DEALER = "ROLE_JC_DEALER";
	
	/**
	 * 角色，总公司计划财务部 负责人
	 */
	public static final String ROLE_JC_MANAGER = "ROLE_JC_MANAGER";
	
	/**
	 * 角色，风险评审会会议材料待分发人
	 */
	public static final String ROLE_RISKMEETING_CL = "ROLE_RISKMEETING_CL";
	/**
	 * 角色，总公司机构业务合作中心 副总经理
	 */
	public static final String ROLE_DW_VICE_MANAGER = "ROLE_DW_VICE_MANAGER";
	/**
	 * 角色，总公司机构业务合作中心 负责人
	 */
	public static final String ROLE_DW_MANAGER = "ROLE_DW_MANAGER";
	/**
	 * 角色，总公司进出口担保业务部 负责人
	 */
	public static final String ROLE_JCK_MANAGER = "ROLE_JCK_MANAGER";
	/**
	 * 角色，总公司进出口担保业务部 副总经理
	 */
	public static final String ROLE_JCK_VICE_MANAGER = "ROLE_JCK_VICE_MANAGER";
	/**
	 * 角色，总公司  计划财务部  综合岗
	 */
	public static final String ROLE_JC_ZH = "ROLE_JC_ZH";
	
	/**
	 * 角色，分公司合同审批   部门负责人角色
	 */
	public static final String ROLE_RISK_MANAGER_HT = "ROLE_RISK_MANAGER_HT";
	
	/**
	 * 角色，分公司域外项目登记，业务单位负责人角色
	 */
	public static final String ROLE_OUTSIDE_REGIST = "ROLE_OUTSIDE_REGIST";
	
	/**
	 * 角色，分发会议纪要选人
	 */
	public static final String ROLE_READ_MEETING_SUMMARY = "ROLE_READ_MEETING_SUMMARY";

	/**
	 * 角色，非融项目，评审会决议分发，候选人
	 */
	public static final String ROLE_READ_NF_MEETING_SUMMARY = "ROLE_READ_NF_MEETING_SUMMARY";
	
	/**
	 * 角色：部门负责人，包括分公司的部门主任、子公司的营业部主任和总公司各部门负责人
	 */
	public static final String ROLE_DEPART_LEADER = "ROLE_DEPART_LEADER";
	
	/**
	 * 角色：总公司 普惠业务管理委员会主任委员
	 */
	public static final String ROLE_INCLUSIVE_CHAIRMAN = "ROLE_INCLUSIVE_CHAIRMAN";
	
	/**
	 * 角色：总公司 普惠业务管理委员会委员
	 */
	public static final String ROLE_INCLUSIVE_COMMITTEE = "ROLE_INCLUSIVE_COMMITTEE";

	/**
	 * 角色：普惠秘书
	 */
	public static final String ROLE_INCLUSIVE_SECRETARY = "ROLE_INCLUSIVE_SECRETARY";
	
	/**
	 * 角色：总公司 分管普惠业务副总裁
	 */
	public static final String ROLE_FZC_INCLUSIVE = "ROLE_FZC_INCLUSIVE";
	
	/**
	 * 角色：总公司 业务部 统计岗
	 */
	public static final String ROLE_YW_STATISTICS = "ROLE_YW_STATISTICS";
	
	/**
	 * 资金借用/划拨审批流程，通过后通知的人（没有参与流程）
	 */
	public static final String ROLE_FUND_INFORMER = "ROLE_FUND_INFORMER";
	
	/**
	 * 模式化业务审批，通过后通知的人
	 */
	public static final String ROLE_BATCH_PROJECT_VERIFY_INFORMER = "ROLE_BATCH_PROJECT_VERIFY_INFORMER";
	
	// ----------------------------------小贷系统 角色------------------------------------------//
	/**
	 * 角色，小贷系统 基本角色
	 */
	public static final String XD_ROLE_USER = "XD_ROLE_USER";
	/**
	 * 角色，小贷系统 分公司负责人
	 */
	public static final String XD_ROLE_B_MANAGER = "XD_ROLE_B_MANAGER";
	/**
	 * 角色，小贷系统 风控岗
	 */
	public static final String XD_ROLE_RISK = "XD_ROLE_RISK";
	/**
	 * 角色，小贷系统 评审负责人
	 */
	public static final String XD_ROLE_PS_MANAGER = "XD_ROLE_PS_MANAGER";
	/**
	 * 角色，小贷系统 主管领导 副总经理
	 */
	public static final String XD_ROLE_FZC = "XD_ROLE_FZC";
	/**
	 * 角色，小贷系统 总经理
	 */
	public static final String XD_ROLE_ZC = "XD_ROLE_ZC";
	
	/**
	 * 角色，小贷系统 分公司财务部负责人
	 */
	public static final String XD_ROLE_FENJCM = "XD_ROLE_FENJCM";
	
	/**
	 * 角色　小贷系统 计划财务部经办人
	 */
	public static final String XD_ROLE_ZONGJC_DEALER = "XD_ROLE_ZONGJC_DEALER";
	
	/**
	 * 角色，小贷系统 计划财务部负责人
	 */
	public static final String XD_ROLE_ZONGJCM = "XD_ROLE_ZONGJCM";
	
	/**
	 * 角色，小贷系统 分公司法律岗
	 */
	public static final String XD_ROLE_FLAW = "XD_ROLE_FLAW";
	/**
	 * 角色，小贷系统 公司法律岗
	 */
	public static final String XD_ROLE_ZLAW = "XD_ROLE_ZLAW";
	/**
	 * 角色，小贷系统 风险部负责人
	 */
	public static final String XD_ROLE_RISK_MANAGER = "XD_ROLE_RISK_MANAGER";
	/**
	 * 角色，小贷系统  印章管理员
	 */
	public static final String XD_ROLE_FSEALER = "XD_ROLE_FSEALER";
	/**
	 * 角色，小贷系统 分公司主管领导
	 */
	public static final String XD_ROLE_B_ZHUGUAN = "XD_ROLE_B_ZHUGUAN";

	/**
	 * 角色，小贷系统 合规岗
	 */
	public static final String XD_ROLE_COMPLIANCE = "XD_ROLE_COMPLIANCE";

	/**
	 * 角色，小贷系统 业务部负责人
	 */
	public static final String XD_ROLE_YW_MANAGER = "XD_ROLE_YW_MANAGER";

	/**
	 * 角色，小贷系统 执行董事
	 */
	public static final String XD_ROLE_EXECUTIVE_DIRECTOR = "XD_ROLE_EXECUTIVE_DIRECTOR";
	
	/**
	 * 角色，首席风险官
	 */
	public static final String ROLE_RISK_OFFICER = "ROLE_RISK_OFFICER";
	
	
	
	
	
	
	// ----------------------------------小贷系统 角色------------------------------------------//

	// ----------------------------------保后管理流程变量信息---------------------------------------------------

	/*
	 * 保后管理 检查类型
	 */
	/**
	 * 保后管理，检查类型 《例行检查》
	 */
	public static final String AFTERGUANCHECK_CHECKTYPE_LX = "LX";

	/**
	 * 保后管理，检查类型 《重点检查》
	 */
	public static final String AFTERGUANCHECK_CHECKTYPE_ZD = "ZD";

	/**
	 * 保后管理，检查类型 《其他》
	 */
	public static final String AFTERGUANCHECK_CHECKTYPE_QT = "QT";

	/*
	 * 保后管理 风险信号
	 */
	/**
	 * 保后管理，风险信号报告标识 《有风险》
	 */
	public static final String AFTERGUANCHECK_RISKSIGNAL_YOU = "YOU";

	/**
	 * 保后管理，风险信号报告标识 《没有风险》
	 */
	public static final String AFTERGUANCHECK_RISKSIGNAL_MEI = "MEI";

	/*
	 * 保后管理 资产分类
	 */
	/**
	 * 保后管理，资产分类审查出分意见 《正常》
	 */
	public static final String AFTERGUANCHECK_FIRSTIDEA_ZC = "ZC";

	/**
	 * 保后管理，资产分类审查出分意见 《关注》
	 */
	public static final String AFTERGUANCHECK_FIRSTIDEA_GZ = "GZ";

	/**
	 * 保后管理，资产分类审查出分意见 《次级》
	 */
	public static final String AFTERGUANCHECK_FIRSTIDEA_CJ = "CJ";

	/**
	 * 保后管理，资产分类审查出分意见 《可疑》
	 */
	public static final String AFTERGUANCHECK_FIRSTIDEA_KY = "KY";

	/**
	 * 保后管理，资产分类审查出分意见 《损失》
	 */
	public static final String AFTERGUANCHECK_FIRSTIDEA_SS = "SS";

	/*
	 * 保后管理类型
	 */
	/**
	 * 保后管理，类型 《担保业务保后管理检查》
	 */
	public static final String AFTERGUANCHECK_TYPE_D = "D";

	/**
	 * 保后管理，类型 《再担保业务保后管理检查》
	 */
	public static final String AFTERGUANCHECK_TYPE_Z = "Z";

	/**
	 * 保后管理，类型 《委托贷款业务贷后管理检查》
	 */
	public static final String AFTERGUANCHECK_TYPE_W = "W";

	// 小贷 贷后管理类型
	public static final String AFTERGUANCHECK_TYPE_X = "X";

	// ---------------------------理财项目流程--------------------------------------------------------

	/**
	 * 理财项目流程 产品类型 集合资金信托计划
	 */
	public static final String LCPROJECTVERIFY_CPTYPE_JHCJXTJH = "JHCJXTJH";

	/**
	 * 理财项目流程 产品类型 结构性存款
	 */
	public static final String LCPROJECTVERIFY_CPTYPE_JGXCK = "JGXCK";

	// ---------------------------机构项目流程--------------------------------------------------------

	/**
	 * 机构项目流程 资格认定
	 */
	public static final String JGPROBAOPI_TYPE_ZG = "JG_ZG";

	/**
	 * 机构项目流程 评级授信
	 */
	public static final String JGPROBAOPI_TYPE_PJ = "JG_PJ";

	/**
	 * 选择，机构报批
	 */
	public static final String CONTR_JG = "CONTR_JG";

	/**
	 * 选择，银行报批
	 */
	public static final String CONTR_YH = "CONTR_YH";

	/**
	 * 机构立项：资格认定
	 */
	public static final String JG_ZGRD = "JG_ZGRD";

	/**
	 * 机构立项：评级授信
	 */
	public static final String JG_PJSX = "JG_PJSX";

	/**
	 * 机构立项：资格认定（进入体系）标识 1为进入 0为未进入
	 */
	public static final String JG_ZGRD_FLAG_IN = "1";

	/**
	 * 机构立项：资格认定（进入体系）标识 1为进入 0为未进入
	 * 注：这个OUTFLAG现在（2012-05-08）并没有用上，但是以后机构加入又退出体系的时候就能用上了
	 */
	public static final String JG_ZGRD_FLAG_OUT = "1";

	/**
	 * 机构立项：评级授信标识 1为YES 0为NO
	 */
	public static final String JG_PJSX_FLAG_YES = "1";

	/**
	 * 合作协议评审 合作协议的发起人来自总公司
	 */
	public static final String XIEYI_ZONG = "XIEYI_ZONG";

	/**
	 * 合作协议评审 合作协议的发起人来自分公司
	 */
	public static final String XIEYI_FEN = "XIEYI_FEN";
	
	/**
	 * 资金借用/划拨流程，财务部审批标识
	 */
	public static final String FUNDBORROW_AUDIT_FINANCE = "1";

	/**
	 * 资金借用/划拨流程，资金管理中心审批标识
	 */
	public static final String FUNDBORROW_AUDIT_FUND = "2";

	/**
	 * 资金借用/划拨标识，借用--1
	 */
	public static final String FUNDBORROW_TYPE_BORROW = "1";

	/**
	 * 资金借用/划拨标识，划拨--2
	 */
	public static final String FUNDBORROW_TYPE_TRANSFER = "2";
	

	// --------------------------转换打印文档常量----------------------------------------------------

	/**
	 * 打印文件类型1
	 */
	public static final String PRINTE_FLAG_ONE = "PR_FLAG_1";

	/**
	 * 打印文件类型2
	 */
	public static final String PRINTE_FLAG_TWO = "PR_FLAG_2";

	/**
	 * 打印只生成PDF文档标志位
	 */
	public static final String ONLY_PDF_FLAG = "ONLY_PDF_FLAG";

	/*
	 * 合规审查结果及显示用文字
	 */
	public static final String COMPLIANCE_REVIEW_RESULT_0 = "0"; // 机构项目合规审查结果----未通过

	public static final String COMPLIANCE_REVIEW_RESULT_1 = "1";

	public static final String COMPLIANCE_REVIEW_RESULT_2 = "2";

	public static final String COMPLIANCE_REVIEW_RESULT_3 = "3";

	public static final String COMPLIANCE_REVIEW_RESULT_1_TEXT = "通过合规性审查";

	public static final String COMPLIANCE_REVIEW_RESULT_2_TEXT = "未通过合规性审查";

	public static final String COMPLIANCE_REVIEW_RESULT_3_TEXT = "建议提交公司评审会评审";

	/**
	 * 打印文件类型1(历史)
	 */
	public static final String PRINTE_FLAG_ONE_OLD = "PR_FLAG_1_OLD";

	/**
	 * 打印文件类型2(历史)
	 */
	public static final String PRINTE_FLAG_TWO_OLD = "PR_FLAG_2_OLD";

	/**
	 * 材料分发类型 项目报批分发项目资料
	 */
	public static final String CLFF_BPCL = "BPCL";
	
	/**
	 * 材料分发类型 批量报备项目资料
	 */
	public static final String CLFF_BRCL = "BRCL";

	/**
	 * 材料分发类型 项目报批分发项目审查意见（担保业务部）
	 */
	public static final String CLFF_BPYWYJ = "BPYWYJ";

	/**
	 * 材料分发类型 项目报批分发项目审查意见（非融中心）
	 */
	public static final String CLFF_BPFRYJ = "BPFRYJ";
	
	/**
	 * 材料分发类型 项目报批分发项目审查意见（担保业务部，用于项目退回时出具的审查意见）
	 */
	public static final String CLFF_BPYWYJ1 = "BPYWYJ1";

	/**
	 * 材料分发类型 项目报批分发项目审查意见（对外合作部）
	 */
	public static final String CLFF_BPDWYJ = "BPDWYJ";
	
	/**
	 * 材料分发类型 项目报批分发项目审查意见（债券中心）
	 */
	public static final String CLFF_BPZQYJ = "BPZQYJ";
	
	/**
	 * 材料分发类型 项目报批分发项目合法合规性审查意见（法规风险部，和风险的不一样）
	 */
	public static final String CLFF_BPFGYJ = "BPFGYJ";
	
	/**
	 * 材料分发类型 项目报批分发项目风险审查意见（风险管理部，前置审查）
	 */
	public static final String CLFF_BPQZFXYJ = "BPQZFXYJ";
	
	/**
	 * 材料分发类型 普惠批量报备项目审查意见（对外合作部）
	 */
	public static final String CLFF_IPBDWYJ = "IPBDWYJ";
	
	/**
	 * 材料分发类型 批量化普惠业务上报，项目审查意见（对外合作部）
	 */
	public static final String CLFF_IPPDWYJ = "IPPDWYJ";
	
	/**
	 * 材料分发类型 批量化普惠业务上报，分发模式化方案（对外合作部）
	 */
	public static final String CLFF_IPPATTERN = "IPPATTERN";
	
	/**
	 * 材料分发类型 项目报批分发项目审查意见（机构业务合作中心）
	 */
	public static final String CLFF_BPDWYJ2018 = "BPDWYJ2018";

	/**
	 * 材料分发类型 查看会议纪要
	 */
	public static final String CLFF_XMHYJY = "XMHYJY";

	/**
	 * 材料分发类型 机构报批材料
	 */
	public static final String CLFF_JGBPCL = "JGBPCL";
	
	/**
	 * 材料分发类型 小贷公司报批材料，材料分发流程名称显示时有用，其他情况和JGBPCL相同
	 */
	public static final String CLFF_XDBPCL = "XDBPCL";

	/**
	 * 材料分发类型 理财报批材料
	 */
	public static final String CLFF_LCBPCL = "LCBPCL";

	/**
	 * 材料分发类型 机构报批审查意见，目前小贷公司报批审查意见也用该变量
	 */
	public static final String CLFF_JGBPYJ = "JGBPYJ";
	
	/**
	 * 材料分发类型 理财报批审查意见
	 */
	public static final String CLFF_LCBPYJ = "LCBPYJ";

	/**
	 * 材料分发类型 报批类变更材料分发
	 */
	public static final String CLFF_BGBPCL = "BGBPCL";

	/**
	 * 材料分发类型 报批类变更材料分发
	 */
	public static final String CLFF_BGBACL = "BGBACL";

	/**
	 * 材料分发类型 项目复议分发项目资料
	 */
	public static final String CLFF_FYCL = "FYCL";

	/**
	 * 材料分发类型 预报项目分发项目资料
	 */
	public static final String CLFF_YBCL = "YBCL";

	/**
	 * 材料分发类型 项目预报分发项目审查意见（担保业务部）
	 */
	public static final String CLFF_YBYWYJ = "YBYWYJ";

	/**
	 * 材料分发类型 项目展期
	 */
	public static final String CLFF_ZQCL = "ZQCL";

	/**
	 * 材料分发类型 展期项目风险管理部意见
	 */
	public static final String CLFF_ZQFXYJ = "ZQFXYJ";
	
	/**
	 * 材料分发类型 展期项目法规风险部意见
	 */
	public static final String CLFF_ZQFGFXYJ = "ZQFGFXYJ";

	/**
	 * 材料分发类型 展期项目风险管理中心意见
	 */
	public static final String CLFF_ZQFGFXYJ2021 = "ZQFGFXYJ2021";

	/**
	 * 材料分发类型 展期项目 法规风险部意见
	 */
	public static final String CLFF_ZQFGFXYJ2022 = "ZQFGFXYJ2022";
	/**
	 * 材料分发类型 代偿项目材料
	 */
	public static final String CLFF_DCCL = "DCCL";
	
	/**
	 * 材料分发类型 项目报备、分发项目资料
	 */
	public static final String CLFF_BBCL = "BBCL";
	/**
	 * 材料分发类型 代偿 风险管理部意见
	 */
	//public static final String CLFF_DCFXYJ = "DCFXYJ";
	
	/**
	 * 材料分发类型 通知审查人  放款审批
	 */
	public static final String CLFF_TZ_FKSP = "TZ_FKSP";
	
	/**
	 * 材料分发类型 通知审查人  放款审批2018
	 */
	public static final String CLFF_TZ_FKSP2018 = "TZ_FKSP2018";

	/**
	 * 材料分发类型 通知审查人  放款审批2022
	 */
	public static final String CLFF_TZ_XDFKSP2022 = "TZ_XDFKSP2022";

	/**
	 * 材料分发类型 通知审查人  小贷放款审批202207
	 */
	public static final String CLFF_TZ_XDFKSP202207 = "TZ_XDFKSP202207";

	/**
	 * 材料分发类型 通知审查人  小贷放款审批202211
	 */
	public static final String CLFF_TZ_XDFKSP202211 = "TZ_XDFKSP202211";

	/**
	 * 材料分发类型 通知审查人  小贷放款审批2023
	 */
	public static final String CLFF_TZ_XDFKSP2023 = "TZ_XDFKSP2023";
	
	/**
	 * 材料分发类型 通知审查人  放款报告
	 */
	public static final String CLFF_TZ_FKBG = "TZ_FKBG";
	
	/**
	 * 材料分发类型  通知审查人 还款报告
	 */
	public static final String CLFF_TZ_HKBG = "TZ_HKBG";
	
	/**
	 * 材料分发类型 通知审查人   收入报告
	 */
	public static final String CLFF_TZ_SRBG = "TZ_SRBG";
	
	/**
	 * 材料分发类型 通知审查人   责任解除
	 */
	public static final String CLFF_TZ_ZRJC = "TZ_ZRJC";
	
	/**
	 * 材料分发类型 通知审查人   责任解除
	 */
	public static final String CLFF_TZ_ZRJC2018 = "TZ_ZRJC2018";
	
	/**
	 * 材料分发类型 通知审查人   合同审批
	 */
	public static final String CLFF_TZ_HTSP = "TZ_HTSP";

	/**
	 * 材料分发类型 通知审查人   合同审批2023，增加外部审核节点
	 */
	public static final String CLFF_TZ_HTSP2023 = "TZ_HTSP2023";
	
	/**
	 * 材料分发类型 通知审查人   分公司合同审批
	 */
	public static final String CLFF_TZ_FHTSP = "TZ_FHTSP";
	
	/**
	 * 材料分发类型 通知审查人   资产分类
	 */
	public static final String CLFF_TZ_ZCFL = "TZ_ZCFL";

	/**
	 * 材料分发类型 通知审查人   资产分类2022
	 */
	public static final String CLFF_TZ_ZCFL2022 = "TZ_ZCFL2022";

	/**
	 * 材料分发类型 通知审查人   资产分类2023
	 */
	public static final String CLFF_TZ_ZCFL2023 = "TZ_ZCFL2023";
	
	/**
	 * 材料分发类型 通知审查人   业务变更
	 */
	public static final String CLFF_TZ_YWBG = "TZ_YWBG";
	
	/**
	 * 材料分发类型 通知审查人   保后管理
	 */
	public static final String CLFF_TZ_BHGL = "TZ_BHGL";

	/**
	 * 材料分发类型 通知审查人   保后管理2021，已不使用，恢复了TZ_BHGL
	 */
	public static final String CLFF_TZ_BHGL_2021 = "TZ_BHGL2021";

	/**
	 * 材料分发类型 通知审查人   保后管理2023
	 */
	public static final String CLFF_TZ_BHGL2023 = "TZ_BHGL2023";
	
	/**
	 * 材料分发类型 通知审查人   风险预警
	 */
	public static final String CLFF_TZ_FXYJ = "TZ_FXYJ";
	
	/**
	 * 材料分发类型 通知审查人   风险预警2018
	 */
	public static final String CLFF_TZ_FXYJ2018 = "TZ_FXYJ2018";

	/**
	 * 材料分发类型 通知审查人   风险预警2021
	 */
	public static final String CLFF_TZ_FXYJ2021 = "TZ_FXYJ2021";

	/**
	 * 材料分发类型 通知审查人   风险预警2022，风险管理中心改名法规风险部
	 */
	public static final String CLFF_TZ_FXYJ2022 = "TZ_FXYJ2022";
	
	/**
	 * 材料分发类型 通知审查人   资金借用、划拨
	 */
	public static final String CLFF_TZ_FUNDBORROW = "TZ_FUNDBORROW";

	/**
	 * 材料分发类型 通知审查人   资金借用、划拨2023，只有党委书记节点，没有董事长节点
	 */
	public static final String CLFF_TZ_FUNDBORROW2023 = "TZ_FUNDBORROW2023";

	/**
	 * 材料分发类型 通知审查人   资金借用、划拨202303，总部中心需负责人审批后，经分管领导审批，再送财务部
	 */
	public static final String CLFF_TZ_FUNDBORROW202303 = "TZ_FUNDBORROW202303";

	/**
	 * 材料分发类型 通知审查人   资金借用、划拨202305，审批部门分为财务和资金中心
	 */
	public static final String CLFF_TZ_FUNDBORROW202305 = "TZ_FUNDBORROW202305";

	/**
	 * 材料分发类型 通知审查人   资金借用、划拨202308，审批部门分为财务和资金中心，增加会签部门
	 */
	public static final String CLFF_TZ_FUNDBORROW202308 = "TZ_FUNDBORROW202308";

	/**
	 * 材料分发类型 通知   模式化业务审批
	 */
	public static final String CLFF_TZ_BATCHPROJECTVERIFY = "TZ_BATCHPROJECTVERIFY";

	/**
	 * 材料分发类型 通知   模式化业务审批（立项）
	 */
	public static final String CLFF_TZ_MULTIPROJECTVERIFY = "TZ_MULTIPROJECTVERIFY";
	
	/**
	 * 材料分发类型 通知   风险项目解除
	 */
	public static final String CLFF_TZ_RISKSIGNALRELEASE = "TZ_RISKSIGNALRELEASE";

	
	/**
	 * 材料分发类型 通知   代偿追偿方案审批
	 */
	public static final String CLFF_TZ_DAICHANGF = "TZ_DAICHANGF";

	/**
	 * 材料分发类型 通知   损失报告审批
	 */
	public static final String CLFF_TZ_DAICHANG_SS = "TZ_DAICHANG_SS";
	
	/**
	 * 材料分发类型 通知   代偿追偿及损失上报
	 */
	public static final String CLFF_TZ_DAICHANGM_SB = "TZ_DAICHANGM_SB";
	
	/**
	 * 通知 报批流程  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_APPROVAL = "TZ_PROJECT_APPROVAL";

	/**
	 * 通知 报批流程2022  风险管理中心改名为法规风险部 审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_APPROVAL2022 = "TZ_PROJECT_APPROVAL2022";

	/**
	 * 通知 报批流程2023  签批项目增加党委书记审批节点 审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_APPROVAL2023 = "TZ_PROJECT_APPROVAL2023";

	/**
	 * 通知 报批流程202305  签批项目恢复董事长审批节点 审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_APPROVAL202305 = "TZ_PROJECT_APPROVAL202305";

	/**
	 * 通知 报批流程  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_RECORD = "TZ_PROJECT_RECORD";

	/**
	 * 通知 报批流程  审查意见通知2022 风险管理中心改名为法规风险部
	 */
	public static final String CLFF_TZ_PROJECT_RECORD2022 = "TZ_PROJECT_RECORD2022";

	/**
	 * 通知 报备流程  审查意见通知2023 风险管理中心改名为风险管理部，恢复法律合规部
	 */
	public static final String CLFF_TZ_PROJECT_RECORD2023 = "TZ_PROJECT_RECORD2023";

	/**
	 * 通知 报批类变更流程  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_APPROVAL_CHANGE = "TZ_PROJECT_APPROVAL_CHANGE";

	/**
	 * 通知 备案类变更流程  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_RECORD_CHANGE = "TZ_PROJECT_RECORD_CHANGE";

	/**
	 * 通知 备案类变更流程2023  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_RECORD_CHANGE2023 = "TZ_PROJECT_RECORD_CHANGE2023";

	/**
	 * 通知 展期流程  审查意见通知
	 */
	public static final String CLFF_TZ_PROJECT_EXTENSION = "TZ_PROJECT_EXTENSION";
	
	//项目代偿信息报告


	//项目追偿信息报告


	//项目代偿损失信息报告
	
	
	
	
	/**
	 * 一个 70*6的数组 用于存放累计的历史数据 报表用 金额
	 * 什么样的脑子能想出这样的方案
	 */
	public static final Double[][] OLD_AMOUNT_TOT_DATA = new Double[][] {
			// 小计，本级，辽宁，吉林，黑龙江，内蒙古
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},// //
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0},//
			{0.0, 0.0, 0.0, 0.0, 0.0, 0.0}//
	};

	// 15*12 存放累计值，业务额统计表
	public static final Double[][] OLD_AMOUNT_TOT_DATA_1 = new Double[][] {
			//{1198090.0,1092904.0,11900.0,11900.0,284642.0,340252.0,203227.0,189957.0,526221.0,505195.0,172100.0,45600.0},//
			{1184075.0,1077889.0,11900.0,11900.0,284642.0,340252.0,192712.0,178442.0,526221.0,505195.0,168600.0,42100.0},//
			{1184075.0,1077889.0,11900.0,11900.0,284642.0,340252.0,192712.0,178442.0,526221.0,505195.0,168600.0,42100.0},//
			{1061875.0,957689.0,400.0,400.0,251642.0,307252.0,162712.0,125442.0,504221.0,483195.0,142900.0,41400.0},//
			{222712.0,200612.0,400.0,400.0,99112.0,96612.0,78700.0,68700.0,34500.0,24000.0,10000.0,10900.0},//
			{839163.0,757077.0,0.0,0.0,152530.0,210640.0,84012.0,56742.0,469721.0,459195.0,132900.0,30500.0},//
			{918656.0,947529.0,400.0,400.0,251642.0,308592.0,123393.0,116442.0,501221.0,486195.0,42000.0,35900.0},//
			{184712.0,191612.0,400.0,400.0,99112.0,96612.0,43700.0,59700.0,31500.0,24000.0,10000.0,10900.0},//
			{733944.0,755917.0,0.0,0.0,152530.0,211980.0,79693.0,56742.0,469721.0,462195.0,32000.0,25000.0},//
			{180357.0,256531.0,0.0,0.0,104000.0,176200.0,13900.0,32400.0,42457.0,34931.0,20000.0,13000.0},//
			{553588.0,499387.0,0.0,0.0,48530.0,35780.0,65793.0,24342.0,427265.0,427265.0,12000.0,12000.0},//
			{105700.0,103700.0,0.0,0.0,33000.0,33000.0,30000.0,53000.0,17000.0,17000.0,25700.0,700.0},//
			{0.0,97700.0,0.0,0.0,0.0,27000.0,0.0,53000.0,0.0,17000.0,0.0,700.0},//
			{16500.0,16500.0,11500.0,11500.0,0.0,0.0,0.0,0.0,5000.0,5000.0,0.0,0.0},//
			{0.0,16500.0,0.0,11500.0,0.0,0.0,0.0,0.0,0.0,5000.0,0.0,0.0},//
			{14015.0,15015.0,0.0,0.0,0.0,0.0,10515.0,11515.0,0.0,0.0,3500.0,3500.0},//
	};

	/**
	 * 一个 59*7的数组 用于存放累计的历史数据 报表用 金额 CW
	 */
	public static final String[][] OLD_AMOUNT_TOT_DATA_CW = new String[][] {
			// 小计，本级，辽宁，吉林，黑龙江，内蒙古
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"},//
			{"0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0"}//
	};

	// 收入确信状态 0 录入未确认 1已确认
	public static final String INCOME_COMFIRM = "1";

	public static final String INCOME_INPUETED = "0";

	// 当风险保后岗计算gua_valid时间的时候，时间的单位
	public static final String PROJECT_GUA_DURATION_STR_YEAR = "年";

	public static final String PROJECT_GUA_DURATION_STR_MONTH = "月";

	public static final String PROJECT_GUA_DURATION_STR_DAY = "天";

	// 合同使用状态： contractScript.useState：0-废弃，删除； 1-使用中
	public static final String CONTRACTSCRIPT_NO_INUSE = "0";

	public static final String CONTRACTSCRIPT_INUSE = "1";

	// 财产清单单条数据使用状态： contractCounterDetail.useState：0-废弃，删除； 1-使用中 2-审批通过
	// 3-审批没有通过
	public static final String COUNTER_DETAIL_NO_INUSE = "0";

	public static final String COUNTER_DETAIL_INUSE = "1";

	public static final String COUNTER_DETAIL_PASS = "2";

	public static final String COUNTER_DETAIL_NOT_PASS = "3";

	// 财产清单类型
	/**
	 * 抵押财产清单
	 */
	public static final String COUNTER_TYPE_DY = "COUNTER_TYPE_DY";

	/**
	 * 质押财产清单（存单类）
	 */
	public static final String COUNTER_TYPE_CD = "COUNTER_TYPE_CD";

	/**
	 * 质押财产清单（动产类）
	 */
	public static final String COUNTER_TYPE_DC = "COUNTER_TYPE_DC";

	/**
	 * 质押财产清单（股权类）
	 */
	public static final String COUNTER_TYPE_GQ = "COUNTER_TYPE_GQ";

	/**
	 * 质押财产清单（权利类）
	 */
	public static final String COUNTER_TYPE_QL = "COUNTER_TYPE_QL";

	/**
	 * 质押财产清单（应收账款类）
	 */
	public static final String COUNTER_TYPE_YS = "COUNTER_TYPE_YS";

	public static final String COUNTER_TYPE_KEY_GUA = "guaType";

	public static final String COUNTER_TYPE_KEY_COU = "couGuaType";

	/**
	 * getProjectInfoCanBeSelectedOrNotState 判断项目是否可以选择的标志之一。根据具体情况判断。
	 */
	public static final String SCORE_CARD_FLAG_YOU = "有";

	/**
	 * 该打分卡是由手工输入的
	 */
	public static final String SCORE_CARD_HUMANINPUT_91 = "91";

	public static final String SCORE_CARD_HUMANINPUT_92 = "92";

	/**
	 * 工程保函类型
	 */
	public static final HashMap<String, String> bidTypeMap = new HashMap<String, String>() {
		{
			put("T", "投标");
			put("L", "履约");
			put("Y", "预付款");
			put("Z", "质量");
			put("N", "农民工");
		}
	};
	
	/**
	 * 合作协议签订--合作种类
	 */
	public static final HashMap<String, String> coopKindMap = new HashMap<String, String>() {
		{
			put("1", "业务推荐机构");
		}
	};
	
	/**
	 * 反担保人类型
	 */
	public static final HashMap<String, String> cgTypeMap = new HashMap<String, String>() {
		{
			put("1", "企业或其它组织");
			put("2", "自然人");
		}
	};
	
	/**
	 * 反担保人证件类型
	 */
	public static final HashMap<String, String> cgIdTypeMap = new HashMap<String, String>() {
		{
			put("0", "身份证");
			put("1", "户口薄");
			put("2", "护照");
			put("3", "军官证");
			put("4", "士兵证");
			put("5", "港澳居民来往内地通行证");
			put("6", "台湾同胞来往内地通行证");
			put("7", "临时身份证");
			put("8", "外国人居留证");
			put("9", "警官证");
			put("A", "香港身份证");
			put("B", "澳门身份证");
			put("C", "台湾身份证");
			put("c", "贷款卡/中征码");
			put("d", "机构信用代码");
			put("a", "组织机构代码证");
			put("X", "其他证件");
		}
	};
	
	public static final HashMap<String, String> afterGuaManageAssetMap = new HashMap<String, String>(){
		{
			put(AFTERGUANCHECK_FIRSTIDEA_ZC, "正常");
			put(AFTERGUANCHECK_FIRSTIDEA_GZ, "关注");
			put(AFTERGUANCHECK_FIRSTIDEA_CJ, "次级");
			put(AFTERGUANCHECK_FIRSTIDEA_KY, "可疑");
			put(AFTERGUANCHECK_FIRSTIDEA_SS, "损失");
		}
	};
	
	/**
	 * 吉林分公司模式化业务类型
	 */
	public static final HashMap<String, String> batchProjectTypeMap = new HashMap<String, String>() {
		{
			put("1", "乘用通");
			put("2", "二手房");
			put("3", "吉牛保");
			put("4", "惠车保");
			put("5", "吉牛活体担保");
			put("6", "小微绿通保");
			put("7", "饲料贷");
			put("8", "惠农化肥担");
			put("9", "小微速通保");
		}
	};
	
	/**
	 * 小贷公司模式化业务类型
	 */
	public static final HashMap<String, String> batchPettyLoanProjectTypeMap = new HashMap<String, String>() {
		{
			put("1", "易车贷");
			put("2", "易宅贷");
		}
	};
	
	public static final HashMap<String, String> belongToDepartMap = new HashMap<String, String>(){
		{
			put("10", DEPART_F_ZONG);
			put("11", DEPART_F_ZONG);
			put("12", DEPART_F_ZONG);
			put("13", DEPART_F_ZONG);
			put("20", DEPART_F_J);
			put("30", DEPART_F_L);
			put("40", DEPART_F_H);
			put("50", DEPART_F_N);
			put("60", DEPART_F_XD);
			put("70", DEPART_F_DL);
			put("80", DEPART_F_BJ);
		}
	};
	
	// 系统标识 业务系统
	public static final String SYS_FLAG_BUSSI = "1";

	// 系统标识 小贷系统
	public static final String SYS_FLAG_XD = "2";
	
	// 系统标识 跨组织（小贷和担保）系统
	public static final String SYS_FLAG_CROSS = "3";

	// 系统标识 业务系统

	// 借款人类型 个人
	public static final String BORROWERTYPE_PERSON = "1";

	// 借款人类型 企业
	public static final String BORROWERTYPE_ENTERPRISE = "2";
	
	public static final String XD_TYPE_DBD = "贷保贷";
	
//	/**
//	 * 项目报备，需要同时出具意见的部门数
//	 */
//	public static final int RECORD_AUDIT_DEPART_NUMBER = 2;
//	
//	/**
//	 * 备案类变更，需要同时出具意见的部门数
//	 */
//	public static final int BABGINN_AUDIT_DEPART_NUMBER = 4;
//	
//	/**
//	 * 项目报批，需要同时出具意见的部门数
//	 */
//	public static final int BUSSIINN_AUDIT_DEPART_NUMBER = 2;
	
	/**
	 * 流程提示信息：提交给经办人
	 */
	public static final String WORKFLOW_TIP_2OWNER = "点击确定把流程提交给 <strong>经办人</strong>，否则请点取消。";
	
	/**
	 * 流程提示信息：退回给经办人
	 */
	public static final String WORKFLOW_TIP_BACK2OWNER = "点击确定把流程退回给 <strong>经办人</strong>，否则请点取消。";
	
	/**
	 * 流程提示信息：结束流程
	 */
	public static final String WORKFLOW_TIP_END = "点击确定结束流程，否则请点取消。";
	
	/**
	 * 流程提示信息：提交给项目经理B角
	 */
	public static final String WORKFLOW_TIP_2ROLEB = "点击确定把流程提交给 <strong>其他项目经理</strong>复核，否则请点取消。";

}
