package com.springboot.credit.common.annotation;

import com.springboot.credit.common.validator.EnumValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 校验枚举类值注解
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RUNTIME)
@Repeatable(VerifyEnum.List.class)
@Constraint(validatedBy = {EnumValidator.class})
public @interface VerifyEnum {
    /**
     * 枚举类型
     */
    Class<? extends java.lang.Enum<?>> enumClass();

    /**
     * 枚举中用于校验的属性值
     **/
    String keyColumn();

    /**
     * 只允许填的枚举code值(填了非枚举code值会报错)
     */
    String[] allowedValues() default { };

    /**
     * 不允许填的枚举code值
     */
    String[] notAllowedValues() default { };

    /**
     * 错误消息
     **/
    String message() default "";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.PARAMETER, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface List {
        VerifyEnum[] value();
    }
}
