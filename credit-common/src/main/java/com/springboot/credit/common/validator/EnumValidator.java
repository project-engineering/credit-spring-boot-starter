package com.springboot.credit.common.validator;

import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.springboot.credit.common.annotation.VerifyEnum;
import com.springboot.credit.common.exception.BusinessException;
import com.springboot.credit.common.util.ValidatorUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

/**
 * 注解@VerifyEnum 处理逻辑方法
 */
@Slf4j
public class EnumValidator  implements ConstraintValidator<VerifyEnum, Object> {

    private Class<? extends Enum<?>> enumClass;

    private String[]  allowedValues;

    private String[]  notAllowedValues;

    private String keyColumn;

    private String message;

    @Override
    public void initialize(VerifyEnum constraintAnnotation) {
        enumClass = constraintAnnotation.enumClass();
        allowedValues = constraintAnnotation.allowedValues();
        notAllowedValues = constraintAnnotation.notAllowedValues();
        keyColumn = constraintAnnotation.keyColumn();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if(value != null){
            // 校验enumClass中是否存在keyColumn字段
            if(!ReflectUtil.hasField(enumClass, keyColumn)){
                throw new RuntimeException(StrUtil.format("EnumValidator：[{}]中不存在填入的[{}]字段", enumClass.getSimpleName(), keyColumn));
            }
            // 获取所有该枚举的指定字段的值列表
            List<Object> codeValues = EnumUtil.getFieldValues(enumClass, keyColumn);
            String validateValueMsg = ValidatorUtil.validateValues(allowedValues, notAllowedValues, value, codeValues);
            if(StrUtil.isNotBlank(validateValueMsg)){
                if (ObjectUtil.isNotEmpty(message)) {
                    throw new BusinessException(message);
                } else {
                    throw new BusinessException(validateValueMsg);
                }
            }else{
                return true;
            }
        }
        return true;
    }
}