package com.springboot.credit.common.constants;

/**
 * 通用常量类
 * @author liuc
 * @date 2023-12-16
 */

public class CommonConstant {
    /**
     * 逗号
     */
    public static String COMMA = ",";
    /**
     * 句号
     */
    public static String PERIOD = ".";
    /**
     * 竖杠
     */
    public static String VERTICAL_BAR = "|";
    /**
     * 空字符串
     */
    public static String NULLSTR = "";
    /**
     * 分号
     */
    public static char SEMICOLON = ';';
    /**
     * 下划线
     */
    public static String UNDERLINE = "_";
    /**
     * 等于号
     */
    public static String EQUAL = "=";

    public static String IS_NUMERIC = "[0-9]*";

    /**
     * 列表默认页码: 1
     */
    public static int COMMON_PAGE_NUM = 1;

    /**
     * 列表默认显示数: 10
     */
    public static int COMMON_PAGE_SIZE = 10;
    /**
     * 字符集
     */
    public static String CHARSET_UTF8 = "UTF-8";
    public static String CHARSET_GBK= "GBK";

    /**
     * 可用标识 : 可用 == 1
     */
    public static final String COMMON_FLAG_INUSE_YES = "1";

    /**
     * 可用标识 : 不可用 == 0
     */
    public static final String COMMON_FLAG_INUSE_NO = "0";

    /** 同步方式：0-系统自动同步 **/
    public static final String SYNC_TYPE_0 = "0";
    /** 同步方式：1-手动同步 **/
    public static final String SYNC_TYPE_1 = "1";
    /** 同步状态：0-同步成功 **/
    public static final String SYNC_STATUS_0 = "0";
    /** 同步状态：1-同步失败 **/
    public static final String SYNC_STATUS_1 = "1";
    /** 同步状态：2-同步中 **/
    public static final String SYNC_STATUS_2 = "2";
}
