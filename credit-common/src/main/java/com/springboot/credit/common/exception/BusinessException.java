package com.springboot.credit.common.exception;

import com.springboot.credit.common.enums.HttpStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义异常
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException{
    /**
     * http状态码
     */
    private int code;

    private Object object;

    public BusinessException(String message,int code,Object object){
        super(message);
        this.code = code;
        this.object = object;
    }

    public BusinessException(HttpStatusEnum HttpStatusEnum){
        this.code = HttpStatusEnum.getCode();
        this.object = HttpStatusEnum.getMsg();
    }

    public BusinessException(HttpStatusEnum HttpStatusEnum,String message){
        this.code = HttpStatusEnum.getCode();
        this.object = message;
    }

    public BusinessException(String message){
        super(message);
    }
}
