package com.springboot.credit.common.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import jakarta.validation.*;
import jakarta.validation.executable.ExecutableValidator;
import org.hibernate.validator.HibernateValidator;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Validator校验工具类
 */
public class ValidatorUtil {
    /**
     * 验证器工程
     */
    private static ValidatorFactory factory;
    /**
     * 对象验证器
     */
    public static Validator validator;
    /**
     * 方法验证器
     */
    public static ExecutableValidator executableValidator;

    static {
        initValidator();
        clean();
    }

    /**
     * 初始化ValidatorFactory和Validator
     */
    public static void initValidator() {
        factory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * 初始化ValidatorFactory和ExecutableValidator
     */
    public static void initExecutableValidator() {
        factory = Validation.buildDefaultValidatorFactory();
        executableValidator = factory.getValidator().forExecutables();
    }

    /**
     * 关闭ValidatorFactory工厂
     */
    public static void clean() {
        factory.close();
    }



    /**
     * 校验填入值是否合法
     * 目前用于：EnumValidator
     *
     * @param allowedValues 允许填入值数组(在codeValues再)
     * @param notAllowedValues 不允许填入值数组
     * @param value 当前填入值须校验的值
     */
    public static String validateValues(String[]  allowedValues, String[] notAllowedValues, Object value) {
        // notAllowedValues存在情况
        if (notAllowedValues != null && notAllowedValues.length > 0) {
            List<String> notAllowedList = Arrays.asList(notAllowedValues);
            if (notAllowedList.contains(String.valueOf(value))) {
                return StrUtil.format("不能填写以下值{}", notAllowedList);
            }
        }

        // allowedValues存在情况
        if (allowedValues != null && allowedValues.length > 0) {
            List<String> allowedList = Arrays.asList(allowedValues);
            if (!allowedList.contains(String.valueOf(value))) {
                return StrUtil.format("可填值只能为{}", Arrays.toString(allowedValues));
            }
        }
        return "";
    }

    /**
     * 校验填入值是否合法
     * 目前用于：DictValidator、EnumValidator
     *
     * @param allowedValues 允许填入值数组(在codeValues再)
     * @param notAllowedValues 不允许填入值数组
     * @param value 当前填入值须校验的值
     * @param codeValues 默认可填值
     */
    public static String validateValues(String[]  allowedValues, String[] notAllowedValues,
                                        Object value, List<Object> codeValues) {
        // notAllowedValues存在情况
        if(notAllowedValues != null && notAllowedValues.length > 0){
            List<String> notAllowedList = Arrays.asList(notAllowedValues);
            codeValues.removeAll(notAllowedList);
            if(notAllowedList.contains(String.valueOf(value))){
                return StrUtil.format("不能填写以下值{}", notAllowedList);
            }
        }

        // allowedValues存在情况
        if(allowedValues != null && allowedValues.length > 0){
            List<String> allowedList = Arrays.asList(allowedValues);
            // 将codeValues统一转成String
            List<String> stringCodeValues = codeValues.stream().map(String::valueOf).collect(Collectors.toList());
            if(!stringCodeValues.containsAll(allowedList)){
                // @VerifyEnum填入allowedValues存在非枚举code值
                throw new RuntimeException("填入allowedValues存在非允许值");
            }else{
                if(allowedList.contains(String.valueOf(value))){
                    return "";
                }else{
                    return StrUtil.format("可填值只能为{}", Arrays.toString(allowedValues));
                }
            }
        }

        // 校验字段值是否是字典允许数据
        if(codeValues.contains(value)){
            return "";
        }else{
            // 重置错误提示
            return StrUtil.format("可填值只能为{}", codeValues);
        }
    }

    /**
     * 获取校验错误消息
     * @param c
     * @return
     */
    public static String getErrorMsg(Set<ConstraintViolation<Object>> c){
        StringBuffer msg = new StringBuffer();
        if (CollectionUtil.isNotEmpty(c)){
            for (ConstraintViolation<Object> constraintViolation : c) {
                String itemMessage = constraintViolation.getMessage();
                String itemName = constraintViolation.getPropertyPath().toString();
                msg.append("字段<").append(itemName).append(">--").append(itemMessage).append("。");
            }
        }
        return msg.toString();
    }

    /**
     * 拼装单个对象校验信息
     *
     * @Param [s, c]
     **/
    public static void getOneInfo(StringBuffer s, Set<ConstraintViolation<Object>> c){
        if (CollectionUtil.isNotEmpty(c)){
            s.append("{ ");
            for (ConstraintViolation<Object> constraintViolation : c) {
                String itemMessage = constraintViolation.getMessage();
                String itemName = constraintViolation.getPropertyPath().toString();
                s.append("字段<" + itemName + "> :" + itemMessage + "。");
            }
            s.append(" }");
        }
    }

    /**
     * 拼装多个对象校验信息
     *
     * @Param [s, collect]
     * @return void
     */
    public static void getListInfo(StringBuffer s, List<Set<ConstraintViolation<Object>>> collect){
        for (int i = 0; i < collect.size(); i++) {
            Set<ConstraintViolation<Object>> constraintViolations = collect.get(i);
            if (CollectionUtil.isNotEmpty(constraintViolations)){
                s.append("[ " + "列表第["+ i + "]项校验不通过:");
                getOneInfo(s, constraintViolations);
                s.append(" ]");
            }
        }
    }

    /**
     * 注解校验,获取处理校验结果
     *
     * @param errorMsg 错误信息
     * @param context 校验上下文
     * @return boolean
     */
    public static boolean getResult(String errorMsg, ConstraintValidatorContext context){
        if(StrUtil.isNotBlank(errorMsg)){
            // 重置错误提示
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(errorMsg)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}
