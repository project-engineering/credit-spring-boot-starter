package com.springboot.credit.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 需要同步的业务表枚举类
 * @author liuc
 * @date 2024-01-04
 */
public enum BusinessTablesEnum {
    /**
     * 项目基本信息表
     */
    PROJECT_INFO("project_info","project_info.sh","business",""),
    /**
     * 代偿信息表
     */
    COMPENSATION_INFO("compensation_info","compensation_info.sh","business",""),
    /**
     * 项目收入明细表
     */
    PROJECT_INCOME("project_income","project_income.sh","business",""),
    /**
     * 合同信息表
     */
    CONTRACT_INFO("contract_info","contract_info.sh","business",""),
    /**
     * 合同反担保措施关联表
     */
    CONTRACT_GUAR_RELATION("contract_guar_relation","contract_guar_relation.sh","business",""),
    /**
     * 反担保措施表
     */
    COUNTER_GUARANTEE("counter_guarantee","counter_guarantee.sh","business",""),
    /**
     * 保证明细表
     */
    COUNTER_ASSURE("counter_assure","counter_assure.sh","business",""),
    /**
     * 追偿信息表
     */
    RECOVERY_INFO("recovery_info","recovery_info.sh","business",""),
    /**
     * 损失信息表
     */
    LOSS_INFO("loss_info","loss_info.sh","business",""),
    /**
     * 放款报告记录表
     */
    LOAN_REPORT_INFO("loan_report_info","loan_report_info.sh","business",""),
    /**
     * 放款报告记录表
     */
    REPAYMENT_APPLY_INFO("repayment_apply_info","repayment_apply_info.sh","business",""),
    /**
     * 企业客户信息表
     */
    CUST_ENTERPRISE_INFO("cust_enterprise_info","cust_enterprise_info.sh","customer","business"),
    /**
     * 企业客户实控人表
     */
    CUST_ENTERPRISE_CONTROLLER("cust_enterprise_controller","cust_enterprise_controller.sh","customer","business"),
    /**
     * 财务报表信息表
     */
    FINANCE_REPORT_INFO("finance_report_info","finance_report_info.sh","customer","business"),
    /**
     * 企业客户管理人表
     */
    CUST_ENTERPRISE_LEADERSHIP("cust_enterprise_leadership","cust_enterprise_leadership.sh","customer","business"),
    /**
     * 企业客户股权结构表
     */
    CUST_ENTERPRISE_INVESTOR("cust_enterprise_investor","cust_enterprise_investor.sh","customer","business"),
    /**
     * 财务报表基本信息表
     */
    FINANCE_REPORT_BASE("finance_report_base","finance_report_base.sh","customer","business"),
    /**
     * 财务报表信息表(人行)
     */
    FINANCE_REPORT_RH("finance_report_rh","finance_report_rh.sh","customer","business");


    /**
     * 需要同步的业务表类型
     */
    private final String tableName;
    /**
     * shell脚本名称
     */
    private final String shellName;
    /**
     *  所属数据源
     */
    private final String belongDataSource;
    /**
     *  所属数据源2
     */
    private final String belongDataSource2;
    /**
     * 构造方法
     * @param tableName 业务表名
     * @param shellName shell脚本名称
     * @param belongDataSource 所属数据源
     */
    BusinessTablesEnum(String tableName, String shellName,String belongDataSource,String belongDataSource2) {
        this.tableName = tableName;
        this.shellName = shellName;
        this.belongDataSource = belongDataSource;
        this.belongDataSource2 = belongDataSource2;
    }

    /**
     * 根据业务表名获取shell脚本名称
     * @param tableName 业务表名
     * @return shell脚本名称
     */
    public static Map<String,Object> of(String tableName) {
        Map<String,Object> result = new HashMap<>();
        for(BusinessTablesEnum businessTablesEnums : BusinessTablesEnum.values()) {
            if(businessTablesEnums.tableName.equalsIgnoreCase(tableName)) {
                result.put("shellName",businessTablesEnums.shellName);
                result.put("belongDataSource",businessTablesEnums.belongDataSource);
                result.put("belongDataSource2",businessTablesEnums.belongDataSource2);
            }
        }
        return result;
    }

    /**
     * 获取所有shell脚本名称
     * @return shell脚本名称列表
     */
    public static List<String> ofList() {
        List<String> list = new ArrayList<>();
        for(BusinessTablesEnum businessTablesEnums : BusinessTablesEnum.values()) {
            list.add(businessTablesEnums.shellName);
        }
        return list;
    }

}
