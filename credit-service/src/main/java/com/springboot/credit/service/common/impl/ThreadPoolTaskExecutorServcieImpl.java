package com.springboot.credit.service.common.impl;

import com.springboot.credit.service.common.ThreadPoolTaskExecutorServcie;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liuc
 * @date 2024-09-14 13:51
 */
@Service
@Log4j2
public class ThreadPoolTaskExecutorServcieImpl implements ThreadPoolTaskExecutorServcie {
    @Resource
    private ThreadPoolTaskExecutor executor;

    @Override
    public void execute() {
        List<String> shellNameList = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            shellNameList.add("shellName" + i + ".sh");
        }

        // 用于跟踪完成的任务数量
        AtomicInteger completedTasks = new AtomicInteger(0);

        // 创建所有异步任务的CompletableFuture列表
        List<CompletableFuture<Void>> futures = shellNameList.stream()
                .map(shellName -> CompletableFuture.runAsync(() -> {
                    try {
                        String tableName = shellName.substring(0, shellName.indexOf("."));
                        log.info("开始进行征信业务表{}的同步工作...", tableName);
                        StopWatch stopWatch = new StopWatch();
                        stopWatch.start();

                        syncSingleTable(tableName, shellName);

                        stopWatch.stop();
                        log.info("同步业务表[{}]完成,执行耗时：{} 毫秒", tableName, stopWatch.getTotalTimeMillis());
                        // 任务完成时，增加计数
                        completedTasks.incrementAndGet();
                    } catch (Exception e) {
                        log.error("同步业务表[{}]失败", shellName, e);
                    }
                }, executor))
                .toList();

        // 等待所有任务完成
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

        // 打印完成的任务数量
        log.info("执行完成的任务数量: {}", completedTasks.get());
    }

    private void syncSingleTable(String tableName, String shellName) {
        // TODO: 同步单个表的代码实现
        log.info("同步业务表[{}]开始...", tableName);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("同步业务表[{}]结束...", tableName);
    }
}
