package com.springboot.credit.service.manage;

import com.springboot.credit.api.common.PageResult;
import com.springboot.credit.api.dto.manage.QueryMessageSituationDto;
import com.springboot.credit.api.params.manage.QueryMessageSituationParam;

import java.util.List;

/**
 * 报文情况管理 服务类
 * @author liuc
 * @date 2024-09-24 11:45
 */
public interface MessageSituationManageService {
    /**
     * 查询报文情况管理列表信息
     * @param queryMessageSituationParam 查询报文情况管理参数
     * @return 报文情况管理列表信息
     */
    PageResult<List<QueryMessageSituationDto>>  queryMessageSituationList(QueryMessageSituationParam queryMessageSituationParam);
}
