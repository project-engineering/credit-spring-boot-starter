package com.springboot.credit.service.common.impl;

import cn.hutool.core.util.IdUtil;
import com.springboot.credit.entity.common.BusiDataSyncInfo;
import com.springboot.credit.common.UtilValidate;
import com.springboot.credit.common.constants.CommonConstant;
import com.springboot.credit.common.enums.BusinessTablesEnum;
import com.springboot.credit.common.util.DateUtil;
import com.springboot.credit.service.common.BusiDataSyncInfoService;
import com.springboot.credit.service.common.SyncBusinessDataService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 业务数据同步
 */
@Service
@Log4j2
public class SyncBusinessDataServiceImpl implements SyncBusinessDataService {
    @Resource
    private BusiDataSyncInfoService busiDataSyncInfoService;
    @Resource
    private ResourceLoader resourceLoader;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${sync.sql-script-local-path}")
    private String sqlScriptLocalPath;
    @Value("${sync.rollback-sql-script-local-path}")
    private String rollbackSqlScriptLocalPath;
    @Value("${sync.sql-script-remote-path}")
    private String sqlScriptRemotePath;
    @Value("${sync.rollback-sql-script-remote-path}")
    private String rollbackSqlScriptRemotePath;
    @Value("${sync.shell-script-remote-path}")
    private String shellScriptRemotePath;
    @Value("${sync.rollback-shell-script-remote-path}")
    private String rollbackShellScriptRemotePath;
    @Value("${sync.log-path}")
    private String logPath;
    @Value("${sync.data-source-business}")
    private String dataSourceBusiness;
    @Value("${sync.data-source-customer}")
    private String dataSourceCustomer;
    @Resource
    private ThreadPoolTaskExecutor executor;

    /**
     *  同步业务数据
     * @param type 业务类型
     */
    @Override
    public void syncBusinessData(String type) {
        log.info("开始同步业务数据，type={}", type);
        BusiDataSyncInfo busiDataSyncInfo = BusiDataSyncInfo.builder()
                .id(IdUtil.simpleUUID())
                .syncType(type)
                .syncStatus(CommonConstant.SYNC_STATUS_2)
                .syncDate(DateUtil.convertObjToUtilDate(DateUtil.getCurrLocalDateTime()))
                .build();
        busiDataSyncInfoService.save(busiDataSyncInfo);
        StringBuilder failureRecord = new StringBuilder();
        String currentDate = DateUtil.covertObjToString(DateUtil.getCurrLocalDateTime(), DateUtil.DATE_FORMATTER_3);
        List<String> shellNameList = BusinessTablesEnum.ofList();
        List<String> rollbackTableList = new ArrayList<>();

        // 使用CompletableFuture进行异步处理, 使用线程池并发执行, 并发数为shellNameList.size(), 等待所有任务完成后再更新同步状态
        CompletableFuture.allOf(shellNameList.stream().map(shellName -> CompletableFuture.runAsync(() -> {
            try {
                String tableName = shellName.substring(0, shellName.indexOf("."));
                log.info("开始进行征信业务表{}的同步工作...", tableName);
                StopWatch stopWatch = new StopWatch();
                stopWatch.start();

                syncSingleTable(tableName, currentDate, shellName, failureRecord, rollbackTableList);

                stopWatch.stop();
                log.info("同步业务表[{}]完成,执行耗时：{} 毫秒", tableName, stopWatch.getTotalTimeMillis());
            } catch (Exception e) {
                log.error("同步业务表[{}]失败", shellName, e);
                synchronized (failureRecord) {
                    failureRecord.append("同步业务表[").append(shellName).append("]失败").append(";");
                }
            }
        }, executor)).toArray(CompletableFuture[]::new)).join();

        // 更新同步状态
        updateSyncStatus(busiDataSyncInfo, failureRecord, rollbackTableList);
        log.info("同步{}张征信业务表完成......", shellNameList.size());
    }

    /**
     * 同步单张业务表
     * @param tableName 表名
     * @param currentDate 当前日期
     * @param shellName 脚本名称
     * @param failureRecord 失败记录
     * @param rollbackTableList 回滚表列表
     * @throws IOException 异常
     */
    private void syncSingleTable(String tableName, String currentDate, String shellName, StringBuilder failureRecord, List<String> rollbackTableList) throws IOException {
        log.info("============================开始同步业务表[{}]===============================", tableName);
        String sqlContent = getSqlScriptContent(tableName);
        if (UtilValidate.isEmpty(sqlContent)) {
            throw new IOException("SQL脚本内容为空");
        }
        // 创建目录（如果不存在）
        Files.createDirectories(Paths.get(shellScriptRemotePath));
        Files.createDirectories(Paths.get(sqlScriptRemotePath));
        writeSqlScriptToFile(tableName, sqlContent);
        String sqlName = tableName+".sql";
        // 将SQL内容写入到指定目录下的文件中
        String sqlFilePath = sqlScriptRemotePath+sqlName;
        // 脚本文件路径
        String filePath = shellScriptRemotePath+shellName;

        String shellScriptContent = getShellScriptContent(shellName,tableName,sqlName,sqlFilePath,filePath,logPath+currentDate+"/");
        if (UtilValidate.isEmpty(shellScriptContent)) {
            throw new IOException("Shell脚本内容为空");
        }

        writeShellScriptToFile(shellName, shellScriptContent);

        int exitCode = doProcess(shellScriptRemotePath + shellName);
        if (exitCode != 0) {
            log.error("Shell脚本[{}]执行失败", shellName);
            failureRecord.append("Shell脚本[").append(shellName).append("]执行失败;");
            throw new RuntimeException("Shell脚本["+shellName+"]执行失败");
        }

        rollbackTableList.add(tableName);
        log.info("============================同步业务表[{}]完成===============================", tableName);
    }

    /**
     * 写入SQL脚本到文件
     * @param tableName 表名
     * @param sqlContent SQL脚本内容
     * @throws IOException 异常
     */
    private void writeSqlScriptToFile(String tableName, String sqlContent) throws IOException {
        String sqlName = tableName + ".sql";
        String sqlFilePath = sqlScriptRemotePath + sqlName;
        Files.write(Paths.get(sqlFilePath), sqlContent.getBytes());
        log.info("SQL文件已成功写入到指定目录：{}", sqlScriptRemotePath);
    }

    /**
     * 将Shell脚本内容写入到指定文件中
     * @param shellName Shell脚本文件名称
     * @param shellScriptContent Shell脚本内容
     */
    private void writeShellScriptToFile(String shellName, String shellScriptContent) throws IOException {
        String filePath = shellScriptRemotePath + shellName;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            writer.write(shellScriptContent);
            log.info("已将Shell脚本内容写入到Shell脚本文件中：{}", filePath);
        }
    }

    /**
     * 更新同步状态
     * @param busiDataSyncInfo 业务数据同步信息
     * @param failureRecord 失败记录
     * @param rollbackTableList 回滚表列表
     */
    private void updateSyncStatus(BusiDataSyncInfo busiDataSyncInfo, StringBuilder failureRecord, List<String> rollbackTableList) {
        if (failureRecord.isEmpty()) {
            busiDataSyncInfo.setSyncStatus(CommonConstant.SYNC_STATUS_0);
        } else {
            busiDataSyncInfo.setSyncStatus(CommonConstant.SYNC_STATUS_1);
            busiDataSyncInfo.setFailureRecord(failureRecord.toString());
            syncBusinessDataRollBack(rollbackTableList);
        }
        busiDataSyncInfoService.updateById(busiDataSyncInfo);
    }

    /**
     * 获取SQL脚本内容
     * @param tableName 表名
     * @return SQL脚本内容
     */
    private String getSqlScriptContent(String tableName){
        String sqlContent = "";
        URI uri;
        try {
            uri = new URI(url.replace("jdbc:", ""));
            String scheme = uri.getPath().substring(1);
            Map<String,Object> map = BusinessTablesEnum.of(tableName);
            String belongDataSource = (String) map.get("belongDataSource");
            String schema2 = getDataSourceSchema(belongDataSource);
            String belongDataSource2 = (String) map.get("belongDataSource2");
            String schema3 = "";
            if (UtilValidate.isNotEmpty(belongDataSource2)) {
                schema3 = getDataSourceSchema(belongDataSource2);
            }
            log.info("表[{}]是从schema2={}数据源同步到征信库的", tableName,schema2);
            String sqlLocalPath = sqlScriptLocalPath+tableName+".sql";
            log.info("sqlLocalFilePath: {}", sqlLocalPath);
            // 加载资源
            InputStream inputStream = resourceLoader.getResource(sqlLocalPath).getInputStream();
            if (UtilValidate.isNotEmpty(inputStream)) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    StringBuilder scriptContent = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        scriptContent.append(line).append("\n");
                    }
                    // 对脚本文件进行动态参数替换
                    sqlContent = scriptContent.toString()
                            .replaceAll("schema1", scheme)
                            .replaceAll("schema2", schema2)
                            .replaceAll("schema3", schema3);
                } catch (IOException e) {
                    log.error("Failed to read the sql script file: {}", e.getMessage());
                }
            } else {
                log.error("Failed to load the sql script file: {}", sqlLocalPath);
                log.error("Failed to generate the sql script content: {}", "sql script file not found from local path["+ sqlScriptLocalPath + "]");
            }
        } catch (URISyntaxException e) {
            log.error("Failed to generate the sql script content: {}", e.getMessage());
        } catch (IOException e) {
            log.error("Failed to load the sql script file: {}", e.getMessage());
        }
        return sqlContent;
    }

    /**
     * 获取数据源的schema
     * @param belongDataSource 数据源名称
     * @return 数据源的schema
     */
    private String getDataSourceSchema(String belongDataSource) {
        return switch (belongDataSource) {
            case "business" -> dataSourceBusiness;
            case "customer" -> dataSourceCustomer;
            default -> {
                log.error("未知的dataSource: {}", belongDataSource);
                yield "";
            }
        };
    }

    /**
     * 获取Shell脚本内容
     * @param shellName shell脚本名称
     * @param tableName 表名
     * @param sqlName SQL脚本名称
     * @param sqlFilePath SQL脚本文件路径
     * @param filePath Shell脚本文件路径
     * @return Shell脚本内容
     */
    private String getShellScriptContent(String shellName, String tableName, String sqlName, String sqlFilePath, String filePath, String logsPath) {
        StringBuilder scriptContent = new StringBuilder();
        try {
            URI uri = new URI(url.replace("jdbc:", ""));
            String host = uri.getHost();
            int port = uri.getPort();

            scriptContent.append("#!/bin/bash\n");
            scriptContent.append("DB_HOST=\"").append(host).append("\"\n");
            scriptContent.append("DB_PORT=\"").append(port).append("\"\n");
            scriptContent.append("DB_USER=\"").append(username).append("\"\n");
            scriptContent.append("DB_PASSWORD=\"").append(password).append("\"\n");
            scriptContent.append("SQL_FILE=\"").append(sqlFilePath).append("\"\n");
            scriptContent.append("SHELL_FILE=\"").append(filePath).append("\"\n");
            scriptContent.append("TABLE_NAME=\"").append(tableName).append("\"\n");
            scriptContent.append("LOGPATH=\"").append(logsPath).append("\"\n");
            scriptContent.append("mkdir -p $LOGPATH\n");
            scriptContent.append("logfile=\"$LOGPATH$TABLE_NAME.log\"\n");

            // 检查shell文件是否存在
            scriptContent.append("if [ ! -f $SHELL_FILE ]; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - Shell脚本[\"").append(shellName).append("\"]不存在于目录 [").append(shellScriptRemotePath).append("]中，请检查. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");

            // 检查sql文件是否存在
            scriptContent.append("if [ ! -f $SQL_FILE ]; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - SQL脚本[\"").append(sqlName).append("\"]不存在于目录 [").append(sqlScriptRemotePath).append("]中，请检查. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");

            // 检查 MySQL 客户端是否存在
            scriptContent.append("if ! command -v mysql &> /dev/null; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - MySQL客户端不存在，请检查. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");

            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - 开始同步征信业务数据... >> $logfile\n");

            // 执行SQL文件
            scriptContent.append("mysql -h$DB_HOST -P$DB_PORT -u$DB_USER -p$DB_PASSWORD < $SQL_FILE >>$logfile 2>&1\n");
            scriptContent.append("if [ $? -ne 0 ]; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - SQL文件执行失败，请检查MySQL日志. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("else\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - SQL文件执行成功. >> $logfile\n");
            scriptContent.append("fi\n");
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - 同步征信业务数据结束. >> $logfile\n");
        } catch (URISyntaxException e) {
            log.error("解析数据库URL失败: {}", e.getMessage(), e);
        }
        return scriptContent.toString();
    }

    /**
     *  执行Shell脚本
     * @param filePath Shell脚本文件路径
     * @return Shell脚本执行结果
     */
    private int doProcess(String filePath) {
        // 启动子进程执行Shell命令
        Process process;
        // 子进程的退出码
        int exitCode = 0;
        try {
            // 执行脚本
            ProcessBuilder processBuilder = new ProcessBuilder("/bin/sh", filePath);
            process = processBuilder.start();
            // 读取子进程的输出
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                log.info(line);
            }
            // 等待子进程结束并获取结果
            exitCode = process.waitFor();
        } catch (IOException e) {
            log.error("Failed to execute the shell script: {}", e.getMessage());
        } catch (InterruptedException e) {
            log.info("Shell script execution interrupted: {}",e.getMessage());
        }
        return exitCode;
    }

    /**
     * 同步报错之后数据回滚
     */
    public void syncBusinessDataRollBack(List<String> rollbackTableList){
        try{
            if (UtilValidate.isNotEmpty(rollbackTableList)) {
                for (String tableName:rollbackTableList){
                    String currentDate = DateUtil.covertObjToString(DateUtil.getCurrLocalDateTime(), DateUtil.DATE_FORMATTER_3);
                    String rollbackShellName = tableName+"_rollback.sh";
                    log.info("开始进行征信业务表{}的同步回滚工作...", tableName);
                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    // 获取SQL脚本内容
                    log.info("开始获取项目中的回滚SQL脚本内容...");
                    String sqlContent = getRollbackSqlScriptContent(tableName);
                    String sqlName = tableName+"_rollback.sql";
                    if (UtilValidate.isNotEmpty(sqlContent)) {
                        // 创建目录（如果不存在）
                        Files.createDirectories(Paths.get(rollbackSqlScriptRemotePath));
                        Files.createDirectories(Paths.get(rollbackShellScriptRemotePath));
                        // 将SQL内容写入到指定目录下的文件中
                        String sqlFilePath = rollbackSqlScriptRemotePath+sqlName;
                        Files.write(Paths.get(sqlFilePath), sqlContent.getBytes());
                        log.info("SQL文件已成功写入到指定目录：" + rollbackSqlScriptRemotePath);
                        // 脚本文件路径
                        String filePath = rollbackShellScriptRemotePath+rollbackShellName;
                        // 获取Shell脚本内容
                        log.info("开始获取Shell脚本内容");
                        String shellScriptContent = getRollbackShellScriptContent(rollbackShellName,tableName,sqlName,sqlFilePath,filePath,logPath+currentDate+"/");
                        if (UtilValidate.isNotEmpty(shellScriptContent)) {
                            log.info("开始将Shell脚本内容写入到Shell脚本文件中，shell脚本文件路径为{}",filePath);
                            try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
                                writer.write(shellScriptContent);
                                log.info("已将Shell脚本内容写入到Shell脚本文件中！"+filePath);
                            } catch (IOException e) {
                                log.error("将写入Shell脚本内容到Shell脚本文件[{}]时失败，原因: {}", rollbackShellName,e.getMessage());
                            }
                            int exitCode = doProcess(filePath);
                            // 根据exitCode判断执行结果
                            if (UtilValidate.areEqual(exitCode,0)) {
                                log.info("[{}] Shell脚本执行成功",rollbackShellName);
                            } else {
                                log.info("[{}] Shell脚本执行失败，请到目录[{}]查看日志",rollbackShellName,logPath);
                            }
                        }else {
                            log.info("路径[{}]中[{}]的脚本内容为空，请检查",rollbackSqlScriptRemotePath,rollbackShellName);
                        }
                    } else {
                        log.info("路径[{}]中[{}]的脚本内容为空，请检查",rollbackShellScriptRemotePath,sqlName);
                    }
                    stopWatch.stop();
                    log.info("回滚业务表[{}]完成,执行耗时：{} 毫秒", tableName,stopWatch.getTotalTimeMillis());
                }
            }
        }catch (Exception e) {
            log.error("回滚业务数据失败", e);
        }
        log.info("回滚业务数据结束......");
    }

    /**
     * 获取回滚SQL脚本内容
     * @param tableName 表名
     * @return 回滚SQL脚本内容
     */
    private String getRollbackSqlScriptContent(String tableName){
        String sqlContent = "";
        URI uri;
        try {
            uri = new URI(url.replace("jdbc:", ""));
            String scheme = uri.getPath().substring(1);
            String sqlLocalPath = rollbackSqlScriptLocalPath+tableName+"_rollback.sql";
            log.info("rollbackSqlScriptLocalPath: {}", sqlLocalPath);
            // 加载资源
            InputStream inputStream = resourceLoader.getResource(sqlLocalPath).getInputStream();
            if (UtilValidate.isNotEmpty(inputStream)) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    StringBuilder scriptContent = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        scriptContent.append(line).append("\n");
                    }
                    // 对脚本文件进行动态参数替换
                    sqlContent = scriptContent.toString().replaceAll("schema1", scheme);
                } catch (IOException e) {
                    log.error("Failed to read the sql script file: {}", e.getMessage());
                }
            } else {
                log.error("Failed to load the sql script file: {}", sqlLocalPath);
                log.error("Failed to generate the sql script content: {}", "sql script file not found from local path["+ rollbackSqlScriptLocalPath + "]");
            }
        } catch (URISyntaxException e) {
            log.error("Failed to generate the sql script content: {}", e.getMessage());
        } catch (IOException e) {
            log.error("Failed to load the sql script file: {}", e.getMessage());
        }
        return sqlContent;
    }

    /**
     * 获取Shell脚本内容
     * @param shellName shell脚本名称
     * @param tableName 表名
     * @param sqlName SQL脚本名称
     * @param sqlFilePath SQL脚本文件路径
     * @param filePath Shell脚本文件路径
     * @return Shell脚本内容
     */
    private String getRollbackShellScriptContent(String shellName,String tableName,String sqlName,String sqlFilePath,String filePath,String logsPath) {
        URI uri;
        // Shell脚本内容
        StringBuilder scriptContent = new StringBuilder();
        try {
            uri = new URI(url.replace("jdbc:", ""));
            String host = uri.getHost();
            int port = uri.getPort();
            scriptContent.append("#!/bin/bash\n");
            scriptContent.append("DB_HOST=\"").append(host).append("\"\n");
            scriptContent.append("DB_PORT=\"").append(port).append("\"\n");
            scriptContent.append("DB_USER=\"").append(username).append("\"\n");
            scriptContent.append("DB_PASSWORD=\"").append(password).append("\"\n");
            scriptContent.append("SQL_FILE=\"").append(sqlFilePath).append("\"\n");
            scriptContent.append("SHELL_FILE=\"").append(filePath).append("\"\n");
            scriptContent.append("TABLE_NAME=\"").append(tableName).append("\"\n");
            scriptContent.append("LOGPATH=\"").append(logsPath).append("\"\n");
            scriptContent.append("mkdir -p $LOGPATH\n");
            scriptContent.append("logfile=\"$LOGPATH$TABLE_NAME.log\"\n");
            //检查shell文件是否存在
            scriptContent.append("if [ -f $SHELL_FILE ]; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - Shell脚本[\"").append(shellName).append("]存在于目录 \"[").append(rollbackShellScriptRemotePath).append("]中. >> $logfile\n");
            scriptContent.append("else \n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - Shell脚本[\"").append(shellName).append("]不存在于目录 \"[").append(rollbackShellScriptRemotePath).append("]中，请检查. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");
            //检查sql文件是否存在
            scriptContent.append("if [ -f $SQL_FILE ]; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - SQL脚本[\"").append(sqlName).append("]存在于目录 \"[").append(rollbackSqlScriptRemotePath).append("]中. >> $logfile\n");
            scriptContent.append("else \n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - SQL脚本[\"").append(sqlName).append("]不存在于目录 \"[").append(rollbackSqlScriptRemotePath).append("]中，请检查.. >> $logfile\n");
            scriptContent.append("  exit 1 \n");
            scriptContent.append("fi\n");
            // 检查 MySQL 客户端是否存在
            scriptContent.append("if command -v mysql &> /dev/null; then\n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - MySQL客户端存在. >> $logfile\n");
            scriptContent.append("else \n");
            scriptContent.append("  echo `date +\"%Y-%m-%d %H:%M:%S\"` - MySQL客户端不存在，请检查. >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - 开始回滚征信业务数据... >> $logfile\n");
            //执行SQL文件
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - \"Start executing SQL file...\" >> $logfile\n");
            scriptContent.append("mysql -h$DB_HOST -P$DB_PORT -u$DB_USER -p$DB_PASSWORD < $SQL_FILE > /dev/null 2>&1\n");
            scriptContent.append("if [ \"$?\" == \"0\" ]; then\n");
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - \"RollBack SQL file executed successfully.\" >> $logfile\n");
            scriptContent.append("else\n");
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - \"Error occurred during execution of the SQL file.\" >> $logfile\n");
            scriptContent.append("  exit 1\n");
            scriptContent.append("fi\n");
            scriptContent.append("echo `date +\"%Y-%m-%d %H:%M:%S\"` - \"Finished executing RollBack SQL file.\" >> $logfile");
        } catch (URISyntaxException e) {
            log.error("Failed to generate the shell script content: {}", e.getMessage());
        }
        return scriptContent.toString();
    }
}
