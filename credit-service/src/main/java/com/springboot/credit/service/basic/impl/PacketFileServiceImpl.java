package com.springboot.credit.service.basic.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.api.vo.basic.PacketFileVO;
import com.springboot.credit.entity.basic.PacketFile;
import com.springboot.credit.mapper.basic.PacketFileMapper;
import com.springboot.credit.service.basic.PacketFileService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 *  Service业务逻辑处理 : 二代报文文件表
 *  @author liuc
 *  @since 2024-01-27
 */
@Log4j2
@Service
public class PacketFileServiceImpl extends ServiceImpl<PacketFileMapper, PacketFile> implements PacketFileService {
    @Resource
    private PacketFileMapper packetFileMapper;

    /**
     * 根据id查询二代公共信息段表信息
     */
    public Page<PacketFile> selectPacketFileListByPage(PacketFileVO packetFileVO){
        Page<PacketFile> page = new Page<>(packetFileVO.getPageNum(),packetFileVO.getPageSize());
        //queryWrapper组装查询where条件
        LambdaQueryWrapper<PacketFile> queryWrapper = new LambdaQueryWrapper<>();
        return packetFileMapper.selectPage(page,queryWrapper);
    }

    /**
     * 根据id查询二代报文文件表信息
     */
    public PacketFile selectPacketFileById(PacketFileVO packetFileVO){
        return packetFileMapper.selectById(packetFileVO.getId());
    }

}