package com.springboot.credit.service.manage.impl;

import com.springboot.credit.api.params.manage.QueryMessageSituationParam;

@FunctionalInterface
interface SqlSupplier {
    String getSql(QueryMessageSituationParam param);
}