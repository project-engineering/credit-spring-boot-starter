package com.springboot.credit.service.common;

public interface SyncBusinessDataService {
    void syncBusinessData(String type);
}
