package com.springboot.credit.service.basic;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.api.vo.basic.PacketFileVO;
import com.springboot.credit.entity.basic.PacketFile;

/**
*  Service接口 : 二代报文文件表
*  @author liuc
*  @since 2024-01-27
*/
public interface PacketFileService extends IService<PacketFile> {
    /**
     * 分页查询二代报文文件表信息
     */
    Page<PacketFile> selectPacketFileListByPage(PacketFileVO packetFileVO);

    /**
     * 根据id查询二代报文文件表信息
     */
    PacketFile selectPacketFileById(PacketFileVO packetFileVO);
}