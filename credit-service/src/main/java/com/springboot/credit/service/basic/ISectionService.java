package com.springboot.credit.service.basic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.entity.basic.Section;

/**
 * 二代公共信息段表 服务层。
 *
 * @author liuc
 * @since 1.0.1
 */
public interface ISectionService extends IService<Section> {

}
