package com.springboot.credit.service.common;

/**
 * @author liuc
 * @date 2024-09-14 13:51
 */
public interface ThreadPoolTaskExecutorServcie {
    void execute();
}
