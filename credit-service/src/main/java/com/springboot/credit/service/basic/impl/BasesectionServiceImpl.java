package com.springboot.credit.service.basic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.entity.basic.BaseSection;
import com.springboot.credit.mapper.basic.BasesectionMapper;
import com.springboot.credit.service.basic.IBasesectionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 二代基础数据段表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-01-08
 */
@Service
public class BasesectionServiceImpl extends ServiceImpl<BasesectionMapper, BaseSection> implements IBasesectionService {

}
