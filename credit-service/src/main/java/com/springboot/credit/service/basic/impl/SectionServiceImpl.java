package com.springboot.credit.service.basic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.entity.basic.Section;
import com.springboot.credit.mapper.basic.SectionMapper;
import com.springboot.credit.service.basic.ISectionService;
import org.springframework.stereotype.Service;

/**
 * 二代公共信息段表 服务层实现。
 *
 * @author liuc
 * @since 1.0.1
 */
@Service
public class SectionServiceImpl extends ServiceImpl<SectionMapper, Section> implements ISectionService {

}
