package com.springboot.credit.service.manage.impl;

import com.springboot.credit.api.common.PageResult;
import com.springboot.credit.api.dto.manage.QueryMessageSituationDto;
import com.springboot.credit.api.params.manage.QueryMessageSituationParam;
import com.springboot.credit.bo.QueryMessageSituationBo;
import com.springboot.credit.common.UtilValidate;
import com.springboot.credit.common.constants.CreditConstants;
import com.springboot.credit.mapper.manage.MessageSituationManageMapper;
import com.springboot.credit.service.manage.MessageSituationManageService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * @author liuc
 * @date 2024-09-24 13:40
 */
@Log4j2
@Service
public class MessageSituationManageServiceImpl implements MessageSituationManageService {
    @Resource
    MessageSituationManageMapper mapper;

    /**
     * 查询报文情况管理列表信息
     * @param queryMessageSituationParam 查询报文情况管理列表参数
     * @return 报文情况管理列表信息
     */
    public PageResult<List<QueryMessageSituationDto>> queryMessageSituationList(QueryMessageSituationParam queryMessageSituationParam) {
        // 初始化CompletableFuture列表
        List<CompletableFuture<List<QueryMessageSituationBo>>> futures = new ArrayList<>();

        // 根据不同的记录类型，添加不同的查询任务
        if (UtilValidate.isEmpty(queryMessageSituationParam.getRecordType())) {
            addFutures(queryMessageSituationParam, futures, this::getBasicSql);
            addFutures(queryMessageSituationParam, futures, this::getLoanGuaranteeSql);
            addFutures(queryMessageSituationParam, futures, this::getFinancialSql);
        } else {
            if (UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_INFO)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_DELETE)) {
                addFutures(queryMessageSituationParam, futures, this::getBasicSql);
            } else if (UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_DEBITCREDIT_ACCOUNT)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_C1_ACCOUNT_ALTER)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_GUARANTEE_ACCOUNT)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_GUARANTEE_ACCOUNT_ALTER)) {
                addFutures(queryMessageSituationParam, futures, this::getLoanGuaranteeSql);
            } else if (UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_BALANCE_SHEET_INFO)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_PROFIT_INFO)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_CASH_INFO)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_BALANCE_DELETE)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_PROFIT_DELETE)
                    || UtilValidate.areEqual(queryMessageSituationParam.getRecordType(), CreditConstants.RECORD_TYPE_ENTERPRISE_CASH_DELETE)) {
                addFutures(queryMessageSituationParam, futures, this::getFinancialSql);
            }
        }


        // 使用自定义的ForkJoinPool来执行并行操作，可以根据实际情况调整线程池大小
        ForkJoinPool customThreadPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors() * 2);

        // 创建一个CompletableFuture来处理所有的CompletableFuture
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));

        // 使用thenApply来处理所有future的结果
        CompletableFuture<Long> countFuture = allFutures.thenApply(v -> {
            return futures.stream()
                    .flatMap(future -> future.join().stream())
                    .map(obj -> new QueryMessageSituationDto(
                            obj.getPacketFileId(),
                            obj.getFileName(),
                            obj.getRecordType(),
                            obj.getGenerateDate(),
                            obj.getDownloadTime()
                    ))
                    .distinct() // 在转换时去重
                    .count(); // 计算总数
        });

        // 分页参数
        int start = queryMessageSituationParam.getPageNo() != null ? queryMessageSituationParam.getPageNo() : 1;
        int limit = queryMessageSituationParam.getPageSize() != null ? queryMessageSituationParam.getPageSize() : 10;

        // 创建另一个CompletableFuture来获取分页数据
        CompletableFuture<List<QueryMessageSituationDto>> pageFuture = countFuture.thenApply(totalCount -> {
            // 根据分页参数创建分页流
            return futures.stream()
                    .flatMap(future -> future.join().stream())
                    .map(obj -> new QueryMessageSituationDto(
                            obj.getPacketFileId(),
                            obj.getFileName(),
                            obj.getRecordType(),
                            obj.getGenerateDate(),
                            obj.getDownloadTime()
                    ))
                    .distinct() // 在转换时去重
                    .sorted(Comparator.comparing(QueryMessageSituationDto::getGenerateDate).reversed()) // 排序
                    .skip((long) (start - 1) * limit) // 跳过前面的记录
                    .limit(limit) // 限制返回的记录数
                    .collect(Collectors.toList());
        });

        try {
            // 获取去重后的总数和分页数据
            long totalCount = countFuture.get();
            List<QueryMessageSituationDto> pageList = pageFuture.get();

            return new PageResult<>(totalCount, pageList);

        } catch (InterruptedException | ExecutionException e) {
            log.error("查询报文情况管理列表信息失败", e);
            return new PageResult<>(0L, new ArrayList<>());
        } finally {
            // 关闭自定义线程池
            customThreadPool.shutdown();
        }
    }

    /**
     * 加入查询future
     * @param param 查询参数
     * @param futures future列表
     * @param supplier sql供应商
     */
    private void addFutures(QueryMessageSituationParam param, List<CompletableFuture<List<QueryMessageSituationBo>>> futures, SqlSupplier supplier) {
        futures.add(CompletableFuture.supplyAsync(() -> mapper.queryMessageSituationList(supplier.getSql(param))));
    }

    /**
     * 获取基础sql
     * @param queryMessageSituationParam 查询参数
     */
    private String getBasicSql(QueryMessageSituationParam queryMessageSituationParam){
        StringBuilder sql = new StringBuilder();
        sql.append(" select	distinct					 ");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectId())) {
            sql.append(" 	pi.project_id ,                 ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectNo())) {
            sql.append(" 	pi.project_no,                  ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectName())) {
            sql.append(" 	pi.project_name,                ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustNo())) {
            sql.append(" 	pi.cust_no ,                    ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustName())) {
            sql.append(" 	pi.cust_name,                   ");
        }
        sql.append(" 		c2pf.file_name,              ");
        sql.append(" 		c2pf.generate_date ,         ");
        sql.append(" 		c2pf.download_time ,         ");
        sql.append(" 		c2pf.record_type,            ");
        sql.append(" 		c2pf.use_state,              ");
        sql.append(" 		c2pi.packet_file_id          ");
        sql.append(" from                                ");
        sql.append(" 	ci_2g_packet_item c2pi,          ");
        sql.append(" 	ci_2g_packet_file c2pf,          ");
        sql.append(" 	ci_2g_section c2g,               ");
        sql.append(" 	cust_enterprise_info cei,        ");
        sql.append(" 	project_info pi                  ");
        sql.append(" where                               ");
        sql.append(" 	c2pi.packet_file_id = c2pf.id    ");
        sql.append(" 	and c2pi.id = c2g.item_id        ");
        sql.append(" 	and cei.cust_id = c2g.project_id ");
        sql.append(" 	and cei.cust_no = pi.cust_no     ");
        sql.append(" and pi.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        sql.append(" and cei.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        sql.append(" and c2pf.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getRecordType())) {
            sql.append(" and c2pf.record_type = '").append(queryMessageSituationParam.getRecordType()).append("'");
        }
        addFilterConditions(queryMessageSituationParam, sql);
        return sql.toString();
    }

    /**
     * 获取贷款担保sql
     * @param queryMessageSituationParam 查询参数
     */
    private String getLoanGuaranteeSql(QueryMessageSituationParam queryMessageSituationParam){
        StringBuilder sql = new StringBuilder();
        sql.append(" select	distinct						");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectId())) {
            sql.append(" 	pi.project_id ,                 ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectNo())) {
            sql.append(" 	pi.project_no,                  ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectName())) {
            sql.append(" 	pi.project_name,                ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustNo())) {
            sql.append(" 	pi.cust_no ,                    ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustName())) {
            sql.append(" 	pi.cust_name,                   ");
        }
        sql.append(" 	c2pf.file_name,                     ");
        sql.append(" 	c2pf.generate_date ,                ");
        sql.append(" 	c2pf.download_time ,                ");
        sql.append(" 	c2pf.record_type,                   ");
        sql.append(" 	c2pf.use_state,                     ");
        sql.append(" 	c2pi.packet_file_id                 ");
        sql.append(" from                                   ");
        sql.append(" 	ci_2g_packet_item c2pi,             ");
        sql.append(" 	ci_2g_packet_file c2pf,             ");
        sql.append(" 	ci_2g_section c2g,                  ");
        sql.append(" 	project_info pi                     ");
        sql.append(" where                                  ");
        sql.append(" 	c2pi.packet_file_id = c2pf.id       ");
        sql.append(" 	and c2pi.id = c2g.item_id           ");
        sql.append(" 	and c2g.project_id = pi.project_id  ");
        sql.append("    and pi.use_state = '"+ CreditConstants.USE_STATUS_1+"'");
        sql.append(" and c2pf.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getRecordType())) {
            sql.append(" and c2pf.record_type = '").append(queryMessageSituationParam.getRecordType()).append("'");
        }
        addFilterConditions(queryMessageSituationParam, sql);
        return sql.toString();
    }

    /**
     * 获取财务报表sql
     * @param queryMessageSituationParam 查询参数
     */
    private String getFinancialSql(QueryMessageSituationParam queryMessageSituationParam){
        StringBuilder sql = new StringBuilder();
        sql.append(" select	distinct					");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectId())) {
            sql.append(" 	pi.project_id ,                 ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectNo())) {
            sql.append(" 	pi.project_no,                  ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectName())) {
            sql.append(" 	pi.project_name,                ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustNo())) {
            sql.append(" 	pi.cust_no ,                    ");
        }
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustName())) {
            sql.append(" 	pi.cust_name,                   ");
        }
        sql.append(" 	c2pf.file_name,                 ");
        sql.append(" 	c2pf.generate_date ,            ");
        sql.append(" 	c2pf.download_time ,            ");
        sql.append(" 	c2pf.record_type,               ");
        sql.append(" 	c2pf.use_state,                 ");
        sql.append(" 	c2pi.packet_file_id             ");
        sql.append(" from                               ");
        sql.append(" 	ci_2g_packet_item c2pi,         ");
        sql.append(" 	ci_2g_packet_file c2pf,         ");
        sql.append(" 	ci_2g_section c2g,              ");
        sql.append(" 	finance_report_rh frr,          ");
        sql.append(" 	cust_enterprise_info cei,       ");
        sql.append(" 	project_info pi                 ");
        sql.append(" where                              ");
        sql.append(" 	c2pi.packet_file_id = c2pf.id   ");
        sql.append(" 	and c2pi.id = c2g.item_id       ");
        sql.append(" 	and frr.id = c2g.reference_id   ");
        sql.append(" 	and cei.cust_no = c2g.enter_id  ");
        sql.append(" 	and cei.cust_no = pi.cust_no    ");
        sql.append("    and cei.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        sql.append("    and pi.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        sql.append("    and frr.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        sql.append("    and c2pf.use_state = '"+CreditConstants.USE_STATUS_1+"'");
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getRecordType())) {
            sql.append(" and c2pf.record_type = '").append(queryMessageSituationParam.getRecordType()).append("'");
        }
        addFilterConditions(queryMessageSituationParam, sql);
        return sql.toString();
    }

    /**
     * 添加过滤条件
     * @param queryMessageSituationParam 查询参数
     * @param sql 基础sql
     */
    private void addFilterConditions(QueryMessageSituationParam queryMessageSituationParam, StringBuilder sql) {
        //报送时间起(指的是报文生成时间)
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getGenerateDateStart())) {
            sql.append(" and c2pf.generate_date >= '").append(queryMessageSituationParam.getGenerateDateStart()).append(" 00:00:00'");
        }
        //报送时间止(指的是报文生成时间)
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getGenerateDateEnd())) {
            sql.append(" and c2pf.generate_date <= '").append(queryMessageSituationParam.getGenerateDateEnd()).append(" 23:59:59'");
        }
        //项目id
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectId())) {
            sql.append(" and pi.project_id = '").append(queryMessageSituationParam.getProjectId()).append("'");
        }
        //项目编号
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectNo())) {
            sql.append(" and pi.project_no = '").append(queryMessageSituationParam.getProjectNo()).append("'");
        }
        //项目名称
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getProjectName())) {
            sql.append(" and pi.project_name like '%").append(queryMessageSituationParam.getProjectName()).append("%'");
        }
        //客户号
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustNo())) {
            sql.append(" and pi.cust_no = '").append(queryMessageSituationParam.getCustNo()).append("'");
        }
        //客户名称
        if (UtilValidate.isNotEmpty(queryMessageSituationParam.getCustName())) {
            sql.append(" and pi.cust_name like '%").append(queryMessageSituationParam.getCustName()).append("%'");
        }
    }
}
