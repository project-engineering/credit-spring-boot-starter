package com.springboot.credit.service.common;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.entity.common.BusiDataSyncInfo;

/**
 * <p>
 * 业务数据同步记录表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-21
 */
public interface BusiDataSyncInfoService extends IService<BusiDataSyncInfo> {

}
