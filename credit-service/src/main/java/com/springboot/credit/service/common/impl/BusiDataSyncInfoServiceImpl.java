package com.springboot.credit.service.common.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.credit.entity.common.BusiDataSyncInfo;
import com.springboot.credit.mapper.common.BusiDataSyncInfoMapper;
import com.springboot.credit.service.common.BusiDataSyncInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务数据同步记录表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-21
 */
@Service
public class BusiDataSyncInfoServiceImpl extends ServiceImpl<BusiDataSyncInfoMapper, BusiDataSyncInfo> implements BusiDataSyncInfoService {

}
