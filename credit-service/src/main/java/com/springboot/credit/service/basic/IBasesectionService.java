package com.springboot.credit.service.basic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.credit.entity.basic.BaseSection;

/**
 * <p>
 * 二代基础数据段表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-01-08
 */
public interface IBasesectionService extends IService<BaseSection> {

}
